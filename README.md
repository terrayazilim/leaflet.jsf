# leaflet.jsf

leaflet.jsf is an Open Source Java Server Faces(JSF2.x) component for interacting with Leaflet. leaflet.jsf allows you to use Leaflet API via JSF. leaflets.jsf contains & supports below features.

# Content

  **Map Misc**

    * Properties
    * Panes

  **UI Layers**

    * Marker
    * Popup
    * Tooltip

  **Raster Layers**

    * TileLayer (XYZ)
    * TilerLayer.WMS

  **Vector Layers**

    * Path
    * Polyline
    * Polygon
    * Rectangle
    * Circle
    * Canvas

  **Other Layers**

    * GeoJSON (Currently Experimental)
    * GridLayer
    * LayerGroup (Currently Experimental)
    * FeatureGroup (Currently Experimental)

  **Basic Types**

    * LatLng
    * LatLngBounds
    * Point
    * Icon
    * DivIcon

  **Controls**

    * Attribution

  **Base Classes**

    * CRS
    * Renderer
    * Layer

  **Options**

    * MarkerOptions
    * CircleOptions
    * PolylineOptions
    * RectangleOptions
    * PolygonOptions
    * TooltipOptions
    * PopupOptions

  **JSF Converters**

    * Geojson MapModel Converter
    * Geojson FeatureCollection Converter
    * Geojson Feature Converter
    * Geojson Object Converter
    * LatLng Converter

  **Model**

    * MapModel, L.Map (Responsive option available)
    * LayerMap

  **Events**

    * Layer Click Event
    * Layer ContextMenu Event
    * Map Click Event
    * Map ContextMenu Event
    * Dragend Event
    * onAdd Event
    * onRemove Event
    * onEdit Event

  **Utility Classes**

    *JsonKit
    *GeojsonKit

  **Included Plugins Features**

    * Dynamic Drawing
    * Export Map Image
    * Fullscreen
    * Roughjs Canvas
    * Polyline Measurement Options
    * Responsive Popups
    * BoxZoom

  **Javascript**

    * leaflet.jsf.js

**Usage**

  Simply add library namespace, dependencies(locally or remotely) and leaflet.jsf.js

	[#] Add Leaflet Remotely:
	
		<link rel="stylesheet" 
				  href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"
				  integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ=="
				  crossorigin=""/>
		<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"
				  integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw=="
				  crossorigin="">
		</script>
	
	[#] Add Style for full-page responsive map.
	
		<style type="text/css">
				html,
				body {
					height: 100%;
					width: 100vw;
					margin: 0;
					padding: 0;
				}
	   </style>
   
   [#] Add dependencies locally. You can add dependencies remotely if you like. See above.
   
    <h:outputScript library="terrayazilim" name="leaflet.jsf.js"/>
    <h:outputScript name="terrayazilim/lib/Leaflet.EasyPrint/bundle.js"/>
    <h:outputScript name="terrayazilim/lib/Leaflet.fullscreen/Leaflet.fullscreen.min.js"/>
    <h:outputStylesheet name="terrayazilim/lib/Leaflet.fullscreen/leaflet.fullscreen.css"/>
    <h:outputScript name="terrayazilim/lib/Leaflet.pm/leaflet.pm.js"/>
    <h:outputStylesheet name="terrayazilim/lib/Leaflet.pm/leaflet.pm.css"/>
    <h:outputScript name="terrayazilim/lib/Leaflet.PolylineMeasure/Leaflet.PolylineMeasure.js"/>
    <h:outputStylesheet name="terrayazilim/lib/Leaflet.PolylineMeasure/Leaflet.PolylineMeasure.css"/>
    <h:outputScript name="terrayazilim/lib/Leaflet.ResponsivePopup/leaflet.responsive.popup.js"/>
    <h:outputStylesheet name="terrayazilim/lib/Leaflet.ResponsivePopup/leaflet.responsive.popup.css"/>
    <h:outputScript name="terrayazilim/lib/Leaflet.RoughCanvas/leaflet-roughcanvas.js"/>
    <h:outputScript name="terrayazilim/lib/L.Control.BoxZoom/leaflet-control-boxzoom.js"/>
    <h:outputStylesheet name="terrayazilim/lib/L.Control.BoxZoom/leaflet-control-boxzoom.css"/>
		
   [#] Add JSF Component:
   
	   <h:form prependId="false">
            <l:leaflet id = "lid" mapmodel="#{suvari.model}" responsive="true"
                       boxZoom = "true"
                       fullScreen = "true"
                       easyPrint = "true"
                       roughCanvas="true"
                       pm = "true"
                       polylineMeasure = "true"
                       style="">
                
                <f:ajax event="click" listener="#{suvari.onClick}"/>
                <f:ajax event="contextmenu" listener="#{suvari.onContextMenu}" render="lid"/>
                <f:ajax event="mapclick" listener="#{suvari.onMapClick}"/>
                <f:ajax event="mapcontextmenu" listener="#{suvari.onMapContextMenu}"/>
                <f:ajax event="dragend" listener="#{suvari.onDragend}"/>
                <f:ajax event="pmcreate" listener="#{suvari.onCreate}"/>
                <f:ajax event="pmremove" listener="#{suvari.onRemove}"/>
                <f:ajax event="pmedit" listener="#{suvari.onEdit}"/>

            </l:leaflet>
      </h:form>
	  
   [#] Basic Usage:
   
	@ManagedBean
	@ViewScoped
	public class Suvari implements Serializable {

		private MapModel model;

		@PostConstruct
		public void init() {
			this.model = new L.map();

			LatLng latlng = L.latLng(37.87376937332855, 32.49275207519532);
			Marker marker = L.marker(latlng);
			Popup responsivePopup = L.responsivePopup("Responsive");
			marker.getOptions().setPopup(responsivePopup);
      			model.put(marker);

			Polyline polyline = getPolyline();
	    polyline.getOptions().setPopup(L.responsivePopup("Polyline Responsive Popup"));
	    model.put(polyline);

	    Polygon polygon = getPolygon();
	    polygon.getOptions().setPopup(L.popup("Polygon Non Responsive Popup"));
	    polygon.getOptions().setRenderer(L.roughCanvas());
	    model.put(polygon);
		}

		public void onClick(ClickEvent event) {
			if (event.isSucceed()) {
				// ClickEvent listener
			}
		}

		public void onContextMenu(ContextMenuEvent event) {
			if (event.isSucceed()) {
				// ContextMenuEvent listener
			}
		}

		public void onMapClick(MapClickEvent event) {
			if (event.isSucceed()) {
				// MapClickEvent listener
			}
		}

		public void onMapContextMenu(MapContextMenuEvent event) {
			if (event.isSucceed()) {
				// MapContextMenuEvent listener
			}
		}

		public void onDragend(DragendEvent event) {
			if (event.isSucceed()) {
				// DragendEvent listener
			}
		}

		public void onCreate(PmCreateEvent event) {
			if (event.isSucceed()) {
				// PmCreateEvent listener
			}
		}

		public void onRemove(PmRemoveEvent event) {
			if (event.isSucceed()) {
				// PmRemoveEvent listener
			}
		}

		public void onEdit(PmEditEvent event) {
			if (event.isSucceed()) {
				// PmEditEvent listener
			}
		}

		public MapModel getModel() {
			return model;
		}

		public void setModel(MapModel model) {
			this.model = model;
		}

	}

[#] Screenshots

![img-1](img/oms1.png)
![img-2](img/osm2.png)
![img-3](img/osm3.png)
![img-4](img/osm4.png)
![img-5](img/osm5.png)
![img-6](img/osm6.png)
![img-7](img/osm7.png)
![img-8](img/osm8.png)

**Development Notes**

leaflet.jsf developed and tested with below libraries. It's highly recommended to use this versions or higher versions for now.

* Java 8
* Mojarra 2.3.0
* CDI 2.0
* Leaflet 1.3.0
* JSTL 1.2

**Status**

1.0.2

**Where can I get the latest release?**


```
<dependency>
  <groupId>tr.com.terrayazilim.leaflet.jsf</groupId>
  <artifactId>terrayazilim-leaflet.jsf</artifactId>
  <version>1.0.2</version>
</dependency>

```

or


```
git clone https://bitbucket.org/terrayazilim/leaflet.jsf
```

or


```
https://bitbucket.org/terrayazilim/leaflet.jsf/downloads/

```

**Documentation**

* https://bitbucket.org/terrayazilim/leaflet.jsf/wiki

**Issue Management**

* https://bitbucket.org/terrayazilim/leaflet.jsf/issues

**Future**

* Mojarra 2.x support
* Java 7 support
* Missing Leaflet Events.

**LICENCE**

* Apache Licence Version 2.0, https://www.apache.org/licenses/LICENSE-2.0

**CREDITS / REFERENCES**

* [Leaflet](https://leafletjs.com)
* [L.Control.BoxZoom](https://github.com/gregallensworth/L.Control.BoxZoom)
* [Leaflet.EasyPrint](https://github.com/rowanwins/leaflet-easyPrint)
* [Leaflet.fullscreen](https://github.com/Leaflet/Leaflet.fullscreen)
* [Leaflet.pm](https://github.com/codeofsumit/leaflet.pm)
* [Leaflet.PolylineMeasure](https://github.com/ppete2/Leaflet.PolylineMeasure)
* [Leaflet.ResponsivePopup](https://github.com/yafred/leaflet-responsive-popup)
* [Leaflet.RoughCanvas](https://github.com/zhuang-hao-ming/Leaflet.RoughCanvas)
* [org.json](http://json.org)

You can find licences and Copyright notices at src/main/resources/META-INF/resources/terrayazilim/lib

**Contact**

* www.terrayazilim.com.tr
* arge@terrayazilim.comtr., info@terrayazilim.com.tr
* cagritepebasili@protonmail.com, cagritepebasili@terrayazilim.com.tr
