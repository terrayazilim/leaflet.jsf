/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import java.io.Serializable;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public class MapPanes implements Serializable {

  static public enum Property {

    mapPane("mapPane"),
    tilePane("tilePane"),
    overlayPane("overlayPane"),
    shadowPane("shadowPane"),
    markerPane("markerPane"),
    tooltipPane("tooltipPane"),
    popupPane("popupPane");

    private final String pointer;

    private Property(String pointer) {
      this.pointer = pointer;
    }

    @Override
    public String toString() {
      return this.pointer;
    }
  }

  private Pane mapPane;
  private Pane tilePane;
  private Pane overlayPane;
  private Pane shadowPane;
  private Pane markerPane;
  private Pane tooltipPane;
  private Pane popupPane;

  public MapPanes() {
  }

  /**
   * Produces MapPane exactly same as leaflet mappanes.
   * 
   * @return
   */
  public static final MapPanes create() {
    MapPanes panes = new MapPanes();

    panes.setMapPane(new Pane(MapPanes.Property.mapPane.toString()));
    panes.setTilePane(new Pane(MapPanes.Property.tilePane.toString()));
    panes.setOverlayPane(new Pane(MapPanes.Property.overlayPane.toString()));
    panes.setShadowPane(new Pane(MapPanes.Property.shadowPane.toString()));
    panes.setMarkerPane(new Pane(MapPanes.Property.markerPane.toString()));
    panes.setTooltipPane(new Pane(MapPanes.Property.tooltipPane.toString()));
    panes.setPopupPane(new Pane(MapPanes.Property.popupPane.toString()));

    return panes;
  }

  /**
   * @return
   */
  public Pane getMapPane() {
    return mapPane;
  }

  /**
   * @param mapPane
   */
  public void setMapPane(Pane mapPane) {
    this.mapPane = mapPane;
  }

  /**
   * @return
   */
  public Pane getTilePane() {
    return tilePane;
  }

  /**
   * @param tilePane
   */
  public void setTilePane(Pane tilePane) {
    this.tilePane = tilePane;
  }

  /**
   * @return
   */
  public Pane getOverlayPane() {
    return overlayPane;
  }

  /**
   * @param overlayPane
   */
  public void setOverlayPane(Pane overlayPane) {
    this.overlayPane = overlayPane;
  }

  /**
   * @return
   */
  public Pane getShadowPane() {
    return shadowPane;
  }

  /**
   * @param shadowPane
   */
  public void setShadowPane(Pane shadowPane) {
    this.shadowPane = shadowPane;
  }

  /**
   * @return
   */
  public Pane getMarkerPane() {
    return markerPane;
  }

  /**
   * @param markerPane
   */
  public void setMarkerPane(Pane markerPane) {
    this.markerPane = markerPane;
  }

  /**
   * @return
   */
  public Pane getTooltipPane() {
    return tooltipPane;
  }

  /**
   * @param tooltipPane
   */
  public void setTooltipPane(Pane tooltipPane) {
    this.tooltipPane = tooltipPane;
  }

  /**
   * @return
   */
  public Pane getPopupPane() {
    return popupPane;
  }

  /**
   * @param popupPane
   */
  public void setPopupPane(Pane popupPane) {
    this.popupPane = popupPane;
  }
}
