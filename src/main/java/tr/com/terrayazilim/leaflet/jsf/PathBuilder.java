/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public final class PathBuilder {

  public Boolean stroke;
  public String color;
  public Double weight;
  public Double opacity;
  public String lineCap;
  public String lineJoin;
  public String dashArray;
  public String dashOffset;
  public Boolean fill;
  public String fillStyle;
  public String fillColor;
  public Double fillOpacity;
  public String fillRule;

  public Renderer renderer;
  public String className;

  public PathBuilder() {
  }

  /**
   * @return
   */
  public Path build() {
    return new Path(this);
  }

  /**
   * @param stroke
   * @return
   */
  public PathBuilder setStroke(Boolean stroke) {
    this.stroke = stroke;
    return this;
  }

  /**
   * @param color
   * @return
   */
  public PathBuilder setColor(String color) {
    this.color = color;
    return this;
  }

  /**
   * @param weight
   * @return
   */
  public PathBuilder setWeight(Double weight) {
    this.weight = weight;
    return this;
  }

  /**
   * @param opacity
   * @return
   */
  public PathBuilder setOpacity(Double opacity) {
    this.opacity = opacity;
    return this;
  }

  /**
   * @param lineCap
   * @return
   */
  public PathBuilder setLineCap(String lineCap) {
    this.lineCap = lineCap;
    return this;
  }

  /**
   * @param lineJoin
   * @return
   */
  public PathBuilder setLineJoin(String lineJoin) {
    this.lineJoin = lineJoin;
    return this;
  }

  /**
   * @param dashArray
   * @return
   */
  public PathBuilder setDashArray(String dashArray) {
    this.dashArray = dashArray;
    return this;
  }

  /**
   * @param dashOffset
   * @return
   */
  public PathBuilder setDashOffset(String dashOffset) {
    this.dashOffset = dashOffset;
    return this;
  }

  /**
   * @param fill
   * @return
   */
  public PathBuilder setFill(Boolean fill) {
    this.fill = fill;
    return this;
  }
  
  /**
   * @param fillStyle
   * @return 
   */
  public PathBuilder setFillStyle(String fillStyle) {
    this.fillStyle = fillStyle;
    return this;
  }

  /**
   * @param fillColor
   * @return
   */
  public PathBuilder setFillColor(String fillColor) {
    this.fillColor = fillColor;
    return this;
  }

  /**
   * @param fillOpacity
   * @return
   */
  public PathBuilder setFillOpacity(Double fillOpacity) {
    this.fillOpacity = fillOpacity;
    return this;
  }

  /**
   * @param fillRule
   * @return
   */
  public PathBuilder setFillRule(String fillRule) {
    this.fillRule = fillRule;
    return this;
  }

  /**
   * @param renderer
   * @return
   */
  public PathBuilder setRenderer(Renderer renderer) {
    this.renderer = renderer;
    return this;
  }

  /**
   * @param className
   * @return
   */
  public PathBuilder setClassName(String className) {
    this.className = className;
    return this;
  }
}
