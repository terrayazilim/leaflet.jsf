/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.layer;

import java.util.Collection;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public interface LayerGroup extends Layer {

  /**
   * @param layer 
   * @return  
   */
  Layer addLayer(Layer layer);

  /**
   * @param layer 
   * @return  
   */
  Layer removeLayer(Layer layer);

  /**
   * @param id 
   * @return  
   */
  Layer removeLayer(String id);

  /**
   * @param layer
   * @return 
   */
  boolean hasLayer(Layer layer);

  /**
   * @param id
   * @return 
   */
  boolean hasLayer(String id);

  /**
   */
  void clearLayers();

  /**
   * @param id
   * @return 
   */
  Layer getLayer(String id);

  /**
   * @return 
   */
  Collection<Layer> getLayers();

  @Override
  public LayerGroup clone();

  @Override
  public void setOptions(LayerOptions layerOptions);

  @Override
  public LayerOptions getOptions();

  @Override
  public LayerType getLayerType();

  @Override
  public void setOptionalData(Object data);

  @Override
  public Object getOptionalData();

  @Override
  public void setLayerId(String layerId);

  @Override
  public String getLayerId();

}
