/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import java.io.Serializable;
import tr.com.terrayazilim.leaflet.jsf.component.Leaflet;

/**
 * Options are can only be used if they enabled in {@link Leaflet#} component.
 * 
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public interface MapModel extends LayerMap, Serializable {

  /**
   * @see Leaflet#setPolylineMeasure(java.lang.Boolean)
   * @see Leaflet#getPolylineMeasure() 
   * 
   * @return PolylineMeasure
   */
  PolylineMeasure getPolylineMeasure();

  /**
   * @see Leaflet#setPolylineMeasure(java.lang.Boolean)
   * @see Leaflet#getPolylineMeasure() 
   * 
   * @param polylineMeasure
   */
  void setPolylineMeasure(PolylineMeasure polylineMeasure);

  /**
   * @see Leaflet#setEasyPrint(java.lang.Boolean) 
   * @see Leaflet#getEasyPrint() 
   * 
   * @return
   */
  EasyPrint getEasyPrint();

  /**
   * @see Leaflet#setEasyPrint(java.lang.Boolean) 
   * @see Leaflet#getEasyPrint() 
   * 
   * @param easyPrint
   */
  void setEasyPrint(EasyPrint easyPrint);

  /**
   * @see Leaflet#setPm(java.lang.Boolean) 
   * @see Leaflet#getPm() 
   * 
   * @return
   */
  Pm getPm();

  /**
   * @see Leaflet#setPm(java.lang.Boolean) 
   * @see Leaflet#getPm() 
   * 
   * @param pm
   */
  void setPm(Pm pm);

  /**
   * @return
   */
  MapOptions getOptions();

  /**
   * @param options
   */
  void setOptions(MapOptions options);

  /**
   * @return
   */
  MapPanes getPanes();

  /**
   * @param panes
   */
  void setPanes(MapPanes panes);
  
  /**
   * @return
   */
  TileLayer getTileLayer();

  /**
   * @param tileLayer
   */
  void setTileLayer(TileLayer tileLayer);
}
