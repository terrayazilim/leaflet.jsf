/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.event;

import javax.faces.component.UIComponent;
import javax.faces.component.behavior.Behavior;
import tr.com.terrayazilim.leaflet.jsf.layer.Layer;
import tr.com.terrayazilim.core.tuple.Quartet;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 * @since 1.0.0
 */
public class PmEditEvent<T extends Layer> extends Event {

  public static class Builder<U extends Layer> {

    public UIComponent component;
    public Behavior behavior;

    public U source;
    public U target;
    public String date;
    public String time;

    public Builder() {
    }

    /**
     * @param source
     * @return
     */
    public Builder<U> setSource(U source) {
      this.source = source;
      return this;
    }

    /**
     * @param target
     * @return
     */
    public Builder<U> setTarget(U target) {
      this.target = target;
      return this;
    }

    /**
     * @param date
     * @return
     */
    public Builder<U> setDate(String date) {
      this.date = date;
      return this;
    }

    /**
     * @param time
     * @return
     */
    public Builder<U> setTime(String time) {
      this.time = time;
      return this;
    }

    /**
     * @param component
     * @return
     */
    public Builder<U> setComponent(UIComponent component) {
      this.component = component;
      return this;
    }

    /**
     * @param behavior
     * @return
     */
    public Builder<U> setBehavior(Behavior behavior) {
      this.behavior = behavior;
      return this;
    }

    /**
     * @return
     */
    public PmEditEvent build() {
      return new PmEditEvent(this);
    }
  }

  private final T sourceLayer;
  private final T target;

  /**
   * @param component
   * @param behavior
   */
  public PmEditEvent(UIComponent component, Behavior behavior) {
    super(component, behavior);
    this.sourceLayer = null;
    this.target = null;
  }

  /**
   * @param builder
   */
  public PmEditEvent(Builder<T> builder) {
    super(builder.component, builder.behavior);

    this.sourceLayer = builder.source;
    this.target = builder.target;
    this.date = builder.date;
    this.time = builder.time;
  }

  /**
   * @param component
   * @param behavior
   * @param sourceLayer
   * @param target
   */
  public PmEditEvent(UIComponent component, Behavior behavior, T sourceLayer, T target) {
    super(component, behavior);
    this.sourceLayer = sourceLayer;
    this.target = target;
  }

  /**
   * @param component
   * @param behavior
   * @param quartet
   */
  public PmEditEvent(UIComponent component, Behavior behavior, Quartet<T, T, String, String> quartet) {
    this(component, behavior, quartet.k, quartet.l);
    this.date = quartet.m;
    this.time = quartet.n;
  }

  @Override
  public Boolean isSucceed() {
    Boolean targetOk = this.target != null;
    Boolean sourceOk = this.sourceLayer != null;

    return targetOk && sourceOk;
  }

  /**
   * @return
   */
  public T getSourceLayer() {
    return this.sourceLayer;
  }

  /**
   * @return
   */
  public T getTarget() {
    return this.target;
  }
}
