/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import java.io.Serializable;
import java.util.Collection;
import tr.com.terrayazilim.leaflet.jsf.layer.Layer;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public class MapOptions implements Serializable {

  private CRS crs;
  
  private LatLng center = new LatLng(37.93382, 32.50223);
  private Integer zoom = 11;
  private Integer minZoom = 1;
  private Integer maxZoom = 18;
  private Collection<Layer> layers;
  
  private LatLngBounds maxBounds;
  private String renderer;
  private Boolean fullscreenControl;
  private Boolean preferCanvas;
  private Boolean attributionControl;
  private Boolean zoomControl;
  private Boolean closePopupOnClick;
  private Integer zoomSnap;
  private Integer zoomDelta;
  private Boolean trackResize;
  
  private Boolean boxZoom;
  
  private Boolean doubleClickZoom;
  private Boolean dragging;
  private Boolean zoomAnimation;
  private Integer zoomAnimationThreshold;
  private Boolean fadeAnimation;
  private Boolean markerZoomAnimation;
  
  private Integer transform3DLimit; // 8388608
  private Boolean inertia;
  private Double inertiaDeceleration;
  private Double inertiaMaxSpeed;
  private Double easeLinearity;
  private Boolean worldCopyJump;
  private Double maxBoundsViscosity;
  
  private Boolean keyboard;
  private Integer keyboardPanDelta;
  private Boolean scrollWheelZoom;
  private Integer wheelDebounceTime;
  private Integer wheelPxPerZoomLevel;
  private Boolean tap;
  private Integer tapTolerance;
  private Boolean touchZoom;
  private Boolean bounceAtZoomLimits;

  public MapOptions() {
  }
  
  public MapOptions(LatLng center, Integer zoom) {
    this.center = center;
    this.zoom = zoom;
  }

  public Boolean getFullscreenControl() {
    return fullscreenControl;
  }

  public void setFullscreenControl(Boolean fullscreenControl) {
    this.fullscreenControl = fullscreenControl;
  }

  public CRS getCrs() {
    return crs;
  }

  public void setCrs(CRS crs) {
    this.crs = crs;
  }

  public LatLng getCenter() {
    return center;
  }

  public void setCenter(LatLng center) {
    this.center = center;
  }

  public Integer getZoom() {
    return zoom;
  }

  public void setZoom(Integer zoom) {
    this.zoom = zoom;
  }

  public Integer getMinZoom() {
    return minZoom;
  }

  public void setMinZoom(Integer minZoom) {
    this.minZoom = minZoom;
  }

  public Integer getMaxZoom() {
    return maxZoom;
  }

  public void setMaxZoom(Integer maxZoom) {
    this.maxZoom = maxZoom;
  }

  public Collection<Layer> getLayers() {
    return layers;
  }

  public void setLayers(Collection<Layer> layers) {
    this.layers = layers;
  }

  public LatLngBounds getMaxBounds() {
    return maxBounds;
  }

  public void setMaxBounds(LatLngBounds maxBounds) {
    this.maxBounds = maxBounds;
  }

  public String getRenderer() {
    return renderer;
  }

  public void setRenderer(String renderer) {
    this.renderer = renderer;
  }

  public Boolean getPreferCanvas() {
    return preferCanvas;
  }

  public void setPreferCanvas(Boolean preferCanvas) {
    this.preferCanvas = preferCanvas;
  }

  public Boolean getAttributionControl() {
    return attributionControl;
  }

  public void setAttributionControl(Boolean attributionControl) {
    this.attributionControl = attributionControl;
  }

  public Boolean getZoomControl() {
    return zoomControl;
  }

  public void setZoomControl(Boolean zoomControl) {
    this.zoomControl = zoomControl;
  }

  public Boolean getClosePopupOnClick() {
    return closePopupOnClick;
  }

  public void setClosePopupOnClick(Boolean closePopupOnClick) {
    this.closePopupOnClick = closePopupOnClick;
  }

  public Integer getZoomSnap() {
    return zoomSnap;
  }

  public void setZoomSnap(Integer zoomSnap) {
    this.zoomSnap = zoomSnap;
  }

  public Integer getZoomDelta() {
    return zoomDelta;
  }

  public void setZoomDelta(Integer zoomDelta) {
    this.zoomDelta = zoomDelta;
  }

  public Boolean getTrackResize() {
    return trackResize;
  }

  public void setTrackResize(Boolean trackResize) {
    this.trackResize = trackResize;
  }

  public Boolean getBoxZoom() {
    return boxZoom;
  }

  public void setBoxZoom(Boolean boxZoom) {
    this.boxZoom = boxZoom;
  }

  public Boolean getDoubleClickZoom() {
    return doubleClickZoom;
  }

  public void setDoubleClickZoom(Boolean doubleClickZoom) {
    this.doubleClickZoom = doubleClickZoom;
  }

  public Boolean getDragging() {
    return dragging;
  }

  public void setDragging(Boolean dragging) {
    this.dragging = dragging;
  }

  public Boolean getZoomAnimation() {
    return zoomAnimation;
  }

  public void setZoomAnimation(Boolean zoomAnimation) {
    this.zoomAnimation = zoomAnimation;
  }

  public Integer getZoomAnimationThreshold() {
    return zoomAnimationThreshold;
  }

  public void setZoomAnimationThreshold(Integer zoomAnimationThreshold) {
    this.zoomAnimationThreshold = zoomAnimationThreshold;
  }

  public Boolean getFadeAnimation() {
    return fadeAnimation;
  }

  public void setFadeAnimation(Boolean fadeAnimation) {
    this.fadeAnimation = fadeAnimation;
  }

  public Boolean getMarkerZoomAnimation() {
    return markerZoomAnimation;
  }

  public void setMarkerZoomAnimation(Boolean markerZoomAnimation) {
    this.markerZoomAnimation = markerZoomAnimation;
  }

  public Integer getTransform3DLimit() {
    return transform3DLimit;
  }

  public void setTransform3DLimit(Integer transform3DLimit) {
    this.transform3DLimit = transform3DLimit;
  }

  public Boolean getInertia() {
    return inertia;
  }

  public void setInertia(Boolean inertia) {
    this.inertia = inertia;
  }

  public Double getInertiaDeceleration() {
    return inertiaDeceleration;
  }

  public void setInertiaDeceleration(Double inertiaDeceleration) {
    this.inertiaDeceleration = inertiaDeceleration;
  }

  public Double getInertiaMaxSpeed() {
    return inertiaMaxSpeed;
  }

  public void setInertiaMaxSpeed(Double inertiaMaxSpeed) {
    this.inertiaMaxSpeed = inertiaMaxSpeed;
  }

  public Double getEaseLinearity() {
    return easeLinearity;
  }

  public void setEaseLinearity(Double easeLinearity) {
    this.easeLinearity = easeLinearity;
  }

  public Boolean getWorldCopyJump() {
    return worldCopyJump;
  }

  public void setWorldCopyJump(Boolean worldCopyJump) {
    this.worldCopyJump = worldCopyJump;
  }

  public Double getMaxBoundsViscosity() {
    return maxBoundsViscosity;
  }

  public void setMaxBoundsViscosity(Double maxBoundsViscosity) {
    this.maxBoundsViscosity = maxBoundsViscosity;
  }

  public Boolean getKeyboard() {
    return keyboard;
  }

  public void setKeyboard(Boolean keyboard) {
    this.keyboard = keyboard;
  }

  public Integer getKeyboardPanDelta() {
    return keyboardPanDelta;
  }

  public void setKeyboardPanDelta(Integer keyboardPanDelta) {
    this.keyboardPanDelta = keyboardPanDelta;
  }

  public Boolean getScrollWheelZoom() {
    return scrollWheelZoom;
  }

  public void setScrollWheelZoom(Boolean scrollWheelZoom) {
    this.scrollWheelZoom = scrollWheelZoom;
  }

  public Integer getWheelDebounceTime() {
    return wheelDebounceTime;
  }

  public void setWheelDebounceTime(Integer wheelDebounceTime) {
    this.wheelDebounceTime = wheelDebounceTime;
  }

  public Integer getWheelPxPerZoomLevel() {
    return wheelPxPerZoomLevel;
  }

  public void setWheelPxPerZoomLevel(Integer wheelPxPerZoomLevel) {
    this.wheelPxPerZoomLevel = wheelPxPerZoomLevel;
  }

  public Boolean getTap() {
    return tap;
  }

  public void setTap(Boolean tap) {
    this.tap = tap;
  }

  public Integer getTapTolerance() {
    return tapTolerance;
  }

  public void setTapTolerance(Integer tapTolerance) {
    this.tapTolerance = tapTolerance;
  }

  public Boolean getTouchZoom() {
    return touchZoom;
  }

  public void setTouchZoom(Boolean touchZoom) {
    this.touchZoom = touchZoom;
  }

  public Boolean getBounceAtZoomLimits() {
    return bounceAtZoomLimits;
  }

  public void setBounceAtZoomLimits(Boolean bounceAtZoomLimits) {
    this.bounceAtZoomLimits = bounceAtZoomLimits;
  }
}
