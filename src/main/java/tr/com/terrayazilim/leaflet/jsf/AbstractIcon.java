/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import java.io.Serializable;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public abstract class AbstractIcon implements Serializable {

  protected String iconUrl;
  protected String iconRetinaUrl;
  
  /**
   * Default value calculated for {@link Icon#COTTON}.
   */
  protected Point iconSize = new Point(50d, 50d);
  
  /**
   * Default value calculated for {@link Icon#COTTON}.
   */
  protected Point iconAnchor = new Point(25d, 40d);
  
  /**
   * Default value calculated for {@link Icon#COTTON}.
   */
  protected Point popupAnchor = new Point(-10d, -25d);
  
  protected Point tooltipAnchor;
  protected String shadowUrl;
  protected String shadowRetinaUrl;
  protected Point shadowSize;
  protected Point shadowAnchor;
  protected String className;

  /**
   * @return
   */
  public String getIconUrl() {
    return iconUrl;
  }

  /**
   * @param iconUrl
   */
  public void setIconUrl(String iconUrl) {
    this.iconUrl = iconUrl;
  }

  /**
   * @return
   */
  public String getIconRetinaUrl() {
    return iconRetinaUrl;
  }

  /**
   * @param iconRetinaUrl
   */
  public void setIconRetinaUrl(String iconRetinaUrl) {
    this.iconRetinaUrl = iconRetinaUrl;
  }

  /**
   * @return
   */
  public Point getIconSize() {
    return iconSize;
  }

  /**
   * @param iconSize
   */
  public void setIconSize(Point iconSize) {
    this.iconSize = iconSize;
  }

  /**
   * @return
   */
  public Point getIconAnchor() {
    return iconAnchor;
  }

  /**
   * @param iconAnchor
   */
  public void setIconAnchor(Point iconAnchor) {
    this.iconAnchor = iconAnchor;
  }

  /**
   * @return
   */
  public Point getPopupAnchor() {
    return popupAnchor;
  }

  /**
   * @param popupAnchor
   */
  public void setPopupAnchor(Point popupAnchor) {
    this.popupAnchor = popupAnchor;
  }

  /**
   * @return
   */
  public Point getTooltipAnchor() {
    return tooltipAnchor;
  }

  /**
   * @param tooltipAnchor
   */
  public void setTooltipAnchor(Point tooltipAnchor) {
    this.tooltipAnchor = tooltipAnchor;
  }

  /**
   * @return
   */
  public String getShadowUrl() {
    return shadowUrl;
  }

  /**
   * @param shadowUrl
   */
  public void setShadowUrl(String shadowUrl) {
    this.shadowUrl = shadowUrl;
  }

  /**
   * @return
   */
  public String getShadowRetinaUrl() {
    return shadowRetinaUrl;
  }

  /**
   * @param shadowRetinaUrl
   */
  public void setShadowRetinaUrl(String shadowRetinaUrl) {
    this.shadowRetinaUrl = shadowRetinaUrl;
  }

  /**
   * @return
   */
  public Point getShadowSize() {
    return shadowSize;
  }

  /**
   * @param shadowSize
   */
  public void setShadowSize(Point shadowSize) {
    this.shadowSize = shadowSize;
  }

  /**
   * @return
   */
  public Point getShadowAnchor() {
    return shadowAnchor;
  }

  /**
   * @param shadowAnchor
   */
  public void setShadowAnchor(Point shadowAnchor) {
    this.shadowAnchor = shadowAnchor;
  }

  /**
   * @return
   */
  public String getClassName() {
    return className;
  }

  /**
   * @param className
   */
  public void setClassName(String className) {
    this.className = className;
  }
}
