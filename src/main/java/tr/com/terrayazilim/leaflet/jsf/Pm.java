/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import java.io.Serializable;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public class Pm implements Serializable {

  // L.Control.Position 
  static public enum Position {

    topleft("topleft"),
    topright("topright"),
    bottomleft("bottomleft"),
    bottomright("bottomright");

    private final String pointer;

    /**
     * @param pointer
     */
    private Position(String pointer) {
      this.pointer = pointer;
    }

    @Override
    public String toString() {
      return this.pointer;
    }
  }

  private Position position = Position.topleft;
  private Boolean editMode = true;
  private Boolean removeMode = true;
  private Boolean drawMarker = true;
  private Boolean drawCircle = true;
  private Boolean drawRectangle = true;
  private Boolean drawPolyline = true;
  private Boolean drawPolygon = true;
  private Boolean cutPolygon = false;

  public Pm() {
  }

  /**
   * @return
   */
  public Boolean isEditMode() {
    return editMode;
  }

  /**
   * @param editMode
   */
  public void setEditMode(Boolean editMode) {
    this.editMode = editMode;
  }

  /**
   * @return
   */
  public Boolean isRemoveMode() {
    return removeMode;
  }

  /**
   * @param removeMode
   */
  public void setRemoveMode(Boolean removeMode) {
    this.removeMode = removeMode;
  }

  /**
   * @return
   */
  public Boolean isDrawMarker() {
    return drawMarker;
  }

  /**
   * @param drawMarker
   */
  public void setDrawMarker(Boolean drawMarker) {
    this.drawMarker = drawMarker;
  }

  /**
   * @return
   */
  public Boolean isDrawCircle() {
    return drawCircle;
  }

  /**
   * @param drawCircle
   */
  public void setDrawCircle(Boolean drawCircle) {
    this.drawCircle = drawCircle;
  }

  /**
   * @return
   */
  public Boolean isDrawRectangle() {
    return drawRectangle;
  }

  /**
   * @param drawRectangle
   */
  public void setDrawRectangle(Boolean drawRectangle) {
    this.drawRectangle = drawRectangle;
  }

  /**
   * @return
   */
  public Boolean isDrawPolyline() {
    return drawPolyline;
  }

  /**
   * @param drawPolyline
   */
  public void setDrawPolyline(Boolean drawPolyline) {
    this.drawPolyline = drawPolyline;
  }

  /**
   * @return
   */
  public Boolean isDrawPolygon() {
    return drawPolygon;
  }

  /**
   * @param drawPolygon
   */
  public void setDrawPolygon(Boolean drawPolygon) {
    this.drawPolygon = drawPolygon;
  }

  /**
   * @return
   */
  public Boolean isCutPolygon() {
    return cutPolygon;
  }

  /**
   * @param cutPolygon
   */
  public void setCutPolygon(Boolean cutPolygon) {
    this.cutPolygon = cutPolygon;
  }

  /**
   * @return
   */
  public Position getPosition() {
    return position;
  }

  /**
   * @param position
   */
  public void setPosition(Position position) {
    this.position = position;
  }
}
