/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.layer;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 * @since 1.0.0
 */
public enum LayerType {

  Marker("lmarker"),
  Circle("lcircle"),
  Rectangle("lrectangle"),
  Polyline("lpolyline"),
  Polygon("lpolygon"),
  LayerGroup("llayergroup"),
  FeatureGroup("lfeaturegroup");

  private final String string;

  /**
   * @param string
   */
  private LayerType(String string) {
    this.string = string;
  }

  @Override
  public String toString() {
    return string;
  }

  /**
   *
   * @return
   */
  public String toStringWithPrefix() {
    return string + "_";
  }

  /**
   *
   * @param layerId
   * @return
   */
  public static LayerType typeOf(String layerId) {
    if (layerId.startsWith(Marker.toStringWithPrefix())) {
      return Marker;
    } else if (layerId.startsWith(Circle.toStringWithPrefix())) {
      return Circle;
    } else if (layerId.startsWith(Rectangle.toStringWithPrefix())) {
      return Rectangle;
    } else if (layerId.startsWith(Polyline.toStringWithPrefix())) {
      return Polyline;
    } else if (layerId.startsWith(Polygon.toStringWithPrefix())) {
      return Polygon;
    } else if (layerId.startsWith(LayerGroup.toStringWithPrefix())) {
      return LayerGroup;
    } else if (layerId.startsWith(FeatureGroup.toStringWithPrefix())) {
	  return FeatureGroup;
	}

    return null;
  }

}
