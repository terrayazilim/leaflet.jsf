/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import java.io.Serializable;
import tr.com.terrayazilim.core.Objects;
import tr.com.terrayazilim.core.math.BigLatLon;
import tr.com.terrayazilim.core.math.BigLatitude;
import tr.com.terrayazilim.core.math.BigLongitude;
import tr.com.terrayazilim.core.tuple.LatlonEntry;
import tr.com.terrayazilim.leaflet.jsf.renderkit.JsonKit;

/**
 * @see JsonKit#getLatLngAdapter() 
 * 
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public class LatLng implements LatlonEntry, Serializable {

  private BigLatLon latLng;

  public LatLng() {
  }

  /**
   * @param latlng
   */
  public LatLng(LatLng latlng) {
    this(latlng.lat(), latlng.lon());
  }

  /**
   * @param latitude
   * @param longitude
   */
  public LatLng(Double latitude, Double longitude) {
    this(BigLatitude.of(latitude), BigLongitude.of(longitude));
  }

  /**
   * @param latitude
   * @param longitude
   */
  public LatLng(BigLatitude latitude, BigLongitude longitude) {
    this.latLng = new BigLatLon();
    latLng.setBigLatitude(latitude);
    latLng.setBigLongitude(longitude);
  }

  /**
   * @param latLng
   */
  public LatLng(BigLatLon latLng) {
    this.latLng = latLng;
  }

  /**
   * @param latLng
   */
  public void setLatLng(BigLatLon latLng) {
    this.latLng = latLng;
  }

  /**
   * @param lat
   * @param lng 
   */
  public void setLatLng(Double lat, Double lng) {
    if (Objects.isNull(latLng)) {
      this.latLng = new BigLatLon();
    }
    
    latLng.setBigLatitude(BigLatitude.of(lat));
    latLng.setBigLongitude(BigLongitude.of(lng));
  }

  /**
   * @return
   */
  public BigLatLon getLatLng() {
    return latLng;
  }

  /**
   * @return
   */
  public BigLatitude getBigLatitude() {
    if (Objects.isNull(latLng)) {
      return null;
    }
    
    return latLng.getBigLatitude();
  }

  /**
   * @param latitude
   */
  public void setBigLatitude(BigLatitude latitude) {
    if (Objects.isNull(latLng)) {
      this.latLng = new BigLatLon();
    }
    
    latLng.setBigLatitude(latitude);
  }

  /**
   * @return
   */
  public BigLongitude getBigLongitude() {
    if (Objects.isNull(latLng)) {
      return null;
    }
    
    return latLng.getBigLongitude();
  }

  /**
   * @param longitude
   */
  public void setBigLongitude(BigLongitude longitude) {
    if (Objects.isNull(latLng)) {
      this.latLng = new BigLatLon();
    }
    
    latLng.setBigLongitude(longitude);
  }

  @Override
  public Double lat() {
    if (Objects.isNull(latLng)) {
      return Double.NaN;
    }
    
    return latLng.lat();
  }

  @Override
  public Double lon() {
    if (Objects.isNull(latLng)) {
      return Double.NaN;
    }
    
    return latLng.lon();
  }
}
