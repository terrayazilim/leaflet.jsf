/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.layer;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import tr.com.terrayazilim.core.annotation.Development;
import tr.com.terrayazilim.gjson.GjsonFeature;
import tr.com.terrayazilim.leaflet.jsf.LatLngBounds;
import tr.com.terrayazilim.leaflet.jsf.Style;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
@Development(status = Development.Status.EXPERIMENTAL)
public class FeatureGroupImpl extends AbstractLayer implements FeatureGroup {

  private Style options;
  private Map<String, Layer> layers;

  public FeatureGroupImpl() {
    super(LayerType.FeatureGroup);
	
    this.layers = new LinkedHashMap<>();
  }

  public FeatureGroupImpl(java.util.Map<String, Layer> layers) {
    super(LayerType.FeatureGroup);

    this.layers = layers;
  }

  @Override
  public GjsonFeature toGeoJSON() {
	throw new UnsupportedOperationException("Not supported yet.");
  }
  
  @Override
  public Style getOptions() {
    return this.options;
  }

  @Override
  public void setOptions(LayerOptions layerOptions) {
	setStyle(layerOptions.toStyle());
  }

  @Override
  public void setStyle(Style style) {
	for (Map.Entry<String, Layer> entry : layers.entrySet()) {
	  String key = entry.getKey();
	  Layer value = entry.getValue();
	  
	  switch (LayerType.typeOf(key)) {
		case Marker: setMarkerStyle(style, key); break;
		case Circle: setCircleStyle(style, key); break;
		case Rectangle: setRectangleStyle(style, key); break;
		case Polyline: setPolylineStyle(style, key); break;
		case Polygon: setPolygonStyle(style, key); break;
	  }
	}
	
	this.options = style;
  }
  
  /**
   * @param style
   * @param markerId 
   */
  private void setMarkerStyle(Style style, String markerId) {
	((Marker) layers.get(markerId)).getOptions().setInteractive(style.getInteractive());
	((Marker) layers.get(markerId)).getOptions().setTooltip(style.getTooltip());
	((Marker) layers.get(markerId)).getOptions().setPopup(style.getPopup());
	((Marker) layers.get(markerId)).getOptions().setPane(style.getPane());
	((Marker) layers.get(markerId)).getOptions().setRenderer(style.getRenderer());
  }
  
  /**
   * @param style
   * @param circleId 
   */
  private void setCircleStyle(Style style, String circleId) {
	Double radius = ((Circle) layers.get(circleId)).getOptions().getRadius();
	
	CircleOptions circleOptions = new CircleOptions(style);
	circleOptions.setInteractive(style.getInteractive());
	circleOptions.setTooltip(style.getTooltip());
	circleOptions.setPopup(style.getPopup());
	circleOptions.setPane(style.getPane());
	circleOptions.setRadius(radius);
	
	((Circle) layers.get(circleId)).setOptions(circleOptions);
  }
  
  /**
   * @param style
   * @param rectangleId 
   */
  private void setRectangleStyle(Style style, String rectangleId) {
	RectangleOptions rectangleOptions = new RectangleOptions(style);
	rectangleOptions.setInteractive(style.getInteractive());
	rectangleOptions.setTooltip(style.getTooltip());
	rectangleOptions.setPopup(style.getPopup());
	rectangleOptions.setPane(style.getPane());
	
	((Rectangle) layers.get(rectangleId)).setOptions(rectangleOptions);
  }
  
  /**
   * @param style
   * @param polylineId 
   */
  private void setPolylineStyle(Style style, String polylineId) {
	PolylineOptions polylineOptions = new PolylineOptions(style);
	polylineOptions.setInteractive(style.getInteractive());
	polylineOptions.setTooltip(style.getTooltip());
	polylineOptions.setPopup(style.getPopup());
	polylineOptions.setPane(style.getPane());
	
	((Polyline) layers.get(polylineId)).setOptions(polylineOptions);
  }
  
  /**
   * @param style
   * @param polygonId 
   */
  private void setPolygonStyle(Style style, String polygonId) {
	PolygonOptions polygonOptions = new PolygonOptions(style);
	polygonOptions.setInteractive(style.getInteractive());
	polygonOptions.setTooltip(style.getTooltip());
	polygonOptions.setPopup(style.getPopup());
	polygonOptions.setPane(style.getPane());
	
	((Polygon) layers.get(polygonId)).setOptions(polygonOptions);
  }

  @Override
  public LatLngBounds getBounds() {
	throw new UnsupportedOperationException("Not supported yet.");
  }
  
  @Override
  public Collection<Layer> getLayers() {
    return layers.values();
  }

  @Override
  public Layer addLayer(Layer layer) {
    return this.layers.put(layer.getLayerId(), layer);
  }

  @Override
  public Layer removeLayer(Layer layer) {
    return this.layers.remove(layer.getLayerId());
  }

  @Override
  public Layer removeLayer(String id) {
    return this.layers.remove(id);
  }

  @Override
  public boolean hasLayer(Layer layer) {
    return this.layers.containsValue(layer);
  }

  @Override
  public boolean hasLayer(String id) {
    return this.layers.containsKey(id);
  }

  @Override
  public void clearLayers() {
    this.layers.clear();
  }

  @Override
  public Layer getLayer(String id) {
    return this.layers.get(id);
  }
  
  @Override
  public FeatureGroupImpl clone() {
    return this;
  }
}