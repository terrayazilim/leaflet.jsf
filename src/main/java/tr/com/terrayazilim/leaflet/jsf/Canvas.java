/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import tr.com.terrayazilim.leaflet.jsf.renderkit.JsonKit;

/**
 * @see RoughCanvas
 * @see JsonKit#getRendererSerializer()
 * 
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public class Canvas implements Renderer {

  private Attribution attribution;
  private Double padding;
  private Integer tolerance;
  private Pane pane;

  public Canvas() {
  }

  @Override
  public Double getPadding() {
    return padding;
  }

  @Override
  public void setPadding(Double padding) {
    this.padding = padding;
  }

  /**
   * @return
   */
  public Integer getTolerance() {
    return tolerance;
  }

  /**
   * @param tolerance
   */
  public void setTolerance(Integer tolerance) {
    this.tolerance = tolerance;
  }

  @Override
  public Pane getPane() {
    return pane;
  }

  @Override
  public void setPane(Pane pane) {
    this.pane = pane;
  }

  @Override
  public Attribution getAttribution() {
    return attribution;
  }

  @Override
  public void setAttribution(Attribution attribution) {
    this.attribution = attribution;
  }
}
