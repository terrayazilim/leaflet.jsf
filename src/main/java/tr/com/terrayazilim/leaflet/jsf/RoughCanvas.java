/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import tr.com.terrayazilim.json.annotation.Ignored;

/**
 * {@link #type} and {@link #randomPaint} field related to 
 * {@literal leaflet.jsf.js#decodeCanvasRenderer}. If you set {@link #randomPaint} value to
 * {@code false}, fill the style of {@literal L.roughCanvas}.
 * 
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public class RoughCanvas implements Renderer {

  @Ignored
  public static final String ROUGH_CANVAS = "L.Canvas.roughCanvas()";

  private String type = "roughCanvas";
  private Attribution attribution;
  private Double padding;
  private Integer tolerance;
  private Pane pane;

  private Integer maxRandomnessOffset;
  private Integer roughness;
  private Integer bowing;
  private Integer strokeWidth;
  private Integer curveTightness;
  private Integer curveStepCount;
  private Integer fillWeight;
  private Integer hachureAngle;
  private Integer hachureGap;

  private Boolean randomPaint = Boolean.TRUE;

  public RoughCanvas() {
    this(Boolean.TRUE);
  }

  /**
   * @param randomPaint
   */
  public RoughCanvas(Boolean randomPaint) {
    this.randomPaint = randomPaint;
  }

  /**
   * @return
   */
  public String getType() {
    return type;
  }

  /**
   * @param type
   */
  public void setType(String type) {
    this.type = type;
  }

  @Override
  public Attribution getAttribution() {
    return attribution;
  }

  @Override
  public void setAttribution(Attribution attribution) {
    this.attribution = attribution;
  }

  @Override
  public Double getPadding() {
    return padding;
  }

  @Override
  public void setPadding(Double padding) {
    this.padding = padding;
  }

  /**
   * @return
   */
  public Integer getTolerance() {
    return tolerance;
  }

  /**
   * @param tolerance
   */
  public void setTolerance(Integer tolerance) {
    this.tolerance = tolerance;
  }

  @Override
  public Pane getPane() {
    return pane;
  }

  @Override
  public void setPane(Pane pane) {
    this.pane = pane;
  }

  /**
   * @return
   */
  public Integer getMaxRandomnessOffset() {
    return maxRandomnessOffset;
  }

  /**
   * @param maxRandomnessOffset
   */
  public void setMaxRandomnessOffset(Integer maxRandomnessOffset) {
    this.maxRandomnessOffset = maxRandomnessOffset;
  }

  /**
   * @return
   */
  public Integer getRoughness() {
    return roughness;
  }

  /**
   * @param roughness
   */
  public void setRoughness(Integer roughness) {
    this.roughness = roughness;
  }

  /**
   * @return
   */
  public Integer getBowing() {
    return bowing;
  }

  /**
   * @param bowing
   */
  public void setBowing(Integer bowing) {
    this.bowing = bowing;
  }

  /**
   * @return
   */
  public Integer getStrokeWidth() {
    return strokeWidth;
  }

  /**
   * @param strokeWidth
   */
  public void setStrokeWidth(Integer strokeWidth) {
    this.strokeWidth = strokeWidth;
  }

  /**
   * @return
   */
  public Integer getCurveTightness() {
    return curveTightness;
  }

  /**
   * @param curveTightness
   */
  public void setCurveTightness(Integer curveTightness) {
    this.curveTightness = curveTightness;
  }

  /**
   * @return
   */
  public Integer getCurveStepCount() {
    return curveStepCount;
  }

  /**
   * @param curveStepCount
   */
  public void setCurveStepCount(Integer curveStepCount) {
    this.curveStepCount = curveStepCount;
  }

  /**
   * @return
   */
  public Integer getFillWeight() {
    return fillWeight;
  }

  /**
   * @param fillWeight
   */
  public void setFillWeight(Integer fillWeight) {
    this.fillWeight = fillWeight;
  }

  /**
   * @return
   */
  public Integer getHachureAngle() {
    return hachureAngle;
  }

  /**
   * @param hachureAngle
   */
  public void setHachureAngle(Integer hachureAngle) {
    this.hachureAngle = hachureAngle;
  }

  /**
   * @return
   */
  public Integer getHachureGap() {
    return hachureGap;
  }

  /**
   * @param hachureGap
   */
  public void setHachureGap(Integer hachureGap) {
    this.hachureGap = hachureGap;
  }

  /**
   * @return
   */
  public Boolean getRandomPaint() {
    return randomPaint;
  }

  /**
   * @param randomPaint
   */
  public void setRandomPaint(Boolean randomPaint) {
    this.randomPaint = randomPaint;
  }
}
