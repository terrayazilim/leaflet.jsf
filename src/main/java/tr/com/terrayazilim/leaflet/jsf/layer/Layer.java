/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.layer;

import java.io.Serializable;
import tr.com.terrayazilim.gjson.GjsonFeature;
import tr.com.terrayazilim.leaflet.jsf.MapModel;

/**
 * @see <a href="https://leafletjs.com/reference-1.3.0.html#layer">L.Layer</a>
 * 
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 * @since 1.0.0
 */
public interface Layer extends Serializable, Cloneable {

  /**
   * @return 
   */
  GjsonFeature toGeoJSON();
  
  /**
   * @since 1.0.1
   * @param model 
   */
  void addTo(MapModel model);
  
  /**
   * @since 1.0.1
   * @param model
   * @return 
   */
  boolean removeFrom(MapModel model);
  
  /**
   * @return Unique LayerId
   */
  String getLayerId();

  /**
   * Setter for Unique Layer Id. Do not use unless it's absolutely necessary.
   *
   * @param layerId
   */
  void setLayerId(String layerId);

  /**
   * In case we use it for now, It's responsible for holding layer type of Object as String. Idk why
   * we use it for this reason. Check later...
   *
   * @return Anything Optional.
   */
  Object getOptionalData();

  /**
   * Setter for Optional data. Use carefully with casting.
   *
   * @param data
   */
  void setOptionalData(Object data);

  /**
   * @return
   */
  LayerType getLayerType();

  /**
   * @return
   */
  LayerOptions getOptions();

  /**
   * @param layerOptions
   */
  void setOptions(LayerOptions layerOptions);

  /**
   * @return
   */
  Layer clone();
}
