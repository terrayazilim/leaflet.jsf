/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import tr.com.terrayazilim.core.Objects;
import tr.com.terrayazilim.leaflet.jsf.layer.LayerOptions;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public class Style extends Path implements LayerOptions {

  private Boolean interactive;
  private Tooltip tooltip;
  private Popup popup;
  private Pane pane;

  public Style() {
  }
  
  public Style(Style style) {
	this.interactive = style.getInteractive();
	this.tooltip = style.getTooltip();
	this.popup = style.getPopup();
	this.pane = style.getPane();
	this.renderer = style.getRenderer();
  }
  
  public Style(Path path) {
	if (Objects.isNull(path)) {
      path = new Path();
    }

    this.stroke = path.getStroke();
    this.color = path.getColor();
    this.weight = path.getWeight();
    this.opacity = path.getOpacity();
    this.lineCap = path.getLineCap();
    this.lineJoin = path.getLineJoin();
    this.dashArray = path.getDashArray();
    this.dashOffset = path.getDashOffset();
    this.fill = path.getFill();
    this.fillColor = path.getFillColor();
    this.fillOpacity = path.getFillOpacity();
    this.fillRule = path.getFillRule();
    this.renderer = path.getRenderer();
    this.className = path.getClassName();
  }

  public Style(Boolean interactive, Tooltip tooltip, Popup popup, Pane pane, Renderer renderer) {
	this.interactive = interactive;
	this.tooltip = tooltip;
	this.popup = popup;
	this.pane = pane;
	this.renderer = renderer;
  }
  
  @Override
  public Boolean getInteractive() {
    return interactive;
  }

  @Override
  public void setInteractive(Boolean interactive) {
    this.interactive = interactive;
  }

  @Override
  public Tooltip getTooltip() {
    return tooltip;
  }

  @Override
  public void setTooltip(Tooltip tooltip) {
    this.tooltip = tooltip;
  }

  @Override
  public Popup getPopup() {
    return popup;
  }

  @Override
  public void setPopup(Popup popup) {
    this.popup = popup;
  }

  @Override
  public Pane getPane() {
    return pane;
  }

  @Override
  public void setPane(Pane pane) {
    this.pane = pane;
  }

  @Override
  public Renderer getRenderer() {
    return renderer;
  }

  @Override
  public void setRenderer(Renderer renderer) {
    this.renderer = renderer;
  }
}
