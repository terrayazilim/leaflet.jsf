/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.event;

import java.io.Serializable;
import javax.faces.component.UIComponent;
import javax.faces.component.behavior.Behavior;
import tr.com.terrayazilim.leaflet.jsf.layer.Marker;
import tr.com.terrayazilim.core.tuple.Quartet;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 * @since 1.0.0
 */
public class DragendEvent extends Event implements Serializable {

  public static class Builder {

    public UIComponent component;
    public Behavior behavior;

    public Marker target;
    public Double distance;
    public String date;
    public String time;

    public Builder() {
    }

    /**
     * @param target
     * @return
     */
    public Builder setTarget(Marker target) {
      this.target = target;
      return this;
    }

    /**
     * @param distance
     * @return
     */
    public Builder setDistance(Double distance) {
      this.distance = distance;
      return this;
    }

    /**
     * @param date
     * @return
     */
    public Builder setDate(String date) {
      this.date = date;
      return this;
    }

    /**
     * @param time
     * @return
     */
    public Builder setTime(String time) {
      this.time = time;
      return this;
    }

    /**
     * @param component
     * @return
     */
    public Builder setComponent(UIComponent component) {
      this.component = component;
      return this;
    }

    /**
     * @param behavior
     * @return
     */
    public Builder setBehavior(Behavior behavior) {
      this.behavior = behavior;
      return this;
    }

    /**
     * @return
     */
    public DragendEvent build() {
      return new DragendEvent(this);
    }
  }

  private final Marker target;
  private final Double distance;

  /**
   * @param component
   * @param behavior
   */
  public DragendEvent(UIComponent component, Behavior behavior) {
    super(component, behavior);
    this.target = null;
    this.distance = null;
  }

  /**
   * @param builder
   */
  public DragendEvent(Builder builder) {
    super(builder.component, builder.behavior);
    this.target = builder.target;
    this.distance = builder.distance;
    this.date = builder.date;
    this.time = builder.time;
  }

  /**
   * @param component
   * @param behavior
   * @param target
   * @param distace
   */
  public DragendEvent(UIComponent component, Behavior behavior, Marker target, Double distace) {
    super(component, behavior);
    this.target = target;
    this.distance = distace;
  }

  /**
   * @param component
   * @param behavior
   * @param quartet
   */
  public DragendEvent(UIComponent component, Behavior behavior, Quartet<Marker, Double, String, String> quartet) {
    this(component, behavior, quartet.k, quartet.l);
    this.date = quartet.m;
    this.time = quartet.n;
  }

  @Override
  public Boolean isSucceed() {
    Boolean targetOk = this.target != null;
    Boolean distanceOk = this.distance != null;

    return targetOk && distanceOk;
  }

  /**
   * @return
   */
  public Marker getTarget() {
    return target;
  }

  /**
   * @return
   */
  public Double getDistance() {
    return distance;
  }
}
