/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import static tr.com.terrayazilim.leaflet.jsf.Icon.COTTON;
import static tr.com.terrayazilim.leaflet.jsf.Icon.DOODLE;
import static tr.com.terrayazilim.leaflet.jsf.Icon.DUSK;
import static tr.com.terrayazilim.leaflet.jsf.Icon.MODERN_RED;
import static tr.com.terrayazilim.leaflet.jsf.Icon.NOLAN;
import static tr.com.terrayazilim.leaflet.jsf.Icon.ULTRAVIOLET;

/**
 * @see AbstractIcon
 * @see Icon
 * 
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public class DivIcon extends AbstractIcon {

  private String html;
  private Point bgPos;

  public DivIcon() {
    this(COTTON);
  }

  /**
   * @param iconUrl
   */
  public DivIcon(String iconUrl) {
    this.iconUrl = iconUrl;
  }

  /**
   * @param iconUrl
   * @param className
   */
  public DivIcon(String iconUrl, String className) {
    this(iconUrl);
    this.className = className;
  }

  public static final DivIcon ultraviolet() {
    return new DivIcon(ULTRAVIOLET);
  }

  public static final DivIcon modernRed() {
    return new DivIcon(MODERN_RED);
  }

  public static final DivIcon dusk() {
    return new DivIcon(DUSK);
  }

  public static final DivIcon nolan() {
    return new DivIcon(NOLAN);
  }

  public static final DivIcon doodle() {
    return new DivIcon(DOODLE);
  }

  /**
   * @return
   */
  public String getHtml() {
    return html;
  }

  /**
   * @param html
   */
  public void setHtml(String html) {
    this.html = html;
  }

  /**
   * @return
   */
  public Point getBgPos() {
    return bgPos;
  }

  /**
   * @param bgPos
   */
  public void setBgPos(Point bgPos) {
    this.bgPos = bgPos;
  }
}
