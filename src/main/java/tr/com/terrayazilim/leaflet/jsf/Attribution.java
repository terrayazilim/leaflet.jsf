/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import tr.com.terrayazilim.leaflet.jsf.renderkit.JsonKit;

/**
 * @see JsonKit#getAttributionSerializer()
 * 
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public class Attribution implements Serializable {

  private String leaflet = "&copy; <a href=\\\"http://www.leafletjs.com \\\">Leaflet<\\/a>";
  private String osm = "&copy; <a href=\\\"http://www.openstreetmap.org/copyright \\\">OpenStreetMap Contributors<\\/a>";

  private Set<String> attributions;

  public Attribution() {
  }

  /**
   * @param attr
   */
  private Attribution(String attr) {
    if (this.attributions == null) {
      this.attributions = new HashSet<>();
    }

    this.attributions.add(attr);
  }

  /**
   * @param attr
   * @return
   */
  public static Attribution with(String attr) {
    return new Attribution(attr);
  }

  /**
   * @return
   */
  public static Attribution hotOSM() {
    return new Attribution("Tiles courtesy of <a href=\"http://hot.openstreetmap.org/\" target=\"_blank\">Humanitarian OpenStreetMap Team</a>");
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();

    builder.append(leaflet);
    builder.append(osm);

    if (this.attributions != null) {
      for (String other : this.attributions) {
        builder.append(other);
      }
    }

    return builder.toString();
  }

  /**
   * @param attr
   */
  public void addAttribution(String attr) {
    if (this.attributions == null) {
      this.attributions = new HashSet<>();
    }

    this.attributions.add(attr);
  }

  /**
   * @return
   */
  public String getLeaflet() {
    return leaflet;
  }

  /**
   * @return
   */
  public String getOsm() {
    return osm;
  }

  /**
   * @return
   */
  public Set<String> getAttributions() {
    if (this.attributions == null) {
      return Collections.emptySet();
    }

    return Collections.unmodifiableSet(this.attributions);
  }
}
