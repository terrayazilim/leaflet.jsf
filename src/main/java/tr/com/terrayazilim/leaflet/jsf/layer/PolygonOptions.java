/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.layer;

import tr.com.terrayazilim.leaflet.jsf.Path;

/**
 * @see <a href="https://leafletjs.com/reference-1.3.0.html#polygon">L.Polygon</a>
 * @see <a href="https://leafletjs.com/reference-1.3.0.html#rectangle">L.Rectangle</a>
 * @see <a href="https://leafletjs.com/reference-1.3.0.html#path">L.Path</a>
 * 
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 * @since 1.0.0
 */
public class PolygonOptions extends RectangleOptions implements LayerOptions {

  public PolygonOptions() {
  }

  /**
   * @param polylineOptions
   */
  public PolygonOptions(PolylineOptions polylineOptions) {
    this.smoothFactor = polylineOptions.getSmoothFactor();
    this.noClip = polylineOptions.getNoClip();
  }
  
  /**
   * @param path 
   */
  public PolygonOptions(Path path) {
	super(path);
  }
}
