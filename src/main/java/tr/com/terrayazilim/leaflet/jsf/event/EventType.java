/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.event;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 * @since 1.0.0
 */
public enum EventType {

  pmcreate("pmcreate"),
  pmremove("pmremove"),
  pmedit("pmedit"),
  dragend("dragend"), // dragend
  click("click"), // click
  contextmenu("contextmenu"), // contextmenu
  mapclick("mapclick"),
  mapcontextmenu("mapcontextmenu"),
  load("load");

  private final String string;

  /**
   * @param string
   */
  private EventType(String string) {
    this.string = string;
  }

  @Override
  public String toString() {
    return this.string;
  }
}
