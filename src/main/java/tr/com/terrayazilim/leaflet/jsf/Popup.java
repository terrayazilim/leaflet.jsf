/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import java.io.Serializable;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public class Popup extends DivOverlay implements Serializable {

  private String content;
  private Integer maxWidth;
  private Integer minWidth;
  private Integer maxHeight;
  private Boolean autoPan;
  private Boolean keepInView;
  private Boolean closeButton;
  private Boolean closeOnClick;
  private Boolean autoClose;

  private Point autoPanPaddingTopLeft;
  private Point autoPanPaddingBottomRight;
  private Point autoPanPadding;
  private Boolean closeOnEscapeKey;

  private Boolean responsive;
  private Boolean hasTip;

  public Popup() {
  }

  /**
   * @param content
   */
  public Popup(String content) {
    this.content = content;
  }

  /**
   * @return
   */
  public String getContent() {
    return content;
  }

  /**
   * @param content
   */
  public void setContent(String content) {
    this.content = content;
  }

  /**
   * @return
   */
  public Integer getMaxWidth() {
    return maxWidth;
  }

  /**
   * @param maxWidth
   */
  public void setMaxWidth(Integer maxWidth) {
    this.maxWidth = maxWidth;
  }

  /**
   * @return
   */
  public Integer getMinWidth() {
    return minWidth;
  }

  /**
   * @param minWidth
   */
  public void setMinWidth(Integer minWidth) {
    this.minWidth = minWidth;
  }

  /**
   * @return
   */
  public Integer getMaxHeight() {
    return maxHeight;
  }

  /**
   * @param maxHeight
   */
  public void setMaxHeight(Integer maxHeight) {
    this.maxHeight = maxHeight;
  }

  /**
   * @return
   */
  public Boolean getAutoPan() {
    return autoPan;
  }

  /**
   * @param autoPan
   */
  public void setAutoPan(Boolean autoPan) {
    this.autoPan = autoPan;
  }

  /**
   * @return
   */
  public Boolean getKeepInView() {
    return keepInView;
  }

  /**
   * @param keepInView
   */
  public void setKeepInView(Boolean keepInView) {
    this.keepInView = keepInView;
  }

  /**
   * @return
   */
  public Boolean getCloseButton() {
    return closeButton;
  }

  /**
   * @param closeButton
   */
  public void setCloseButton(Boolean closeButton) {
    this.closeButton = closeButton;
  }

  /**
   * @return
   */
  public Boolean getCloseOnClick() {
    return closeOnClick;
  }

  /**
   * @param closeOnClick
   */
  public void setCloseOnClick(Boolean closeOnClick) {
    this.closeOnClick = closeOnClick;
  }

  /**
   * @return
   */
  public Boolean getAutoClose() {
    return autoClose;
  }

  /**
   * @param autoClose
   */
  public void setAutoClose(Boolean autoClose) {
    this.autoClose = autoClose;
  }

  /**
   * @return
   */
  public Point getAutoPanPaddingTopLeft() {
    return autoPanPaddingTopLeft;
  }

  /**
   * @param autoPanPaddingTopLeft
   */
  public void setAutoPanPaddingTopLeft(Point autoPanPaddingTopLeft) {
    this.autoPanPaddingTopLeft = autoPanPaddingTopLeft;
  }

  /**
   * @return
   */
  public Point getAutoPanPaddingBottomRight() {
    return autoPanPaddingBottomRight;
  }

  /**
   * @param autoPanPaddingBottomRight
   */
  public void setAutoPanPaddingBottomRight(Point autoPanPaddingBottomRight) {
    this.autoPanPaddingBottomRight = autoPanPaddingBottomRight;
  }

  /**
   * @return
   */
  public Point getAutoPanPadding() {
    return autoPanPadding;
  }

  /**
   * @param autoPanPadding
   */
  public void setAutoPanPadding(Point autoPanPadding) {
    this.autoPanPadding = autoPanPadding;
  }

  /**
   * @return
   */
  public Boolean getCloseOnEscapeKey() {
    return closeOnEscapeKey;
  }

  /**
   * @param closeOnEscapeKey
   */
  public void setCloseOnEscapeKey(Boolean closeOnEscapeKey) {
    this.closeOnEscapeKey = closeOnEscapeKey;
  }

  /**
   * @return
   */
  public Boolean getResponsive() {
    return responsive;
  }

  /**
   * @param responsive
   */
  public void setResponsive(Boolean responsive) {
    this.responsive = responsive;
  }

  /**
   * @return
   */
  public Boolean getHasTip() {
    return hasTip;
  }

  /**
   * @param hasTip
   */
  public void setHasTip(Boolean hasTip) {
    this.hasTip = hasTip;
  }
}
