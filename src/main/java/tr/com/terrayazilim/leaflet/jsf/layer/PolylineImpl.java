/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.layer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import tr.com.terrayazilim.core.Objects;
import tr.com.terrayazilim.gjson.GjsonFeature;
import tr.com.terrayazilim.gjson.GjsonMultiLineString;
import tr.com.terrayazilim.gjson.util.BigLatLonList;
import tr.com.terrayazilim.json.JsonObject;
import tr.com.terrayazilim.json.annotation.Ignored;
import tr.com.terrayazilim.leaflet.jsf.LatLng;
import tr.com.terrayazilim.leaflet.jsf.renderkit.JsonKit;

/**
 * @see <a href="https://leafletjs.com/reference-1.3.0.html#polyline">L.Polyline</a>
 * 
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 * @since 1.0.0
 */
public class PolylineImpl extends AbstractLayer implements Polyline {

  @Ignored
  private static final long serialVersionUID = 598571855127519561L;

  private List<List<LatLng>> latLngs;
  private PolylineOptions options = new PolylineOptions();

  public PolylineImpl() {
	super(LayerType.Polyline);
	this.latLngs = new ArrayList<>();
  }

  /**
   * @param latLngs 
   */
  public PolylineImpl(List<List<LatLng>> latLngs) {
	this();
	this.latLngs = latLngs;
  }

  /**
   * @param latLngs
   * @param options 
   */
  public PolylineImpl(List<List<LatLng>> latLngs, PolylineOptions options) {
	this(latLngs);
	this.options = options;
  }

  /**
   * @param polyline 
   */
  public PolylineImpl(Polyline polyline) {
	this(polyline.getLatLngs(), polyline.getOptions());

	this.options = polyline.getOptions();
  }

  @Override
  public GjsonFeature toGeoJSON() {
	JsonKit jsonKit = new JsonKit();
	
	GjsonMultiLineString gmls = new GjsonMultiLineString();
	Collection<BigLatLonList> stack = new ArrayList<>();
	
	for (List<LatLng> latlngs : getLatLngs()) {
	  BigLatLonList tmp = new BigLatLonList();
	  for (LatLng latlng : latlngs) {
		tmp.add(latlng.getLatLng());
	  }
	  stack.add(tmp);
	}

	gmls.setCoordinates(stack);

	GjsonFeature feature = new GjsonFeature();
	feature.setGeometry(gmls);
	feature.setId(getLayerId());

	JsonObject properties = jsonKit.getJson().toJson(getOptions());
	properties.put("layerType", getLayerType().toString());
	feature.setProperties(properties);

	return feature;
  }

  @Override
  public List<List<LatLng>> getLatLngs() {
	return latLngs;
  }
  
  @Override
  public List<LatLng> getLatLngs(int index) {
	if (Objects.isNull(this.latLngs)) {
	  return null;
	} else if (latLngs.size() <= index) {
	  return null;
	}
	
	return latLngs.get(index);
  }

  @Override
  public void setLatLngs(List<List<LatLng>> latLngs) {
	this.latLngs = latLngs;
  }

  @Override
  public void setLatLngs(int index, List<LatLng> arg) {
	if (Objects.isNull(this.latLngs)) {
	  if (index == 0) {
		this.latLngs = new ArrayList<>();
		latLngs.add(arg);
		return;
	  } else {
		throw new LeafletLayerException("Given index doesn't match with dimension {Hint: ErrorCode:225}");
	  }
	}

	int size = latLngs.size();
	if (index <= size) {
	  latLngs.set(index, arg);
	} else {
	  throw new LeafletLayerException("Given index doesn't match with dimension {Hint: ErrorCode:226}");
	}
  }

  @Override
  public void setLatLngs(int index, LatLng... arg) {
	setLatLngs(index, Arrays.asList(arg));
  }

  @Override
  public boolean addLatLng(int index, LatLng latLng) {
	if (Objects.isNull(this.latLngs)) {
	  this.latLngs = new ArrayList<>();
	}

	int size = latLngs.size();
	if (index < size) {
	  return latLngs.get(index).add(latLng);
	} else if (index == size) {
	  latLngs.add(index, new ArrayList<>());
	  return latLngs.get(index).add(latLng);
	} else {
	  throw new LeafletLayerException("Given index doesn't match with dimension {Hint: ErrorCode:224}");
	}
  }

  @Override
  public boolean addLatLng(LatLng latLng) {
	return addLatLng(0, latLng);
  }

  @Override
  public boolean removeLatLng(int index, LatLng latLng) {
	if (Objects.isNull(this.latLngs)) {
	  this.latLngs = new ArrayList<>();
	  return false;
	} else if (index >= latLngs.size()) {
	  return false;
	}

	return latLngs.get(index).remove(latLng);
  }

  @Override
  public boolean removeLatLng(LatLng latLng) {
	return removeLatLng(0, latLng);
  }

  @Override
  public PolylineOptions getOptions() {
	return options;
  }

  @Override
  public void setOptions(LayerOptions options) {
	if (Objects.nonNull(options) && options instanceof PolylineOptions) {
	  this.options = (PolylineOptions) options;
	}
  }

  @Override
  public boolean isEmpty() {
	if (this.latLngs == null) {
	  return true;
	}

	return latLngs.isEmpty();
  }

  @Override
  public Rectangle toRectangle() {
	Collection<LatLng> stack = new ArrayList<>();
	for (List<LatLng> list : this.latLngs) {
	  for (LatLng next : list) {
		stack.add(next);
	  }
	}
	
	return new RectangleImpl(stack);
  }

  @Override
  public Polyline clone() {
	Polyline polyline = new PolylineImpl(this);
	polyline.setLayerId(getLayerId());
	polyline.setOptionalData(getOptionalData());

	return polyline;
  }
}
