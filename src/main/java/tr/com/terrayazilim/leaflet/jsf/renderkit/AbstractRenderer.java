/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.renderkit;

import java.io.IOException;
import javax.faces.context.ResponseWriter;
import javax.faces.render.Renderer;
import tr.com.terrayazilim.core.Strings;
import tr.com.terrayazilim.core.j2js.FunctionBuilder;
import tr.com.terrayazilim.core.j2js.Parameter;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public abstract class AbstractRenderer extends Renderer {
  
  private static final String SCRIPT = "script";
  private static final String JS_TYPE = "text/javascript";
  
  /**
   * @param responseWriter
   * @throws IOException 
   */
  protected void beginScript(ResponseWriter responseWriter) throws IOException {
    responseWriter.startElement(SCRIPT, null);
  }

  /**
   * @param responseWriter
   * @throws IOException 
   */
  protected void endScript(ResponseWriter responseWriter) throws IOException {
    responseWriter.endElement(SCRIPT);
  }
  
  /**
   * @param responseWriter
   * @throws IOException 
   */
  protected void writeJavascriptAttribute(ResponseWriter responseWriter) throws IOException {
    responseWriter.writeAttribute("type", JS_TYPE, null);
  }
  
  /**
   * @param responseWriter
   * @param context
   * @throws IOException
   */
  protected void writeln(ResponseWriter responseWriter, Object context) throws IOException {
    responseWriter.write(context.toString());
    responseWriter.writeText("\n", null);
  }
  
  /**
   * @param responseWriter
   * @throws IOException 
   */
  protected void writeln(ResponseWriter responseWriter) throws IOException {
    responseWriter.writeText("\n", null);
  }

  /**
   * @param responseWriter
   * @param context
   * @throws IOException 
   */
  protected void write(ResponseWriter responseWriter, Object context) throws IOException {
    if (context != null) {
      responseWriter.write(context.toString());
    }
  }
  
  /**
   * @param objectName
   * @param functionName
   * @param parameter
   * @return
   */
  protected String newJavascriptMethod(String objectName, String functionName, Parameter parameter) {
    return FunctionBuilder.newInstance()
        .setObjectName(objectName)
        .setName(functionName)
        .setParameter(parameter)
        .build()
        .toString();
  }

  /**
   * @param objectName
   * @param functionName
   * @param parameters
   * @return
   */
  protected String newJavascriptMethod(String objectName, String functionName, Parameter... parameters) {
    FunctionBuilder builder = FunctionBuilder.newInstance()
        .setObjectName(objectName)
        .setName(functionName);

    if (parameters != null) {
      for (Parameter parameter : parameters) {
        builder.setParameter(parameter);
      }
    }

    return builder.build().toString();
  }

  /**
   * @param stringLiteral
   * @return
   */
  protected Parameter newJavascriptParameter(String stringLiteral) {
    return Parameter.of(Strings.inQuote(stringLiteral));
  }
}
