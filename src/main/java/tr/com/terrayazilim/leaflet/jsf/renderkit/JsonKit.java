/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.renderkit;

import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.Map;
import tr.com.terrayazilim.core.Strings;
import tr.com.terrayazilim.core.Verify;
import tr.com.terrayazilim.core.math.BigLatitude;
import tr.com.terrayazilim.core.math.BigLongitude;
import tr.com.terrayazilim.json.Json;
import tr.com.terrayazilim.json.JsonAdapter;
import tr.com.terrayazilim.json.JsonArray;
import tr.com.terrayazilim.json.JsonDeserializer;
import tr.com.terrayazilim.json.JsonObject;
import tr.com.terrayazilim.json.JsonPolicy;
import tr.com.terrayazilim.json.JsonSerializer;
import tr.com.terrayazilim.json.JsonValue;
import tr.com.terrayazilim.json.annotation.Ignored;
import tr.com.terrayazilim.json.codec.JsonDeserializerContext;
import tr.com.terrayazilim.json.codec.JsonSerializerContext;
import tr.com.terrayazilim.json.stream.JsonBuffer;
import tr.com.terrayazilim.json.stream.JsonStream;
import tr.com.terrayazilim.json.type.Token;
import tr.com.terrayazilim.leaflet.jsf.Attribution;
import tr.com.terrayazilim.leaflet.jsf.Canvas;
import tr.com.terrayazilim.leaflet.jsf.LatLng;
import tr.com.terrayazilim.leaflet.jsf.LatLngBounds;
import tr.com.terrayazilim.leaflet.jsf.Point;
import tr.com.terrayazilim.leaflet.jsf.Renderer;
import tr.com.terrayazilim.leaflet.jsf.RoughCanvas;
import tr.com.terrayazilim.leaflet.jsf.layer.LeafletLayerException;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 * @since 1.0.0
 */
public final class JsonKit {

  private final Map<Type, JsonAdapter<?>> adapters;
  private final Map<Object, JsonSerializer<?>> serializers;
  private final Map<Object, JsonDeserializer<?>> deserializers;
  
  /**
   * Since the Java models of leaflet doesn't hold default values. Enabling serialization of null
   * values overrides original leaflet object property values. According to this, nullSerialization
   * never should be set true. It must set false.
   */
  private final boolean nullSerialization = false;
  
  /**
   * @see #nullSerialization
   * This doesn't effect anything. So make the Json ignore null values in deserialization.
   */
  private final boolean nullDeserialization = false;
  
  /**
   * Some java models of leaflet contains Enum. So we must enable this option.
   */
  private final boolean enumSerialization = true;
  
  /**
   * @see #enumSerialization
   */
  private final boolean enumDeserialization = true;
  
  /**
   * Most java models of leaflet contains super classes. We must enable this option.
   */
  private final boolean superClassSerialization = true;
  
  /**
   * @see #superClassSerialization
   */
  private final boolean superClassDeserialization = true;
  
  /**
   * We put {@link Ignored} flag to fields in java models of leaflet that we want to ignore. 
   * We do not need any further modifier checking. So set #ignoreModifierChecking to {@code true}
   * for avoiding unnecessary checking and gaining performance.
   */
  private final boolean ignoreModifierChecking = true;
  
  /**
   * {@link Json} instance.
   */
  private final Json json;

  public JsonKit() {
    this.adapters = new LinkedHashMap<>();
    this.serializers = new LinkedHashMap<>();
    this.deserializers = new LinkedHashMap<>();

    adapters.put(new Token<>(LatLng.class).getType(), getLatLngAdapter());
    adapters.put(new Token<>(Point.class).getType(), getPointAdapter());
    adapters.put(new Token<>(LatLngBounds.class).getType(), getLatLngBoundsAdapter());
    
    serializers.put(new Token<>(Attribution.class).getType(), getAttributionSerializer());
    serializers.put(new Token<>(Renderer.class).getType(), getRendererSerializer());
	
	deserializers.put(new Token<>(Renderer.class).getType(), getRendererDeserializer());
    
    this.json = new Json(getBuiltJson());
  }
  
  /**
   * @return {@link tr.com.terrayazilim.json.Json}
   */
  public Json getJson() {
    return this.json;
  }
  
  /**
   * @return 
   */
  private JsonPolicy getBuiltJson() {
    final JsonPolicy policy = new JsonPolicy();
    
    policy.setAdapters(adapters);
    policy.setSerializer(serializers);
    policy.setDeserializers(deserializers);
    
    policy.setNullSerialization(nullSerialization);
    policy.setNullDeserialization(nullDeserialization);
    
    policy.setEnumSerialization(enumSerialization);
    policy.setEnumDeserialization(enumDeserialization);
    
    policy.setSuperClassSerialization(superClassSerialization);
    policy.setSuperClassDeserialization(superClassDeserialization);
    policy.setIgnoreFieldModifiers(ignoreModifierChecking);    
    
    return policy;
  }
  
  /**
   * {@link LatLng} adapter. Format: {"lat":37.7282,"lng":32.23923}.
   *
   * @return
   */
  private JsonAdapter<LatLng> getLatLngAdapter() {
    return new JsonAdapter<LatLng>() {
      @Override
      public void write(JsonBuffer buffer, LatLng latlng) {
        buffer.beginObject()
            .key("lat")
            .value(latlng.lat())
            .key("lng")
            .value(latlng.lon())
            .endObject();
      }

      @Override
      public LatLng read(JsonStream in) {
        final LatLng latlng = new LatLng();
        while (in.next()) {
          switch (in.key()) {
            case "lat":
              latlng.setBigLatitude(BigLatitude.of(in.peekDouble()));
              break;
            case "lng":
              latlng.setBigLongitude(BigLongitude.of(in.nextDouble()));
              break;
          }
        }

        return latlng;
      }
    };
  }

  /**
   * {@link Point} adapter. Format: [x, y].
   *
   * @return
   */
  private JsonAdapter<Point> getPointAdapter() {
    return new JsonAdapter<Point>() {
      @Override
      public void write(JsonBuffer buffer, Point point) {
        Verify.requireNonNull(point.getX(), "X Coordinate is either null. {Hint: ErrorCode:161}");
        Verify.requireNonNull(point.getY(), "Y Coordinate is either null. {Hint: ErrorCode:162}");
        
        buffer.beginArray()
            .value(point.getX())
            .value(point.getY())
            .endArray();
      }

      @Override
      public Point read(JsonStream in) {
        final Point point = new Point();
        
        Verify.requireTrue(in.hasNext(), "Malformed Point Definition. {Hint: ErrorCode:163}");
        point.setX(in.nextDouble());
		
        Verify.requireTrue(in.hasNext(), "Malformed Point Definition. {Hint: ErrorCode:164}");
        point.setY(in.nextDouble());

        return point;
      }
    };
  }

  /**
   * {@link Attribution} serializer.
   *
   * @return
   */
  private JsonSerializer<Attribution> getAttributionSerializer() {
    return new JsonSerializer<Attribution>() {
      @Override
      public void write(JsonBuffer buffer, JsonSerializerContext<Attribution> context) {
        final StringBuilder builder = new StringBuilder();
        final Attribution attr = context.getTarget();
        
        builder.append(attr.getOsm());
        if (attr.getAttributions() != null) {
          for (String other : attr.getAttributions()) {
            builder.append(other);
          }
        }

        buffer.value(builder.toString());
      }
    };
  }
  
  /**
   * {@link Renderer} serializer.
   * 
   * @return 
   */
  private JsonSerializer<Renderer> getRendererSerializer() {
    return new JsonSerializer<Renderer>() {
      @Override
      public void write(JsonBuffer buffer, JsonSerializerContext<Renderer> context) {
        final Renderer renderer = context.getTarget();
        JsonAdapter adapter;
		
        if (renderer instanceof Canvas) {
          Canvas canvas = (Canvas) renderer;
          adapter = context.getJson().getAdapter(new Token<>(canvas.getClass()));
        } else if (renderer instanceof RoughCanvas) {
          RoughCanvas roughCanvas = (RoughCanvas) renderer;
          adapter = context.getJson().getAdapter(new Token<>(roughCanvas.getClass()));
        } else {
          throw new LeafletRendererException("Unknown Renderer. {Hint: ErrorCode:160}");
        }
        
        adapter.write(buffer, renderer);
      }
    };
  }
  
  private static final String ROUGH_CANVAS_TYPE = "roughCanvas";
  
  private static final String DEFAULT_CANVAS_TYPE = "default";
  
  /**
   * {@link Renderer} deserializer.
   * 
   * @return 
   */
  private JsonDeserializer<Renderer> getRendererDeserializer() {
	return new JsonDeserializer<Renderer>() {
	  @Override
	  public Renderer read(JsonStream in, JsonDeserializerContext<Renderer> context) {
		if (!in.isObjectStream()) {
		  throw new LeafletLayerException("Unkown Renderer. {Hint: ErrorCode:281}");
		}
		
		JsonObject object = in.getJsonObject();
		String type = object.optString("type");
		if (Strings.nullOrBlank(type)) {
		  throw new LeafletLayerException("Unkown Renderer. {Hint: ErrorCode:282}");
		}
		
		JsonAdapter adapter;
		switch (type) {
		  case ROUGH_CANVAS_TYPE:
			adapter = context.getJson().getAdapter(new Token<>(RoughCanvas.class));
			break;
		  case DEFAULT_CANVAS_TYPE:
			adapter = context.getJson().getAdapter(new Token<>(Canvas.class));
			break;
		  default:
			throw new LeafletLayerException("Unkown Renderer. {Hint: ErrorCode:283}");
		}
		
		return (Renderer) adapter.read(in);
	  }
	};
  }
  
  /**
   * {@link LatLngBounds} adapter.
   * 
   * @return 
   */
  private JsonAdapter<LatLngBounds> getLatLngBoundsAdapter() {
    return new JsonAdapter<LatLngBounds>() {
      @Override
      public void write(JsonBuffer buffer, LatLngBounds bounds) {
        preCheck(bounds.getSouthWest(), "SouthWest bound is null. {Hint: ErrorCode:165}");
        preCheck(bounds.getNorthEast(), "NorthEast bound is null. {Hint: ErrorCode:166}");
        buffer.beginArray();
        
        buffer.beginArray()
            .value(bounds.getSouthWest().lat())
            .value(bounds.getSouthWest().lon())
            .endArray();
        
        buffer.beginArray()
            .value(bounds.getNorthEast().lat())
            .value(bounds.getNorthEast().lon())
            .endArray();
        
        buffer.endArray();
      }

      @Override
      public LatLngBounds read(JsonStream in) {
        final LatLngBounds bounds = new LatLngBounds();

		LatLng sw = null;
        LatLng ne = null;
        while (in.next()) {          
          switch (in.getStatus()) {
            case Array: {
              JsonArray array = in.peekArray();
              Verify.requireTrue(array.length() == 2, "Malformed LatLngBounds Json. {Hint: ErrorCode:167}");
              if (sw == null) {
                sw = new LatLng();
                sw.setLatLng(array.getDouble(0), array.getDouble(1));
              } else if (ne == null) {
                ne = new LatLng();
                ne.setLatLng(array.getDouble(0), array.getDouble(1));
              }
            } break;
          }
        }
        
        bounds.setSouthWest(sw);
        bounds.setNorthEast(ne);
        return bounds;
      }
    };
  }
  
  /**
   * @param latlng
   * @param message 
   */
  static void preCheck(LatLng latlng, String message) {
    Verify.requireNonNull(latlng, message);
    
    Verify.requireTrue(latlng.lat().isNaN(), message);
    Verify.requireTrue(latlng.lon().isNaN(), message);
  }
}
