/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.layer;

import tr.com.terrayazilim.leaflet.jsf.Pane;
import tr.com.terrayazilim.leaflet.jsf.Path;
import tr.com.terrayazilim.leaflet.jsf.Popup;
import tr.com.terrayazilim.leaflet.jsf.Renderer;
import tr.com.terrayazilim.leaflet.jsf.Tooltip;

/**
 * @see <a href="https://leafletjs.com/reference-1.3.0.html#circle">L.Circle</a>
 * @see <a href="https://leafletjs.com/reference-1.3.0.html#path">L.Path</a>
 * 
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public class CircleOptions extends Path implements LayerOptions {

  private Double radius;

  private Boolean interactive;
  private Tooltip tooltip;
  private Popup popup;
  private Pane pane;

  public CircleOptions() {
    super();
  }

  /**
   * @param radius
   */
  public CircleOptions(Double radius) {
    this.radius = radius;
  }

  /**
   * @param options
   */
  public CircleOptions(Path options) {
    super(options);
  }

  /**
   * @return
   */
  public Double getRadius() {
    return radius;
  }

  /**
   * @param radius
   */
  public void setRadius(Double radius) {
    this.radius = radius;
  }

  @Override
  public Boolean getInteractive() {
    return interactive;
  }

  @Override
  public void setInteractive(Boolean interactive) {
    this.interactive = interactive;
  }

  @Override
  public Tooltip getTooltip() {
    return tooltip;
  }

  @Override
  public void setTooltip(Tooltip tooltip) {
    this.tooltip = tooltip;
  }

  @Override
  public Popup getPopup() {
    return popup;
  }

  @Override
  public void setPopup(Popup popup) {
    this.popup = popup;
  }

  @Override
  public Pane getPane() {
    return pane;
  }

  @Override
  public void setPane(Pane pane) {
    this.pane = pane;
  }

  @Override
  public Renderer getRenderer() {
    return renderer;
  }

  @Override
  public void setRenderer(Renderer renderer) {
    this.renderer = renderer;
  }

  /**
   * @return
   */
  @Override
  public CircleOptions clone() {
    return new CircleOptions(this);
  }
}
