/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.layer;

import tr.com.terrayazilim.core.Objects;
import tr.com.terrayazilim.gjson.GjsonFeature;
import tr.com.terrayazilim.gjson.GjsonPoint;
import tr.com.terrayazilim.json.JsonObject;
import tr.com.terrayazilim.json.annotation.Ignored;
import tr.com.terrayazilim.leaflet.jsf.LatLng;
import tr.com.terrayazilim.leaflet.jsf.renderkit.JsonKit;

/**
 * @see <a href="https://leafletjs.com/reference-1.3.0.html#marker">L.Marker</a>
 * 
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 * @since 1.0.0
 */
public class MarkerImpl extends AbstractLayer implements Marker {

  @Ignored
  private static final long serialVersionUID = 5957320257262561L;

  private LatLng latLng;
  private MarkerOptions options = new MarkerOptions();

  public MarkerImpl() {
    super(LayerType.Marker);
  }

  public MarkerImpl(LatLng latLng) {
    this();
    this.latLng = latLng;
  }

  public MarkerImpl(LatLng latLng, MarkerOptions options) {
    this(latLng);

    if (Objects.nonNull(options)) {
      this.options = options;
    }
  }

  public MarkerImpl(Marker marker) {
    this(marker.getLatLng());

    this.latLng = marker.getLatLng();
    this.options = marker.getOptions();
  }

  @Override
  public GjsonFeature toGeoJSON() {
	JsonKit jsonKit = new JsonKit();
	
	GjsonPoint point = new GjsonPoint();
	point.setLat(getLatLng().getBigLatitude());
	point.setLon(getLatLng().getBigLongitude());
	
	GjsonFeature feature = new GjsonFeature();
	feature.setId(getLayerId());
	feature.setGeometry(point);
	
	JsonObject properties = jsonKit.getJson().toJson(this.options);
	properties.put("layerType", getLayerType().toString());
	feature.setProperties(properties);
	
	return feature;
  }
  
  @Override
  public void setLatLng(LatLng latLng) {
    this.latLng = latLng;
  }

  @Override
  public LatLng getLatLng() {
    return this.latLng;
  }

  @Override
  public void setOptions(LayerOptions options) {
    if (Objects.nonNull(options) && options instanceof MarkerOptions) {
      this.options = (MarkerOptions) options;
    }
  }

  @Override
  public MarkerOptions getOptions() {
    if (Objects.isNull(this.options)) {
      this.options = new MarkerOptions();
    }

    return this.options;
  }

  @Override
  public Marker clone() {
    Marker marker = new MarkerImpl(this);
    marker.setLayerId(getLayerId());
    marker.setOptionalData(getOptionalData());
    
    return marker;
  }
}
