/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import tr.com.terrayazilim.json.annotation.Ignored;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public class Icon extends AbstractIcon {

  @Ignored
  public static final String COTTON = "http://i.hizliresim.com/2aQk0d.png";

  @Ignored
  public static final String ULTRAVIOLET = "http://i.hizliresim.com/mo8z9Z.png";

  @Ignored
  public static final String MODERN_RED = "http://i.hizliresim.com/8zEbyW.png";

  @Ignored
  public static final String DUSK = "http://i.hizliresim.com/DDkRvm.png";

  @Ignored
  public static final String NOLAN = "http://i.hizliresim.com/EDEq1B.png";

  @Ignored
  public static final String DOODLE = "http://i.hizliresim.com/j6Y27D.png";

  public Icon() {
    this(COTTON);
  }

  /**
   * @param iconUrl
   */
  public Icon(String iconUrl) {
    this.iconUrl = iconUrl;
  }

  /**
   * @return 
   */
  public static final Icon ultraviolet() {
    return new Icon(ULTRAVIOLET);
  }

  /**
   * @return 
   */
  public static final Icon modernRed() {
    return new Icon(MODERN_RED);
  }

  /**
   * @return 
   */
  public static final Icon dusk() {
    return new Icon(DUSK);
  }

  /**
   * @return 
   */
  public static final Icon nolan() {
    return new Icon(NOLAN);
  }

  /**
   * @return 
   */
  public static final Icon doodle() {
    return new Icon(DOODLE);
  }
}
