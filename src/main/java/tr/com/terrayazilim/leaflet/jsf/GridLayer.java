/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import java.io.Serializable;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public class GridLayer implements Serializable {

  private Integer tileSize;
  private Double opacity;
  private Boolean updateWhenIdle;
  private Boolean updateWhenZooming;
  private Integer updateInterval;
  private Integer zIndex;
  private LatLngBounds bounds;
  private Integer minZoom;
  private Integer maxZoom;
  private Integer maxNativeZoom;
  private Integer minNativeZoom;
  private Boolean noWrap;
  private Pane pane;
  private String className;
  private Integer keepBuffer;
  private Attribution attribution;

  public GridLayer() {
  }

  /**
   * @return
   */
  public Integer getTileSize() {
    return tileSize;
  }

  /**
   * @param tileSize
   */
  public void setTileSize(Integer tileSize) {
    this.tileSize = tileSize;
  }

  /**
   * @return
   */
  public Double getOpacity() {
    return opacity;
  }

  /**
   * @param opacity
   */
  public void setOpacity(Double opacity) {
    this.opacity = opacity;
  }

  /**
   * @return
   */
  public Boolean getUpdateWhenIdle() {
    return updateWhenIdle;
  }

  /**
   * @param updateWhenIdle
   */
  public void setUpdateWhenIdle(Boolean updateWhenIdle) {
    this.updateWhenIdle = updateWhenIdle;
  }

  /**
   * @return
   */
  public Boolean getUpdateWhenZooming() {
    return updateWhenZooming;
  }

  /**
   * @param updateWhenZooming
   */
  public void setUpdateWhenZooming(Boolean updateWhenZooming) {
    this.updateWhenZooming = updateWhenZooming;
  }

  /**
   * @return
   */
  public Integer getUpdateInterval() {
    return updateInterval;
  }

  /**
   * @param updateInterval
   */
  public void setUpdateInterval(Integer updateInterval) {
    this.updateInterval = updateInterval;
  }

  /**
   * @return
   */
  public Integer getzIndex() {
    return zIndex;
  }

  /**
   * @param zIndex
   */
  public void setzIndex(Integer zIndex) {
    this.zIndex = zIndex;
  }

  /**
   * @return
   */
  public LatLngBounds getBounds() {
    return bounds;
  }

  /**
   * @param bounds
   */
  public void setBounds(LatLngBounds bounds) {
    this.bounds = bounds;
  }

  /**
   * @return
   */
  public Integer getMinZoom() {
    return minZoom;
  }

  /**
   * @param minZoom
   */
  public void setMinZoom(Integer minZoom) {
    this.minZoom = minZoom;
  }

  /**
   * @return
   */
  public Integer getMaxZoom() {
    return maxZoom;
  }

  /**
   * @param maxZoom
   */
  public void setMaxZoom(Integer maxZoom) {
    this.maxZoom = maxZoom;
  }

  /**
   * @return
   */
  public Integer getMaxNativeZoom() {
    return maxNativeZoom;
  }

  /**
   * @param maxNativeZoom
   */
  public void setMaxNativeZoom(Integer maxNativeZoom) {
    this.maxNativeZoom = maxNativeZoom;
  }

  /**
   * @return
   */
  public Integer getMinNativeZoom() {
    return minNativeZoom;
  }

  /**
   * @param minNativeZoom
   */
  public void setMinNativeZoom(Integer minNativeZoom) {
    this.minNativeZoom = minNativeZoom;
  }

  /**
   * @return
   */
  public Boolean getNoWrap() {
    return noWrap;
  }

  /**
   * @param noWrap
   */
  public void setNoWrap(Boolean noWrap) {
    this.noWrap = noWrap;
  }

  /**
   * @return
   */
  public Pane getPane() {
    return pane;
  }

  /**
   * @param pane
   */
  public void setPane(Pane pane) {
    this.pane = pane;
  }

  /**
   * @return
   */
  public String getClassName() {
    return className;
  }

  /**
   * @param className
   */
  public void setClassName(String className) {
    this.className = className;
  }

  /**
   * @return
   */
  public Integer getKeepBuffer() {
    return keepBuffer;
  }

  /**
   * @param keepBuffer
   */
  public void setKeepBuffer(Integer keepBuffer) {
    this.keepBuffer = keepBuffer;
  }

  /**
   * @return
   */
  public Attribution getAttribution() {
    return attribution;
  }

  /**
   * @param attribution
   */
  public void setAttribution(Attribution attribution) {
    this.attribution = attribution;
  }
}
