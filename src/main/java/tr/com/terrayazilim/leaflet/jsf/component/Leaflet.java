/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.faces.component.FacesComponent;
import javax.faces.component.UIComponent;
import javax.faces.component.UIComponentBase;
import javax.faces.component.behavior.Behavior;
import javax.faces.component.behavior.ClientBehaviorHolder;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.FacesEvent;
import tr.com.terrayazilim.core.Objects;
import tr.com.terrayazilim.core.Strings;
import tr.com.terrayazilim.core.Verify;
import tr.com.terrayazilim.core.annotation.Issue;
import tr.com.terrayazilim.core.tuple.Pair;
import tr.com.terrayazilim.json.JsonArray;
import tr.com.terrayazilim.json.JsonException;
import tr.com.terrayazilim.json.JsonObject;
import tr.com.terrayazilim.leaflet.jsf.L;
import tr.com.terrayazilim.leaflet.jsf.LatLng;
import tr.com.terrayazilim.leaflet.jsf.MapModel;
import tr.com.terrayazilim.leaflet.jsf.event.ClickEvent;
import tr.com.terrayazilim.leaflet.jsf.event.ContextMenuEvent;
import tr.com.terrayazilim.leaflet.jsf.event.DragendEvent;
import tr.com.terrayazilim.leaflet.jsf.event.EventType;
import tr.com.terrayazilim.leaflet.jsf.event.MapClickEvent;
import tr.com.terrayazilim.leaflet.jsf.event.MapContextMenuEvent;
import tr.com.terrayazilim.leaflet.jsf.event.PmCreateEvent;
import tr.com.terrayazilim.leaflet.jsf.event.PmEditEvent;
import tr.com.terrayazilim.leaflet.jsf.event.PmRemoveEvent;
import tr.com.terrayazilim.leaflet.jsf.layer.Circle;
import tr.com.terrayazilim.leaflet.jsf.layer.Layer;
import tr.com.terrayazilim.leaflet.jsf.layer.LayerType;
import tr.com.terrayazilim.leaflet.jsf.layer.Marker;
import tr.com.terrayazilim.leaflet.jsf.layer.Polygon;
import tr.com.terrayazilim.leaflet.jsf.layer.Polyline;
import tr.com.terrayazilim.leaflet.jsf.layer.Rectangle;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
@FacesComponent(
        createTag = true,
        namespace = "http://www.terrayazilim.com.tr/leaflet",
        tagName = "leaflet",
        value = "tr.com.terrayazilim.leaflet.jsf.component.Leaflet"
)
public class Leaflet extends UIComponentBase implements ClientBehaviorHolder {

  private static final String MAP_DEFAULT_RENDERER = "tr.com.terrayazilim.leaflet.jsf.renderkit.LeafletRenderer";
  private static final String MAP_COMPONENT_FAMILY = "tr.com.terrayazilim.leaflet.jsf.component";
  private static final String BEHAVIOR_EVENT = "javax.faces.behavior.event";
  private static final String SOURCE = "javax.faces.source";

  private static final String PARTIAL_EXEC = "javax.faces.partial.execute";
  private static final String PARTIAL_RENDER = "javax.faces.partial.render";

  private static final String ENCODE_URL = "javax.faces.encodedURL";

  private static final String VIEW_ROOT = "javax.faces.ViewRoot";
  private static final String VIEW_STATE = "javax.faces.ViewState";
  private static final String VIEW_HEAD = "javax.faces.ViewHead";
  private static final String VIEW_BODY = "javax.faces.ViewBody";

  @Issue(
          category = Issue.Type.ENHANCEMENT,
          content = "Using public {@link UIComponentBase#setRendererType} in constructor could be"
          + "dangerous. Because {@link UIComponentBase#setRendererType} can be overriden in"
          + "the classes that extends Leaflet. According to this, I believe we can use"
          + "{@code getStateHelper().put(PropertyKeys.rendererType, rendererType)} as an"
          + "alternative.",
          isFixed = false
  )
  public Leaflet() {
    // setRendererType method can be overriden. (Is there a better way ?)
    // As a Response above;
    // In UIComponentBase, these done with the following
    // getStateHelper().put(PropertyKeys.rendererType, rendererType);
    setRendererType(MAP_DEFAULT_RENDERER);
  }

  /**
   * Method does return the event names that declared in {@link EventType}.
   *
   * @return Declared Event names as a Collection of Strings.
   */
  @Override
  public Collection<String> getEventNames() {
    final List<String> eventNames = new ArrayList<>();

    for (EventType eachEvent : EventType.values()) {
      eventNames.add(eachEvent.toString());
    }

    return eventNames;
  }

  /**
   * @return
   */
  @Override
  public String getFamily() {
    return MAP_COMPONENT_FAMILY;
  }

  /**
   * @see UIComponent#PropertyKeys
   * 
   * @version 1.0.1
   * @since 1.0.0
   */
  protected enum PropertyKeys {

    pm("pm"),
    polylineMeasure("polylineMeasure"),
    fullScreen("fullScreen"),
    boxZoom("boxZoom"),
    easyPrint("easyPrint"),
    roughCanvas("roughCanvas"),
    responsive("responsive"),
    mapmodel("mapmodel"),
    style("style");

    private final String string;

    /**
     * @param string
     */
    private PropertyKeys(String string) {
      this.string = string;
    }

    @Override
    public String toString() {
      return string;
    }
  }

  /**
   * @param pm
   */
  public void setPm(Boolean pm) {
    getStateHelper().put(PropertyKeys.pm, pm);
  }

  /**
   * @return Value of pm in xhtml. If pm doesn't declared it returns null.
   */
  public Boolean getPm() {
    return (Boolean) getStateHelper().eval(PropertyKeys.pm, null);
  }

  /**
   * @param polylinemeasure
   */
  public void setPolylineMeasure(Boolean polylinemeasure) {
    getStateHelper().put(PropertyKeys.polylineMeasure, polylinemeasure);
  }

  /**
   * @return Value of polylineMeasure in xhtml. If polylineMeasure doesn't declared it returns null.
   */
  public Boolean getPolylineMeasure() {
    return (Boolean) getStateHelper().eval(PropertyKeys.polylineMeasure, null);
  }

  /**
   * @param fullscreen
   */
  public void setFullScreen(Boolean fullscreen) {
    getStateHelper().put(PropertyKeys.fullScreen, fullscreen);
  }

  /**
   * @return Value of fullScreen in xhtml. If fullScreen doesn't declared it returns null.
   */
  public Boolean getFullScreen() {
    return (Boolean) getStateHelper().eval(PropertyKeys.fullScreen, null);
  }

  /**
   * @param boxzoom
   */
  public void setBoxZoom(Boolean boxzoom) {
    getStateHelper().put(PropertyKeys.boxZoom, boxzoom);
  }

  /**
   * @return Value of boxZoom in xhtml. If boxZoom doesn't declared it returns null.
   */
  public Boolean getBoxZoom() {
    return (Boolean) getStateHelper().eval(PropertyKeys.boxZoom, null);
  }

  /**
   * @param easyPrint
   */
  public void setEasyPrint(Boolean easyPrint) {
    getStateHelper().put(PropertyKeys.easyPrint, easyPrint);
  }

  /**
   * @return Value of easyPrint in xhtml. If easyPrint doesn't declared it returns null.
   */
  public Boolean getEasyPrint() {
    return (Boolean) getStateHelper().eval(PropertyKeys.easyPrint, null);
  }

  /**
   * @param roughCanvas
   */
  public void setRoughCanvas(Boolean roughCanvas) {
    getStateHelper().put(PropertyKeys.roughCanvas, roughCanvas);
  }

  /**
   * @return Value of roughCanvas in xhtml. If roughCanvas doesn't declared it returns null.
   */
  public Boolean getRoughCanvas() {
    return (Boolean) getStateHelper().eval(PropertyKeys.roughCanvas, null);
  }

  /**
   * @param responsive
   */
  public void setResponsive(Boolean responsive) {
    getStateHelper().put(PropertyKeys.responsive, responsive);
  }

  /**
   * @return Value of responsive in xhtml. If responsive doesn't declared it returns null.
   */
  public Boolean getResponsive() {
    return (Boolean) getStateHelper().eval(PropertyKeys.responsive, null);
  }

  /**
   * @param mapmodel
   */
  public void setMapmodel(MapModel mapmodel) {
    getStateHelper().put(PropertyKeys.mapmodel, mapmodel);
  }

  /**
   * @return Value of mapmodel in xhtml. If mapmodel doesn't declared it returns null.
   */
  public MapModel getMapmodel() {
    return (MapModel) getStateHelper().eval(PropertyKeys.mapmodel, null);
  }

  /**
   * @param style
   */
  public void setStyle(String style) {
    getStateHelper().put(PropertyKeys.style, style);
  }

  /**
   * @return Value of style in xhtml. If style doesn't declared it returns null.
   */
  public String getStyle() {
    return (String) getStateHelper().eval(PropertyKeys.style, null);
  }

  @Override
  public void queueEvent(FacesEvent event) {
    FacesContext facesContext = this.getFacesContext();
    Map<String, String> parameter = facesContext.getExternalContext().getRequestParameterMap();
    String behaviorEventName = parameter.get(BEHAVIOR_EVENT);
    String clientId = this.getClientId(facesContext);
    String eventSourceId = parameter.get(SOURCE);

    // Magic. Do not touch.
    try {
      if (clientId.equals(eventSourceId)) {
        AjaxBehaviorEvent behaviorEvent = (AjaxBehaviorEvent) event;
        FacesEvent catchedEvent = null;

        if (behaviorEventName.equals(EventType.dragend.toString())) {
          catchedEvent = dragendEvent(behaviorEvent.getBehavior());
        } else if (behaviorEventName.equals(EventType.click.toString())) {
          catchedEvent = layerClickEvent(behaviorEvent.getBehavior(), EventType.click);
        } else if (behaviorEventName.equals(EventType.contextmenu.toString())) {
          catchedEvent = layerClickEvent(behaviorEvent.getBehavior(), EventType.contextmenu);
        } else if (behaviorEventName.equals(EventType.mapclick.toString())) {
          catchedEvent = mapClickEvent(behaviorEvent.getBehavior(), EventType.mapclick);
        } else if (behaviorEventName.equals(EventType.mapcontextmenu.toString())) {
          catchedEvent = mapClickEvent(behaviorEvent.getBehavior(), EventType.mapcontextmenu);
        } else if (behaviorEventName.equals(EventType.pmcreate.toString())) {
          catchedEvent = pmcreateEvent(behaviorEvent.getBehavior());
        } else if (behaviorEventName.equals(EventType.pmremove.toString())) {
          catchedEvent = pmremoveEdit(behaviorEvent.getBehavior());
        } else if (behaviorEventName.equals(EventType.pmedit.toString())) {
          catchedEvent = pmeditEvent(behaviorEvent.getBehavior());
        }

        /**
         * Workaround for javax.faces.event.AjaxBehaviorEvent MethodNotFoundException. When we add any layer to the Map
         * without updating the component, any interaction on new added layer throws above Exception as expected.
         */
        if (catchedEvent instanceof tr.com.terrayazilim.leaflet.jsf.event.Event) {
          tr.com.terrayazilim.leaflet.jsf.event.Event tmp = (tr.com.terrayazilim.leaflet.jsf.event.Event) catchedEvent;
          if (tmp.isSucceed()) {
            catchedEvent.setPhaseId(behaviorEvent.getPhaseId());
            super.queueEvent(catchedEvent);
          }
        }
      } else {
        super.queueEvent(event);
      }
    } catch (Exception e) {
      throw new LeafletException("Event couldn't join the queue. {Hint: ErrorCode:192}", e);
    }
  }

  /**
   * @see leaflet.jsf.js#L.jsf.map.request
   */
  private static final String JSON_PREFIX = "_json";

  /**
   * @see leaflet.jsf.js#L.jsf.layerIdPrefix
   */
  private static final String LAYERID_PREFIX = "::LayerId";

  /**
   * @see leaflet.jsf.js#L.jsf.synchPrefix
   */
  private static final String SYNCH_PREFIX = "::Synch";

  /**
   * @see leaflet.jsf.js#L.jsf.requestPrefix
   */
  private static final String REQUEST_PREFIX = "::Request";

  /**
   * @see leaflet.jsf.js#L.jsf.uuidPrefix
   */
  private static final String UUID_PREFIX = "::Uuid";

  /**
   * @see leaflet.jsf.js#L.jsf.datePrefix
   */
  private static final String DATE_PREFIX = "::Date";

  /**
   * @see leaflet.jsf.js#L.jsf.timePrefix
   */
  private static final String TIME_PREFIX = "::Time";

  /**
   * @see leaflet.jsf.js#L.jsf.distancePrefix
   */
  private static final String DISTANCE_PREFIX = "::Distance";

  /**
   * @see leaflet.jsf.js#L.jsf.dimensionPrefix
   */
  private static final String DIMENSION_PREFIX = "::Dimension";

  /**
   * @see leaflet.jsf.js#L.jsf.latPrefix
   */
  private static final String LATITUDE_PREFIX = "::Latitude";

  /**
   * @see leaflet.jsf.js#L.jsf.lngPrefix
   */
  private static final String LONGITUDE_PREFIX = "::Longitude";

  /**
   * @see leaflet.jsf.js#L.jsf.latlngsPrefix
   */
  private static final String LATLNGS_PREFIX = "::LatLngs";

  /**
   * @see leaflet.jsf.js#L.jsf.radiusPrefix
   */
  private static final String RADIUS_PREFIX = "::Radius";

  /**
   * @see leaflet.jsf.js#L.jsf.eventPrefix
   */
  private static final String EVENT_PREFIX = "::Event";

  /**
   * @see leaflet.jsf.js#L.jsf.seperatorPrefix
   */
  private static final String SEPERATOR_PREFIX = ";;";

  /**
   * @version 1.0.1
   * @since 1.0.1
   */
  static private class LazyEventResolver {

    private final JsonObject context;
    private final String clientId;

    private String layerId;
    private JsonArray latlngs;
    private Double latitude;
    private Double longitude;
    private Integer dimension;
    private Double distance;
    private Double radius;
    private String date;
    private String time;

    public LazyEventResolver(String contextJson, String clientDivId) {
      Verify.requireNonNull(contextJson, "Malformed Empty Json. {Hint: ErrorCode:180}");
      Verify.requireNonNull(clientDivId, "Malformed Empty Client Id. {Hint: ErrorCode:181}");

      this.context = new JsonObject(contextJson);
      this.clientId = clientDivId;
    }

    public String getLayerId() {
      if (this.layerId != null) {
        return this.layerId;
      }

      this.layerId = context.optString(this.clientId + LAYERID_PREFIX);
      return this.layerId;
    }

    public JsonArray getLatLngs() {
      if (this.latlngs != null) {
        return this.latlngs;
      }

      this.latlngs = context.getJSONArray(this.clientId + LATLNGS_PREFIX);
      return this.latlngs;
    }

    public Double getLatitude() {
      if (this.latitude != null) {
        return this.latitude;
      }

      String lat = context.optString(this.clientId + LATITUDE_PREFIX);
      this.latitude = (lat != null) ? Double.valueOf(lat) : null;

      return this.latitude;
    }

    public Double getLongitude() {
      if (this.longitude != null) {
        return this.longitude;
      }

      String lng = context.optString(this.clientId + LONGITUDE_PREFIX);
      this.longitude = (lng != null) ? Double.valueOf(lng) : null;

      return this.longitude;
    }

    public Integer getDimension() {
      if (this.dimension != null) {
        return this.dimension;
      }

      String dim = context.optString(this.clientId + DIMENSION_PREFIX);
      this.dimension = (dim != null) ? Integer.valueOf(dim) : null;

      return this.dimension;
    }

    public Double getDistance() {
      if (this.distance != null) {
        return this.distance;
      }

      this.distance = context.optDouble(this.clientId + DISTANCE_PREFIX);
      return this.distance;
    }

    public Double getRadius() {
      if (this.radius != null) {
        return this.radius;
      }

      this.radius = context.optDouble(this.clientId + RADIUS_PREFIX);
      return this.radius;
    }

    public String getDate() {
      if (this.date != null) {
        return this.date;
      }

      this.date = context.optString(this.clientId + DATE_PREFIX);
      return this.date;
    }

    public String getTime() {
      if (this.time != null) {
        return this.time;
      }

      this.time = context.optString(this.clientId + TIME_PREFIX);
      return this.time;
    }
  }

  /**
   * @param behavior Behavior
   * @return FacesEvent DragendEvent
   */
  protected DragendEvent dragendEvent(Behavior behavior) {
    FacesContext facesContext = this.getFacesContext();
    Map<String, String> parameters = facesContext.getExternalContext().getRequestParameterMap();
    String clientId = this.getClientId(facesContext);
    String context = parameters.get(Strings.join(clientId, JSON_PREFIX));

    LazyEventResolver resolver = new LazyEventResolver(context, clientId);
    Marker updated = null;
    if (Objects.isNull(getMapmodel())) {
      return new DragendEvent(this, behavior); // isSucceed() gonna be false.
    }

    try {
      Marker outdated = getMapmodel().getMarker(resolver.getLayerId());
      if (Objects.isNull(outdated)) {
        return new DragendEvent(this, behavior); // isSucceed() gonna be false.
      }

      updated = outdated.clone();
      updated.setLatLng(new LatLng(resolver.getLatitude(), resolver.getLongitude()));
      updated.setLayerId(resolver.getLayerId());
    } catch (Exception e) {
      throw new LeafletException("Malformed Dragend Event Context. {Hint: ErrorCode:187}", e);
    }

    final DragendEvent event = new DragendEvent.Builder()
            .setBehavior(behavior)
            .setComponent(this)
            .setTarget(updated)
            .setDistance(resolver.getDistance())
            .setDate(Strings.getIfNull(resolver.getDate(), ""))
            .setTime(Strings.getIfNull(resolver.getTime(), ""))
            .build();

    return event;
  }

  /**
   * @param behavior Behavior
   * @param eventType EventType
   * @return FacesEvent ClickEvent | ContextMenuEvent | MapClickEvent | MapContextMenuEvent
   */
  protected FacesEvent verifiedClickEvent(Behavior behavior, EventType eventType) {
    switch (eventType) {
      case click:
        return new ClickEvent(this, behavior);
      case contextmenu:
        return new ContextMenuEvent(this, behavior);
      case mapclick:
        return new MapClickEvent(this, behavior);
      case mapcontextmenu:
        return new MapContextMenuEvent(this, behavior);
      default:
        throw new RuntimeException("Unpected Event Type. Expected Event Types: "
                + "ClickEvent, ContextMenuEvent, MapClickEvent, MapContextMenuEvent."
                + "{Hint: ErrorCode:182}");
    } // All in all you're just another brick in the wall.
  }

  /**
   * @param behavior Behavior
   * @param eventType EventType
   * @return FacesEvent ClickEvent | ContextMenuEvent
   */
  protected FacesEvent layerClickEvent(Behavior behavior, EventType eventType) {
    FacesContext facesContext = this.getFacesContext();
    Map<String, String> parameters = facesContext.getExternalContext().getRequestParameterMap();
    String clientId = this.getClientId(facesContext);
    String context = parameters.get(Strings.join(clientId, JSON_PREFIX));

    LazyEventResolver resolver = new LazyEventResolver(context, clientId);
    if (Objects.isNull(getMapmodel())) {
      return verifiedClickEvent(behavior, eventType);     // isSucceed() gonna be false.
    }

    final Layer target = getMapmodel().get(resolver.getLayerId());
    if (target == null) {
      return verifiedClickEvent(behavior, eventType);     // isSucceed() gonna be false.
    }

    tr.com.terrayazilim.leaflet.jsf.event.Event event;
    switch (eventType) {
      case click:
        event = new ClickEvent.Builder()
                .setBehavior(behavior)
                .setComponent(this)
                .setTarget(target.clone())
                .setDate(Strings.getIfNull(resolver.getDate(), ""))
                .setTime(Strings.getIfNull(resolver.getTime(), ""))
                .build();
        break;
      case contextmenu:
        event = new ContextMenuEvent.Builder()
                .setBehavior(behavior)
                .setComponent(this)
                .setTarget(target.clone())
                .setDate(Strings.getIfNull(resolver.getDate(), ""))
                .setTime(Strings.getIfNull(resolver.getTime(), ""))
                .build();
        break;
      default:
        throw new RuntimeException("Unpected Event Type. Expected Event Types: "
                + "ClickEvent, ContextMenuEvent. "
                + "{Hint: ErrorCode:183}");
    }

    // No dark sarcasm in the classroom.
    return event;
  }

  /**
   * @param behavior Behavior
   * @param eventType EventType
   * @return FacesEvent ClickEvent | ContextMenuEvent
   */
  protected FacesEvent mapClickEvent(Behavior behavior, EventType eventType) {
    FacesContext facesContext = this.getFacesContext();
    Map<String, String> parameters = facesContext.getExternalContext().getRequestParameterMap();
    String clientId = this.getClientId(facesContext);
    String context = parameters.get(Strings.join(clientId, JSON_PREFIX));

    LazyEventResolver resolver = new LazyEventResolver(context, clientId);
    if (Objects.isNull(getMapmodel())) {
      return verifiedClickEvent(behavior, eventType); // isSucceed() gonna be false.
    }

    tr.com.terrayazilim.leaflet.jsf.event.Event event;
    final LatLng latlng = L.latLng(resolver.getLatitude(), resolver.getLongitude());
    switch (eventType) {
      case mapclick:
        event = new MapClickEvent.Builder()
                .setBehavior(behavior)
                .setComponent(this)
                .setTarget(latlng)
                .setDate(Strings.getIfNull(resolver.getDate(), ""))
                .setTime(Strings.getIfNull(resolver.getTime(), ""))
                .build();
        break;
      case mapcontextmenu:
        event = new MapContextMenuEvent.Builder()
                .setBehavior(behavior)
                .setComponent(this)
                .setTarget(latlng)
                .setDate(Strings.getIfNull(resolver.getDate(), ""))
                .setTime(Strings.getIfNull(resolver.getTime(), ""))
                .build();
        break;
      default:
        throw new RuntimeException("Unpected Event Type. Expected Types: "
                + "ClickEvent, ContextMenuEvent. "
                + "{Hint: ErorCode:185}");
    }

    return event;
  }

  /**
   * @version 1.0.1
   * @since 1.0.1
   */
  static private class LazyRectangleResolver {

    private static final String SOUTHWEST_PREFIX = "_sw";
    private static final String NORTHEAST_PREFIX = "_ne";

    private final JsonObject context;
    private final String clientId;

    private Pair<Double, Double> southWest;
    private Pair<Double, Double> northEast;

    public LazyRectangleResolver(JsonObject context, String clientId) {
      this.context = context;
      this.clientId = clientId;
    }

    public Pair<Double, Double> getSouthWestPair() {
      if (this.southWest != null) {
        return this.southWest;
      }

      String swlat = context.optString(clientId + SOUTHWEST_PREFIX + LATITUDE_PREFIX);
      String swlng = context.optString(clientId + SOUTHWEST_PREFIX + LONGITUDE_PREFIX);
      this.southWest = Pair.of(Double.valueOf(swlat), Double.valueOf(swlng));

      return southWest;
    }

    public Pair<Double, Double> getNorthEastPair() {
      if (this.northEast != null) {
        return this.northEast;
      }

      String nelat = context.optString(clientId + NORTHEAST_PREFIX + LATITUDE_PREFIX);
      String nelng = context.optString(clientId + NORTHEAST_PREFIX + LONGITUDE_PREFIX);
      this.northEast = Pair.of(Double.valueOf(nelat), Double.valueOf(nelng));

      return northEast;
    }
  }

  /**
   * @param resolver LazyEventResolver
   * @return Layer
   */
  private Layer resolveLayer(LazyEventResolver resolver) {
    switch (LayerType.typeOf(resolver.getLayerId())) {
      case Marker: {
        return createMarker(resolver.getLayerId(), resolver.getLatitude(),
                resolver.getLongitude());
      }
      case Circle: {
        return createCircle(resolver.getLayerId(), resolver.getLatitude(),
                resolver.getLongitude(), resolver.getRadius());
      }
      case Rectangle: {
        return createRectangle(resolver.getLayerId(),
                new LazyRectangleResolver(resolver.context, resolver.clientId));
      }
      case Polyline: {
        return createPolyline(resolver.getLayerId(), resolver.getLatLngs());
      }
      case Polygon: {
        return createPolygon(resolver.getLayerId(), resolver.getLatLngs());
      }
    }

    return null;
  }

  /**
   * @param behavior Behavior
   * @return FacesEvent PmCreateEvent
   */
  protected PmCreateEvent pmcreateEvent(Behavior behavior) {
    FacesContext facesContext = this.getFacesContext();
    Map<String, String> parameters = facesContext.getExternalContext().getRequestParameterMap();
    String clientId = this.getClientId(facesContext);
    String context = parameters.get(Strings.join(clientId, JSON_PREFIX));

    LazyEventResolver resolver = new LazyEventResolver(context, clientId);
    Layer layer = resolveLayer(resolver);

    PmCreateEvent event = onCreate(behavior, layer, Pair.of(resolver.getDate(), resolver.getTime()));
    if (event == null) {
      return new PmCreateEvent(this, behavior);       // isSucceed() gonna be false.
    }

    return event;
  }

  /**
   * @param behavior Behavior
   * @return FacesEvent PmCreateEvent
   */
  protected PmRemoveEvent pmremoveEdit(Behavior behavior) {
    FacesContext facesContext = this.getFacesContext();
    Map<String, String> parameters = facesContext.getExternalContext().getRequestParameterMap();
    String clientId = this.getClientId(facesContext);
    String context = parameters.get(clientId + JSON_PREFIX);

    LazyEventResolver resolver = new LazyEventResolver(context, clientId);
    if (Objects.isNull(getMapmodel())) {
      return new PmRemoveEvent(this, behavior);       // isSucceed() gonna be false.
    }

    Layer target = getMapmodel().get(resolver.getLayerId());
    if (target == null) {
      return new PmRemoveEvent(this, behavior);       // isSucceed() gonna be false.
    }

    final PmRemoveEvent event = new PmRemoveEvent.Builder<>()
            .setBehavior(behavior)
            .setComponent(this)
            .setTarget(target)
            .setDate(Strings.getIfNull(resolver.getDate(), ""))
            .setTime(Strings.getIfNull(resolver.getTime(), ""))
            .build();

    return event;
  }

  /**
   * @param behavior Behavior
   * @return FacesEvent PmCreateEvent
   */
  protected FacesEvent pmeditEvent(Behavior behavior) {
    FacesContext facesContext = this.getFacesContext();
    Map<String, String> parameters = facesContext.getExternalContext().getRequestParameterMap();
    String clientId = this.getClientId(facesContext);
    String context = parameters.get(clientId + JSON_PREFIX);

    LazyEventResolver resolver = new LazyEventResolver(context, clientId);
    if (Objects.isNull(getMapmodel())) {
      // isSucceed() gonna be false.
      return new PmEditEvent(this, behavior);
    }

    final Layer source = getMapmodel().get(resolver.getLayerId());
    if (source == null) {
      return new PmEditEvent(this, behavior);     // isSucceed() gonna be false.
    }

    Layer layer = resolveLayer(resolver);
    PmEditEvent event = onEdit(behavior, source, layer, Pair.of(resolver.getDate(), resolver.getTime()));
    if (event == null) {
      return new PmEditEvent(this, behavior);     // isSucceed() gonna be false.
    }

    return event;
  }

  /**
   * @param behavior Behavior
   * @param source Layer
   * @param layer Layer
   * @param attr Pair<String, String>
   * @return PmEditEvent
   */
  protected PmEditEvent onEdit(Behavior behavior, Layer source, Layer layer, Pair<String, String> attr) {
    if (Objects.isNull(layer) || Objects.isNull(source)) {
      return new PmEditEvent(this, behavior); // isSucceed() going to be false.
    }

    return new PmEditEvent.Builder<>()
            .setBehavior(behavior)
            .setComponent(this)
            .setDate(Strings.getIfNull(attr.k, ""))
            .setTime(Strings.getIfNull(attr.l, ""))
            .setSource(source)
            .setTarget(layer)
            .build();
  }

  /**
   * @param layerId String
   * @param lat Double
   * @param lng Double
   * @return Marker
   */
  private Marker createMarker(String layerId, Double lat, Double lng) {
    final LatLng latlng = L.latLng(lat, lng);
    final Marker marker = L.marker(latlng);
    marker.setLayerId(layerId);

    return marker;
  }

  /**
   * @param layerId String
   * @param lat Double
   * @param lng Double
   * @param radius String
   * @return Marker
   */
  private Circle createCircle(String layerId, Double lat, Double lng, Double radius) {
    final LatLng latlng = L.latLng(lat, lng);
    final Circle circle = L.circle(latlng, radius);
    circle.setLayerId(layerId);

    return circle;
  }

  /**
   * @param layerId String
   * @param resolver LazyRectangleResolver
   * @return Rectangle
   */
  private Rectangle createRectangle(String layerId, LazyRectangleResolver resolver) {
    Double swlat = resolver.getSouthWestPair().k;
    Double swlng = resolver.getSouthWestPair().l;
    Double nelat = resolver.getNorthEastPair().k;
    Double nelng = resolver.getNorthEastPair().l;

    final LatLng southWest = L.latLng(swlat, swlng);
    final LatLng northEast = L.latLng(nelat, nelng);

    final Rectangle rectangle = L.rectangle(southWest, northEast);
    rectangle.setLayerId(layerId);

    return rectangle;
  }

  /**
   * @param clientId String
   * @param latlngs JSONArray
   * @return JsonArray
   */
  private Polyline createPolyline(String layerId, JsonArray json) {
    final List<List<LatLng>> list = new ArrayList<>();

    try {
      for (int i = 0; i < json.length(); i++) {
        JsonArray latlngs = json.getJSONArray(i);
		List<LatLng> stack = new ArrayList<>();
		for (int j = 0; j < latlngs.length(); j++) {
		  String value = latlngs.optString(j);
		  String[] entry = value.split(",");
		  
		  stack.add(new LatLng(Double.valueOf(entry[0]), Double.valueOf(entry[1])));
		}
		
		list.add(stack);
      }
    } catch (JsonException | NumberFormatException e) {
      throw new LeafletException("Malformed json context or NumberFormatException. "
              + "{Hint: ErrorCode:189}", e);
    }

    final Polyline polyline = L.polyline(list);
    polyline.setLayerId(layerId);

    return polyline;
  }

  /**
   * @param clientId String
   * @param latlngs JSONArray
   * @return Polygon
   */
  private Polygon createPolygon(String layerId, JsonArray json) {
    final List<List<LatLng>> list = new ArrayList<>();

    try {
      for (int i = 0; i < json.length(); i++) {
        JsonArray latlngs = json.getJSONArray(i);
		List<LatLng> stack = new ArrayList<>();
		for (int j = 0; j < latlngs.length(); j++) {
		  String value = latlngs.optString(j);
		  String[] entry = value.split(",");
		  
		  stack.add(new LatLng(Double.valueOf(entry[0]), Double.valueOf(entry[1])));
		}
		
		list.add(stack);
      }
    } catch (JsonException | NumberFormatException e) {
      throw new LeafletException("Malformed json context or NumberFormatException. "
              + "{Hint: ErrorCode:189}", e);
    }

    final Polygon polygon = L.polygon(list);
    polygon.setLayerId(layerId);

    return polygon;
  }
  
  /**
   * @param behavior Behavior
   * @param layer Layer
   * @param attr Pair<String, String>
   * @return PmCreateEvent
   */
  private PmCreateEvent onCreate(Behavior behavior, Layer layer, Pair<String, String> attr) {
    if (Objects.isNull(layer)) {
      return new PmCreateEvent(this, behavior);   // isSucceed() going to be false.
    }

    return new PmCreateEvent.Builder<>()
            .setBehavior(behavior)
            .setComponent(this)
            .setDate(Strings.getIfNull(attr.k, ""))
            .setTime(Strings.getIfNull(attr.l, ""))
            .setTarget(layer)
            .build();
  }
}
