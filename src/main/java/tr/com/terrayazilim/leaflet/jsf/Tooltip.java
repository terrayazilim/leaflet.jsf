/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import java.io.Serializable;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public class Tooltip extends DivOverlay implements Serializable {

  private String content;
  private String direction;
  private Boolean permanent;
  private Boolean sticky;
  private Boolean interactive;
  private Double opacity;

  public Tooltip() {
  }

  /**
   * @param content
   */
  public Tooltip(String content) {
    this.content = content;
  }

  /**
   * @return
   */
  public String getContent() {
    return content;
  }

  /**
   * @param content
   */
  public void setContent(String content) {
    this.content = content;
  }

  /**
   * @return
   */
  public String getDirection() {
    return direction;
  }

  /**
   * @param direction
   */
  public void setDirection(String direction) {
    this.direction = direction;
  }

  /**
   * @return
   */
  public Boolean getPermanent() {
    return permanent;
  }

  /**
   * @param permanent
   */
  public void setPermanent(Boolean permanent) {
    this.permanent = permanent;
  }

  /**
   * @return
   */
  public Boolean getSticky() {
    return sticky;
  }

  /**
   * @param sticky
   */
  public void setSticky(Boolean sticky) {
    this.sticky = sticky;
  }

  /**
   * @return
   */
  public Boolean getInteractive() {
    return interactive;
  }

  /**
   * @param interactive
   */
  public void setInteractive(Boolean interactive) {
    this.interactive = interactive;
  }

  /**
   * @return
   */
  public Double getOpacity() {
    return opacity;
  }

  /**
   * @param opacity
   */
  public void setOpacity(Double opacity) {
    this.opacity = opacity;
  }
}
