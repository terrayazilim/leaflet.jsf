/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.event;

import javax.faces.component.UIComponent;
import javax.faces.component.behavior.Behavior;
import tr.com.terrayazilim.leaflet.jsf.LatLng;
import tr.com.terrayazilim.core.tuple.Triplet;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 * @since 1.0.0
 */
public class MapContextMenuEvent extends Event {

  public static class Builder {

    public UIComponent component;
    public Behavior behavior;

    public LatLng target;
    public String date;
    public String time;

    public Builder() {
    }

    /**
     * @param target
     * @return
     */
    public Builder setTarget(LatLng target) {
      this.target = target;
      return this;
    }

    /**
     * @param date
     * @return
     */
    public Builder setDate(String date) {
      this.date = date;
      return this;
    }

    /**
     * @param time
     * @return
     */
    public Builder setTime(String time) {
      this.time = time;
      return this;
    }

    /**
     * @param component
     * @return
     */
    public Builder setComponent(UIComponent component) {
      this.component = component;
      return this;
    }

    /**
     * @param behavior
     * @return
     */
    public Builder setBehavior(Behavior behavior) {
      this.behavior = behavior;
      return this;
    }

    /**
     * @return
     */
    public MapContextMenuEvent build() {
      return new MapContextMenuEvent(this);
    }
  }

  private final LatLng target;

  /**
   * @param component
   * @param behavior
   */
  public MapContextMenuEvent(UIComponent component, Behavior behavior) {
    super(component, behavior);
    this.target = null;
  }

  /**
   * @param builder
   */
  public MapContextMenuEvent(Builder builder) {
    super(builder.component, builder.behavior);
    this.target = builder.target;
    this.date = builder.date;
    this.time = builder.time;
  }

  /**
   * @param component
   * @param behavior
   * @param latlng
   */
  public MapContextMenuEvent(UIComponent component, Behavior behavior, LatLng latlng) {
    super(component, behavior);
    this.target = latlng;
  }

  /**
   * @param component
   * @param behavior
   * @param triplet
   */
  public MapContextMenuEvent(UIComponent component, Behavior behavior, Triplet<LatLng, String, String> triplet) {
    this(component, behavior, triplet.k);
    this.date = triplet.l;
    this.time = triplet.m;
  }

  @Override
  public Boolean isSucceed() {
    Boolean targetOk = this.target != null;

    return targetOk;
  }

  /**
   * @return
   */
  public LatLng getTarget() {
    return target;
  }
}
