/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.layer;

import tr.com.terrayazilim.leaflet.jsf.Icon;
import tr.com.terrayazilim.leaflet.jsf.Style;

/**
 * @see <a href="https://leafletjs.com/reference-1.3.0.html#marker">L.Marker</a>
 * 
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 * @since 1.0.0
 */
public class MarkerOptions extends AbstractLayerOptions implements Cloneable {

  private Icon icon = new Icon();
  private Boolean draggable;
  private Boolean keyboard;
  private String title;
  private String alt;
  private Integer zIndexOffset;
  private Double opacity;
  private Boolean riseOnHover;
  private Integer riseOffset;

  public MarkerOptions() {
  }

  /**
   * @param builder
   */
  public MarkerOptions(MarkerOptionsBuilder builder) {
    this.icon = builder.icon;
    this.draggable = builder.draggable;
    this.keyboard = builder.keyboard;
    this.title = builder.title;
    this.alt = builder.alt;
    this.zIndexOffset = builder.zIndexOffset;
    this.opacity = builder.opacity;
    this.riseOnHover = builder.riseOnHover;
    this.riseOffset = builder.riseOffset;
  }

  @Override
  public Style toStyle() {
	Style style = new Style();
	
	style.setInteractive(getInteractive());
	style.setTooltip(getTooltip());
	style.setPopup(getPopup());
	style.setPane(getPane());
	style.setRenderer(getRenderer());
	
	return style;
  }

  /**
   * @return
   */
  public Icon getIcon() {
    return icon;
  }

  /**
   * @param icon
   */
  public void setIcon(Icon icon) {
    this.icon = icon;
  }

  /**
   * @return
   */
  public Boolean getDraggable() {
    return draggable;
  }

  /**
   * @param draggable
   */
  public void setDraggable(Boolean draggable) {
    this.draggable = draggable;
  }

  /**
   * @return
   */
  public Boolean getKeyboard() {
    return keyboard;
  }

  /**
   * @param keyboard
   */
  public void setKeyboard(Boolean keyboard) {
    this.keyboard = keyboard;
  }

  /**
   * @return
   */
  public String getTitle() {
    return title;
  }

  /**
   * @param title
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * @return
   */
  public String getAlt() {
    return alt;
  }

  /**
   * @param alt
   */
  public void setAlt(String alt) {
    this.alt = alt;
  }

  /**
   * @return
   */
  public Integer getzIndexOffset() {
    return zIndexOffset;
  }

  /**
   * @param zIndexOffset
   */
  public void setzIndexOffset(Integer zIndexOffset) {
    this.zIndexOffset = zIndexOffset;
  }

  /**
   * @return
   */
  public Double getOpacity() {
    return opacity;
  }

  /**
   * @param opacity
   */
  public void setOpacity(Double opacity) {
    this.opacity = opacity;
  }

  /**
   * @return
   */
  public Boolean getRiseOnHover() {
    return riseOnHover;
  }

  /**
   * @param riseOnHover
   */
  public void setRiseOnHover(Boolean riseOnHover) {
    this.riseOnHover = riseOnHover;
  }

  /**
   * @return
   */
  public Integer getRiseOffset() {
    return riseOffset;
  }

  /**
   * @param riseOffset
   */
  public void setRiseOffset(Integer riseOffset) {
    this.riseOffset = riseOffset;
  }
}
