/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import java.io.Serializable;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public class Pane implements Serializable {

  private String id;
  private HTMLElement htmlElement;

  public Pane() {
  }

  /**
   * @param id
   * @param htmlElement
   */
  public Pane(String id, HTMLElement htmlElement) {
    this.id = id;
    this.htmlElement = htmlElement;
  }

  /**
   * @param id
   */
  public Pane(String id) {
    this.id = id;
  }

  /**
   * @return
   */
  public String getId() {
    return id;
  }

  /**
   * @param id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * @return
   */
  public HTMLElement getHtmlElement() {
    return htmlElement;
  }

  /**
   * @param htmlElement
   */
  public void setHtmlElement(HTMLElement htmlElement) {
    this.htmlElement = htmlElement;
  }
}
