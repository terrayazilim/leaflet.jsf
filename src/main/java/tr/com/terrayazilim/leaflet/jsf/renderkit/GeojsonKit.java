/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.renderkit;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import tr.com.terrayazilim.core.Objects;
import tr.com.terrayazilim.core.Strings;
import tr.com.terrayazilim.core.collection.Maps;
import tr.com.terrayazilim.core.math.BigLatLon;
import tr.com.terrayazilim.core.math.BigLatitude;
import tr.com.terrayazilim.core.math.BigLongitude;
import tr.com.terrayazilim.gjson.Gjson;
import tr.com.terrayazilim.gjson.GjsonFeature;
import tr.com.terrayazilim.gjson.GjsonFeatureCollection;
import tr.com.terrayazilim.gjson.GjsonGeometry;
import tr.com.terrayazilim.gjson.GjsonLineString;
import tr.com.terrayazilim.gjson.GjsonMultiLineString;
import tr.com.terrayazilim.gjson.GjsonObject;
import tr.com.terrayazilim.gjson.GjsonPoint;
import tr.com.terrayazilim.gjson.GjsonPolygon;
import tr.com.terrayazilim.gjson.util.BigLatLonList;
import tr.com.terrayazilim.json.JsonObject;
import tr.com.terrayazilim.json.type.Token;
import tr.com.terrayazilim.leaflet.jsf.L;
import tr.com.terrayazilim.leaflet.jsf.LatLng;
import tr.com.terrayazilim.leaflet.jsf.LayerMap;
import tr.com.terrayazilim.leaflet.jsf.layer.Circle;
import tr.com.terrayazilim.leaflet.jsf.layer.CircleImpl;
import tr.com.terrayazilim.leaflet.jsf.layer.CircleOptions;
import tr.com.terrayazilim.leaflet.jsf.layer.Layer;
import tr.com.terrayazilim.leaflet.jsf.layer.LayerType;
import tr.com.terrayazilim.leaflet.jsf.layer.LeafletLayerException;
import tr.com.terrayazilim.leaflet.jsf.layer.Marker;
import tr.com.terrayazilim.leaflet.jsf.layer.MarkerImpl;
import tr.com.terrayazilim.leaflet.jsf.layer.MarkerOptions;
import tr.com.terrayazilim.leaflet.jsf.layer.Polygon;
import tr.com.terrayazilim.leaflet.jsf.layer.PolygonImpl;
import tr.com.terrayazilim.leaflet.jsf.layer.PolygonOptions;
import tr.com.terrayazilim.leaflet.jsf.layer.Polyline;
import tr.com.terrayazilim.leaflet.jsf.layer.PolylineImpl;
import tr.com.terrayazilim.leaflet.jsf.layer.PolylineOptions;
import tr.com.terrayazilim.leaflet.jsf.layer.Rectangle;
import tr.com.terrayazilim.leaflet.jsf.layer.RectangleImpl;
import tr.com.terrayazilim.leaflet.jsf.layer.RectangleOptions;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 * @since 1.0.0
 */
public final class GeojsonKit {

  private final JsonKit jsonKit;
  private final Gjson gjson;

  public GeojsonKit() {
	this.jsonKit = new JsonKit();
	this.gjson = new Gjson();
  }

  /**
   * @param geojson
   * @return 
   */
  public Map<String, Layer> getMap(String geojson) {
	GjsonObject object = gjson.fromGjson(geojson).quickFix();
	
	switch (object.getType()) {
	  case Feature:
		GjsonFeatureCollection gfc = new GjsonFeatureCollection();
		gfc.setFeatures(Arrays.asList(((GjsonFeature) object)));
		return getMap(gfc);
	  case FeatureCollection:
		return getMap(((GjsonFeatureCollection) object));
	  default:
		throw new LeafletLayerException("Geojson is not Feature or FeatureCollection. {Hint: ErrorCode:272}");
	}
  }
  
  /**
   * @param gfc
   * @return 
   */
  public Map<String, Layer> getMap(GjsonFeatureCollection gfc) {
	Map<String, Layer> map = new LinkedHashMap<>();
	
	Collection<GjsonFeature> stack = gfc.getFeatures();
	if (Objects.isNull(stack) || stack.isEmpty()) {
	  return map;
	}
	
	for (GjsonFeature feature : stack) {
	  Layer layer = getLayer(feature);
	  map.put(layer.getLayerId(), layer);
	}
	
	return map;
  }
  
  /**
   * @param map
   * @return 
   */
  public GjsonFeatureCollection getFeatureCollection(Map<String, Layer> map) {
	GjsonFeatureCollection gfc = new GjsonFeatureCollection();
	if (Maps.nullOrEmpty(map)) {
	  return gfc;
	}
	
	Collection<GjsonFeature> stack = new ArrayList<>();
	for (Map.Entry<String, Layer> entry : map.entrySet()) {
	  Layer value = entry.getValue();
	  stack.add(value.toGeoJSON());
	}
	
	gfc.setFeatures(stack);
	return gfc;
  }
  
  /**
   * @param layerMap
   * @return 
   */
  public GjsonFeatureCollection getFeatureCollection(LayerMap layerMap) {
	GjsonFeatureCollection gfc = new GjsonFeatureCollection();
	
	Collection<GjsonFeature> stack = new ArrayList<>();
	for (Layer value : layerMap.values()) {
	  stack.add(value.toGeoJSON());
	}
	
	gfc.setFeatures(stack);
	return gfc;
  }
  
  /**
   * @param gjsonFeature
   * @param layerType
   * @return
   */
  private JsonObject getPropertiesIfLayerTypesMatch(GjsonFeature gjsonFeature, LayerType layerType) {
	if (Objects.isNull(gjsonFeature) || Objects.isNull(layerType)) {
	  return null;
	}

	GjsonFeature feature = gjsonFeature.quickFix();
	JsonObject properties = feature.getProperties();
	String type = properties.optString("layerType");
	if (Strings.nullOrBlank(type) || !type.equals(layerType.toString())) {
	  return null;
	}

	return properties;
  }

  /**
   * @param feature
   * @return
   */
  public Layer getLayer(GjsonFeature feature) {
	GjsonFeature safe = feature.quickFix();

	if (isMarker(safe)) {
	  return getMarker(safe);
	} else if (isCircle(safe)) {
	  return getCircle(safe);
	} else if (isRectangle(safe)) {
	  return getRectangle(safe);
	} else if (isPolyline(safe)) {
	  return getPolyline(safe);
	} else if (isPolygon(safe)) {
	  return getPolygon(safe);
	}

	throw new LeafletLayerException("Layer cannot created. Geojson geometry is not instanceof Marker,"
			+ "Circle, Rectangle, Polyline, Polygon. {Hint: ErrorCode:215}");
  }

  /**
   * @param feature
   * @return
   */
  public Marker getMarker(GjsonFeature feature) {
	Marker marker = new MarkerImpl();
	JsonObject properties = getPropertiesIfLayerTypesMatch(feature, LayerType.Marker);
	if (Objects.isNull(properties)) {
	  throw new LeafletLayerException("Geojson is not instance of Marker. {Hint: ErrorCode:214}");
	}
	
	GjsonGeometry geometry = feature.getGeometry();
	String layerId = feature.getId();
	MarkerOptions markerOptions = jsonKit.getJson().fromJson(properties, new Token<>(MarkerOptions.class));
	marker.setOptions(markerOptions);

	if (!(geometry instanceof GjsonPoint)) {
	  throw new LeafletLayerException("Layer cannot created. Geojson geometry is not GjsonPoint."
			  + "{Hint: ErrorCode:215}");
	}

	BigLatitude latitude = ((GjsonPoint) geometry).getLat();
	BigLongitude longitude = ((GjsonPoint) geometry).getLon();
	marker.setLatLng(new LatLng(latitude, longitude));

	marker.setLayerId(layerId);
	return marker;
  }

  /**
   * @param feature
   * @return
   */
  public Circle getCircle(GjsonFeature feature) {
	Circle circle = new CircleImpl();
	JsonObject properties = getPropertiesIfLayerTypesMatch(feature, LayerType.Circle);
	if (Objects.isNull(properties)) {
	  throw new LeafletLayerException("Geojson is not instance of Circle. {Hint: ErrorCode:232}");
	}
	
	CircleOptions circleOptions = jsonKit.getJson().fromJson(properties, new Token<>(CircleOptions.class));
	circle.setOptions(circleOptions);

	GjsonGeometry geometry = feature.getGeometry();
	if (!(geometry instanceof GjsonPoint)) {
	  throw new LeafletLayerException("Layer cannot created. Geojson geometry is not GjsonPoint."
			  + "{Hint: ErrorCode:216}");
	}

	BigLatitude latitude = ((GjsonPoint) geometry).getLat();
	BigLongitude longitude = ((GjsonPoint) geometry).getLon();
	circle.setLatLng(new LatLng(latitude, longitude));

	circle.setLayerId(feature.getId());
	return circle;
  }

  /**
   * @param feature
   * @return
   */
  public Rectangle getRectangle(GjsonFeature feature) {
	Rectangle rectangle = new RectangleImpl();
	JsonObject properties = getPropertiesIfLayerTypesMatch(feature, LayerType.Rectangle);
	if (Objects.isNull(properties)) {
	  throw new LeafletLayerException("Geojson is not instance of Rectangle. {Hint: ErrorCode:233}");
	}
	
	RectangleOptions rectangleOptions = jsonKit.getJson().fromJson(properties, new Token<>(RectangleOptions.class));
	rectangle.setOptions(rectangleOptions);

	GjsonGeometry geometry = feature.getGeometry();
	if (!(geometry instanceof GjsonLineString)) {
	  throw new LeafletLayerException("Layer cannot created. Geojson geometry is not GjsonLineString."
			  + "{Hint: ErrorCode:217}");
	}

	BigLatLonList stack = ((GjsonLineString) geometry).getCoordinates();
	BigLatLon sw = null;
	BigLatLon ne = null;

	for (BigLatLon latlon : stack) {
	  if (sw == null) {
		sw = latlon;
	  } else if (ne == null) {
		ne = latlon;
	  }
	}

	rectangle.setBounds(new LatLng(sw), new LatLng(ne));
	rectangle.validate();
	rectangle.setLayerId(feature.getId());
	return rectangle;
  }

  /**
   * @param feature
   * @return
   */
  public Polyline getPolyline(GjsonFeature feature) {
	Polyline polyline = new PolylineImpl();
	JsonObject properties = getPropertiesIfLayerTypesMatch(feature, LayerType.Polyline);
	if (Objects.isNull(properties)) {
	  throw new LeafletLayerException("Geojson is not instance of Polyline. {Hint: ErrorCode:234}");
	}
	
	PolylineOptions polylineOptions = jsonKit.getJson().fromJson(properties, new Token<>(PolylineOptions.class));
	polyline.setOptions(polylineOptions);

	GjsonGeometry geometry = feature.getGeometry();
	if (geometry instanceof GjsonMultiLineString) {
	  Collection<BigLatLonList> stack = ((GjsonMultiLineString) geometry).getCoordinates();
	  List<List<LatLng>> exterior = new ArrayList<>();

	  for (BigLatLonList eachStep : stack) {
		List<LatLng> interior = new ArrayList<>();
		for (BigLatLon latlon : eachStep) {
		  interior.add(new LatLng(latlon));
		}

		exterior.add(interior);
	  }

	  polyline.setLatLngs(exterior);
	} else if (geometry instanceof GjsonLineString) {
	  BigLatLonList stack = ((GjsonLineString) geometry).getCoordinates();
	  List<List<LatLng>> exterior = new ArrayList<>();
	  List<LatLng> interior = new ArrayList<>();
	  for (BigLatLon latlon : stack) {
		interior.add(new LatLng(latlon));
	  }

	  exterior.add(interior);
	  polyline.setLatLngs(exterior);
	} else {
	  throw new LeafletLayerException("Layer cannot created. Geojson geometry is not GjsonLineString."
			  + "{Hint: ErrorCode:218}");
	}

	polyline.setLayerId(feature.getId());
	return polyline;
  }

  /**
   * @param feature
   * @return
   */
  public Polygon getPolygon(GjsonFeature feature) {
	Polygon polygon = new PolygonImpl();
	JsonObject properties = getPropertiesIfLayerTypesMatch(feature, LayerType.Polygon);
	if (Objects.isNull(properties)) {
	  throw new LeafletLayerException("Geojson is not instance of Polygon. {Hint: ErrorCode:235}");
	}
	
	PolygonOptions polygonOptions = jsonKit.getJson().fromJson(properties, new Token<>(PolygonOptions.class));
	polygon.setOptions(polygonOptions);

	GjsonGeometry geometry = feature.getGeometry();
	if (!(geometry instanceof GjsonPolygon)) {
	  throw new LeafletLayerException("Layer cannot created. Geojson geometry is not GjsonLineString."
			  + "{Hint: ErrorCode:218}");
	}

	Collection<BigLatLonList> stack = ((GjsonPolygon) geometry).getCoordinates();
	List<List<LatLng>> exterior = new ArrayList<>();

	for (BigLatLonList eachStep : stack) {
	  List<LatLng> interior = new ArrayList<>();
	  for (BigLatLon latlon : eachStep) {
		interior.add(new LatLng(latlon));
	  }

	  exterior.add(interior);
	}

	polygon.setLatLngs(exterior);
	polygon.setLayerId(feature.getId());
	return polygon;
  }
  
  /**
   * @param gjsonObject
   * @return 
   */
  public String toGjson(GjsonObject gjsonObject ) {
	return gjson.toGjson(gjsonObject);
  }
  
  /**
   * @param gjsonObject
   * @param type
   * @return 
   */
  public String toGjson(GjsonObject gjsonObject, Type type) {
	return gjson.toGjson(gjsonObject, type);
  }
  
  /**
   * @param geojson
   * @return 
   */
  public GjsonObject fromGjson(String geojson) {
	return gjson.fromGjson(geojson);
  }
  
  /**
   * @param <T>
   * @param geojson
   * @param gjsonObjectClass
   * @return 
   */
  public <T extends GjsonObject> T fromGjson(String geojson, Class<T> gjsonObjectClass) {
	return gjson.fromGjson(geojson, gjsonObjectClass);
  }

  /**
   * @param feature
   * @return
   */
  public LayerType getLayerType(GjsonFeature feature) {
	if (Objects.isNull(feature)) {
	  return null;
	}

	JsonObject properties = feature.getProperties();
	if (Objects.isNull(properties)) {
	  return null;
	}

	String type = properties.optString("layerType");
	if (Strings.nullOrBlank(type)) {
	  return null;
	}

	for (LayerType value : LayerType.values()) {
	  if (type.equalsIgnoreCase(value.toString())) {
		return value;
	  }
	}

	return null;
  }

  /**
   * @param feature
   * @param layerType
   * @return
   */
  public boolean is(GjsonFeature feature, LayerType layerType) {
	if (Objects.isNull(feature) || Objects.isNull(layerType)) {
	  return false;
	}

	JsonObject properties = feature.getProperties();
	if (Objects.isNull(properties)) {
	  return false;
	}

	String type = properties.optString("layerType");
	if (Strings.nullOrBlank(type)) {
	  return false;
	}

	return type.equals(layerType.toString());
  }

  /**
   * @param feature
   * @return
   */
  public boolean isMarker(GjsonFeature feature) {
	return is(feature, LayerType.Marker);
  }

  /**
   * @param feature
   * @return
   */
  public boolean isCircle(GjsonFeature feature) {
	return is(feature, LayerType.Circle);
  }

  /**
   * @param feature
   * @return
   */
  public boolean isRectangle(GjsonFeature feature) {
	return is(feature, LayerType.Rectangle);
  }

  /**
   * @param feature
   * @return
   */
  public boolean isPolyline(GjsonFeature feature) {
	return is(feature, LayerType.Polyline);
  }

  /**
   * @param feature
   * @return
   */
  public boolean isPolygon(GjsonFeature feature) {
	return is(feature, LayerType.Polygon);
  }

  /**
   * @param gjsonPoint
   * @return
   */
  public Marker getMarker(GjsonPoint gjsonPoint) {
	BigLatitude lat = gjsonPoint.getLat();
	BigLongitude lng = gjsonPoint.getLon();

	return L.marker(new LatLng(lat, lng));
  }

  /**
   * @return 
   */
  public Gjson getGjson() {
	return gjson;
  }

  /**
   * @return 
   */
  public JsonKit getJsonKit() {
	return jsonKit;
  }
}
