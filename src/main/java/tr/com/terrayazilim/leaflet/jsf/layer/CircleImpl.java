/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.layer;

import tr.com.terrayazilim.core.Objects;
import tr.com.terrayazilim.gjson.GjsonFeature;
import tr.com.terrayazilim.gjson.GjsonPoint;
import tr.com.terrayazilim.json.JsonObject;
import tr.com.terrayazilim.json.annotation.Ignored;
import tr.com.terrayazilim.leaflet.jsf.LatLng;
import tr.com.terrayazilim.leaflet.jsf.renderkit.JsonKit;

/**
 * @see <a href="https://leafletjs.com/reference-1.3.0.html#circle">L.Circle</a>
 * 
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 * @since 1.0.0
 */
public class CircleImpl extends AbstractLayer implements Circle {

  @Ignored
  private static final long serialVersionUID = 986875384581247123L;

  private LatLng latLng;
  private CircleOptions options = new CircleOptions();

  public CircleImpl() {
    super(LayerType.Circle);
  }

  /**
   * @param latLng
   * @param radius 
   */
  public CircleImpl(LatLng latLng, Double radius) {
    this();
    this.latLng = latLng;
    this.options.setRadius(radius);
  }

  /**
   * @param latlng
   * @param options 
   */
  public CircleImpl(LatLng latlng, CircleOptions options) {
    this();
    this.latLng = latlng;
    this.options = options;
  }

  /**
   * @param circle 
   */
  public CircleImpl(Circle circle) {
    this(circle.getLatLng(), circle.getOptions());

    this.options = circle.getOptions();
  }

  @Override
  public GjsonFeature toGeoJSON() {
	JsonKit jsonKit = new JsonKit();
	
	GjsonPoint point = new GjsonPoint();
	point.setLat(getLatLng().getBigLatitude());
	point.setLon(getLatLng().getBigLongitude());
	
	GjsonFeature feature = new GjsonFeature();
	feature.setId(getLayerId());
	feature.setGeometry(point);
	
	JsonObject properties = jsonKit.getJson().toJson(this.options);
	properties.put("layerType", getLayerType().toString());
	feature.setProperties(properties);
	
	return feature;
  }

  @Override
  public LatLng getLatLng() {
    return latLng;
  }

  @Override
  public void setLatLng(LatLng latLng) {
    this.latLng = latLng;
  }

  @Override
  public double getRadius() {
    return this.options.getRadius();
  }

  @Override
  public void setRadius(double radius) {
    this.options.setRadius(radius);
  }

  @Override
  public CircleOptions getOptions() {
    if (Objects.isNull(this.options)) {
      this.options = new CircleOptions();
    }

    return this.options;
  }

  @Override
  public void setOptions(LayerOptions options) {
    if (Objects.nonNull(options) && options instanceof CircleOptions) {
      this.options = (CircleOptions) options;
    }
  }

  @Override
  public Circle clone() {
    Circle circle = new CircleImpl(this);
    circle.setLayerId(getLayerId());
    circle.setOptionalData(getOptionalData());
    
    return circle;
  }
}
