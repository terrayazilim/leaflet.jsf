/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import java.io.Serializable;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public class XYZ extends TileLayer implements Serializable {

  private String subdomains;
  private String errorTileUrl;
  private Integer zoomOffset;
  private Boolean tms;
  private Boolean zoomReverse;
  private Boolean detectRetina;
  private Boolean crossOrigin;

  public XYZ() {
    setAttribution(new Attribution());
  }

  /**
   * @param url
   */
  public XYZ(String url) {
    super(url);
    setAttribution(new Attribution());
  }

  /**
   * @return
   */
  public String getSubdomains() {
    return subdomains;
  }

  /**
   * @param subdomains
   */
  public void setSubdomains(String subdomains) {
    this.subdomains = subdomains;
  }

  /**
   * @return
   */
  public String getErrorTileUrl() {
    return errorTileUrl;
  }

  /**
   * @param errorTileUrl
   */
  public void setErrorTileUrl(String errorTileUrl) {
    this.errorTileUrl = errorTileUrl;
  }

  /**
   * @return
   */
  public Integer getZoomOffset() {
    return zoomOffset;
  }

  /**
   * @param zoomOffset
   */
  public void setZoomOffset(Integer zoomOffset) {
    this.zoomOffset = zoomOffset;
  }

  /**
   * @return
   */
  public Boolean getTms() {
    return tms;
  }

  /**
   * @param tms
   */
  public void setTms(Boolean tms) {
    this.tms = tms;
  }

  /**
   * @return
   */
  public Boolean getZoomReverse() {
    return zoomReverse;
  }

  /**
   * @param zoomReverse
   */
  public void setZoomReverse(Boolean zoomReverse) {
    this.zoomReverse = zoomReverse;
  }

  /**
   * @return
   */
  public Boolean getDetectRetina() {
    return detectRetina;
  }

  /**
   * @param detectRetina
   */
  public void setDetectRetina(Boolean detectRetina) {
    this.detectRetina = detectRetina;
  }

  /**
   * @return
   */
  public Boolean getCrossOrigin() {
    return crossOrigin;
  }

  /**
   * @param crossOrigin
   */
  public void setCrossOrigin(Boolean crossOrigin) {
    this.crossOrigin = crossOrigin;
  }
}
