/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.layer;

import java.util.UUID;
import tr.com.terrayazilim.core.Objects;
import tr.com.terrayazilim.core.Strings;
import tr.com.terrayazilim.core.annotation.Issue;
import tr.com.terrayazilim.leaflet.jsf.MapModel;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 * @since 1.0.0
 */
public abstract class AbstractLayer implements Layer {

  public abstract Layer clone();

  protected String layerId;
  protected LayerType layerType;
  protected Object optionalData;

  /**
   * Main Constructor. Every other constructor calls this first. It's responsible for generating
   * unique layer id.
   *
   * @param layerType
   */
  protected AbstractLayer(LayerType layerType) {
    this.layerType = layerType;
    this.layerId = generateLayerId(layerType);
  }

  @Override
  public void addTo(MapModel model) {
	if (Objects.nonNull(model)) {
	  model.put(this);
	}
  }

  @Override
  public boolean removeFrom(MapModel model) {
	if (Objects.isNull(model)) {
	  return false;
	}
	
	return model.remove(getLayerId(), this);
  }

  @Override
  public Object getOptionalData() {
    return optionalData;
  }

  @Override
  public void setOptionalData(Object optionalData) {
    // Consider this strategy for String data
    /*
     * if (optionalData != null) {
     * boolean isString = optionalData instanceof String;
     *
     * if (isString) {
     * String optionalString = (String) optionalData;
     * boolean isStartsWithQuote = optionalString.startsWith("'") || optionalString.startsWith("\"");
     * boolean iEndsWithQuote = optionalString.endsWith("'") || optionalString.endsWith("\"");
     *
     * if (isStartsWithQuote) {
     * if (iEndsWithQuote) {
     * this.optionalData = optionalData;
     * } else {
     * optionalString = optionalString + "'";
     * this.optionalData = optionalString; }
     *
     * }
     * }
     * }
     */

    // if given object is not null and it's instance of String.
    if (optionalData != null && optionalData instanceof String) {

      // Since we veryfied given Object is String. We can safely cast it to String.
      String string = (String) optionalData;

      // is arg starts with ' ?
      boolean startCheck = string.startsWith("'");

      // is arg ends with '?
      boolean endCheck = string.endsWith("'");

      // if arg starts and ends with '
      if (startCheck && endCheck) {
        this.optionalData = optionalData;
      } else if (startCheck) {
        this.optionalData = optionalData + "'";
      } else if (endCheck) {
        this.optionalData = "'" + optionalData;
      } else {
        this.optionalData = Strings.inQuote((String) optionalData);
      }
    }

    this.optionalData = optionalData;
  }

  /**
   * @param layerType
   * @return Generates UUID.
   */
  private static String generateLayerId(LayerType layerType) {
    return new StringBuilder()
        .append(layerType.toStringWithPrefix())
        .append(UUID.randomUUID().toString())
        .toString();
  }

  @Issue(
      category = Issue.Type.ENHANCEMENT,
      content = "If arg layerId is null or blank, setLayerId avoid setting. Should we "
      + "also protect the layerId against strings that doesn't start with layer prefix ?",
      isFixed = false
  )
  @Override
  public void setLayerId(String layerId) {
    // if given argument id is null or blank it complicates things. so we shouldn't let null layer id's.
    if (!Strings.nullOrBlank(layerId)) {
      this.layerId = layerId;
    }
  }

  @Override
  public String getLayerId() {
    return this.layerId;
  }

  @Override
  public LayerType getLayerType() {
    return this.layerType;
  }
}
