/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.layer;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import tr.com.terrayazilim.core.annotation.Development;
import tr.com.terrayazilim.gjson.GjsonFeature;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
@Development(status = Development.Status.EXPERIMENTAL)
public class LayerGroupImpl extends AbstractLayer implements LayerGroup {

  private Map<String, Layer> layers;

  public LayerGroupImpl() {
    super(LayerType.LayerGroup);
	
    this.layers = new LinkedHashMap<>();
  }
  
  @Override
  public GjsonFeature toGeoJSON() {
	throw new UnsupportedOperationException("Not supported yet.");
  }

  /**
   * @param layers 
   */
  public LayerGroupImpl(java.util.Map<String, Layer> layers) {
    super(LayerType.LayerGroup);

    this.layers = layers;
  }

  @Override
  public Collection<Layer> getLayers() {
    return layers.values();
  }

  @Override
  public Layer addLayer(Layer layer) {
    return this.layers.put(layer.getLayerId(), layer);
  }

  @Override
  public Layer removeLayer(Layer layer) {
    return this.layers.remove(layer.getLayerId());
  }

  @Override
  public Layer removeLayer(String id) {
    return this.layers.remove(id);
  }

  @Override
  public boolean hasLayer(Layer layer) {
    return this.layers.containsValue(layer);
  }

  @Override
  public boolean hasLayer(String id) {
    return this.layers.containsKey(id);
  }

  @Override
  public void clearLayers() {
    this.layers.clear();
  }

  @Override
  public Layer getLayer(String id) {
    return this.layers.get(id);
  }

  @Override
  public LayerOptions getOptions() {
	throw new LeafletLayerException("Options are not supported in LayerGroup. {Hint:ErrorCode:209}");
  }

  @Override
  public void setOptions(LayerOptions layerOptions) {
	throw new LeafletLayerException("Options are not supported in LayerGroup. {Hint:ErrorCode:210}");
  }
  
  @Override
  public LayerGroupImpl clone() {
    return this;
  }
}
