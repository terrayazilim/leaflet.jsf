/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import java.io.Serializable;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public class EasyPrint implements Serializable {

  // L.Control.Position 
  static public enum Position {

    topleft("topleft"),
    topright("topright"),
    bottomleft("bottomleft"),
    bottomright("bottomright");

    private final String pointer;

    /**
     * @param pointer
     */
    private Position(String pointer) {
      this.pointer = pointer;
    }

    @Override
    public String toString() {
      return this.pointer;
    }
  }

  // We will be victorius.
  private Position position = Position.topleft;
  private String title;
  private String defaultSizeTitles;
  private Boolean exportOnly = true;
  private TileLayer tileLayer;
  private Integer tileWait;
  private String filename = "osm";
  private Boolean hidden;
  private Boolean hideControlContainer = true;
  private String hideClasses;
  private String customWindowTitle;
  private String spinnerBgColor;
  private String customSpinnerClass;

  public EasyPrint() {
  }

  /**
   * @return
   */
  public Position getPosition() {
    return position;
  }

  /**
   * @param position
   */
  public void setPosition(Position position) {
    this.position = position;
  }

  /**
   * @return
   */
  public String getTitle() {
    return title;
  }

  /**
   * @param title
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * @return
   */
  public String getDefaultSizeTitles() {
    return defaultSizeTitles;
  }

  /**
   * @param defaultSizeTitles
   */
  public void setDefaultSizeTitles(String defaultSizeTitles) {
    this.defaultSizeTitles = defaultSizeTitles;
  }

  /**
   * @return
   */
  public Boolean getExportOnly() {
    return exportOnly;
  }

  /**
   * @param exportOnly
   */
  public void setExportOnly(Boolean exportOnly) {
    this.exportOnly = exportOnly;
  }

  /**
   * @return
   */
  public TileLayer getTileLayer() {
    return tileLayer;
  }

  /**
   * @param tileLayer
   */
  public void setTileLayer(TileLayer tileLayer) {
    this.tileLayer = tileLayer;
  }

  /**
   * @return
   */
  public Integer getTileWait() {
    return tileWait;
  }

  /**
   * @param tileWait
   */
  public void setTileWait(Integer tileWait) {
    this.tileWait = tileWait;
  }

  /**
   * @return
   */
  public String getFilename() {
    return filename;
  }

  /**
   * @param filename
   */
  public void setFilename(String filename) {
    this.filename = filename;
  }

  /**
   * @return
   */
  public Boolean getHidden() {
    return hidden;
  }

  /**
   * @param hidden
   */
  public void setHidden(Boolean hidden) {
    this.hidden = hidden;
  }

  /**
   * @return
   */
  public Boolean getHideControlContainer() {
    return hideControlContainer;
  }

  /**
   * @param hideControlContainer
   */
  public void setHideControlContainer(Boolean hideControlContainer) {
    this.hideControlContainer = hideControlContainer;
  }

  /**
   * @return
   */
  public String getHideClasses() {
    return hideClasses;
  }

  /**
   * @param hideClasses
   */
  public void setHideClasses(String hideClasses) {
    this.hideClasses = hideClasses;
  }

  /**
   * @return
   */
  public String getCustomWindowTitle() {
    return customWindowTitle;
  }

  /**
   * @param customWindowTitle
   */
  public void setCustomWindowTitle(String customWindowTitle) {
    this.customWindowTitle = customWindowTitle;
  }

  /**
   * @return
   */
  public String getSpinnerBgColor() {
    return spinnerBgColor;
  }

  /**
   * @param spinnerBgColor
   */
  public void setSpinnerBgColor(String spinnerBgColor) {
    this.spinnerBgColor = spinnerBgColor;
  }

  /**
   * @return
   */
  public String getCustomSpinnerClass() {
    return customSpinnerClass;
  }

  /**
   * @param customSpinnerClass
   */
  public void setCustomSpinnerClass(String customSpinnerClass) {
    this.customSpinnerClass = customSpinnerClass;
  }
}
