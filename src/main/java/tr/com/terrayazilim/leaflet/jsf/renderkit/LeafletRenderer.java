/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.renderkit;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.faces.component.UIComponent;
import javax.faces.component.behavior.ClientBehavior;
import javax.faces.component.behavior.ClientBehaviorContext;
import javax.faces.component.behavior.ClientBehaviorHolder;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.FacesRenderer;
import tr.com.terrayazilim.core.Bools;
import tr.com.terrayazilim.core.Objects;
import tr.com.terrayazilim.core.Strings;
import tr.com.terrayazilim.core.j2js.FunctionBuilder;
import tr.com.terrayazilim.core.j2js.Parameter;
import tr.com.terrayazilim.core.j2js.Variable;
import tr.com.terrayazilim.core.j2js.VariableBuilder;
import tr.com.terrayazilim.json.type.Token;
import tr.com.terrayazilim.leaflet.jsf.EasyPrint;
import tr.com.terrayazilim.leaflet.jsf.LayerMap;
import tr.com.terrayazilim.leaflet.jsf.MapModel;
import tr.com.terrayazilim.leaflet.jsf.MapOptions;
import tr.com.terrayazilim.leaflet.jsf.Pm;
import tr.com.terrayazilim.leaflet.jsf.PolylineMeasure;
import tr.com.terrayazilim.leaflet.jsf.TileLayer;
import tr.com.terrayazilim.leaflet.jsf.WMS;
import tr.com.terrayazilim.leaflet.jsf.XYZ;
import tr.com.terrayazilim.leaflet.jsf.component.Leaflet;
import tr.com.terrayazilim.leaflet.jsf.layer.Layer;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
@FacesRenderer(
		componentFamily = "tr.com.terrayazilim.leaflet.jsf.component",
		rendererType = "tr.com.terrayazilim.leaflet.jsf.renderkit.LeafletRenderer"
)
public class LeafletRenderer extends AbstractRenderer {

  /**
   * @param facesContext FacesContext
   * @param leaflet Leaflet
   * @throws IOException IOException
   */
  protected void encodeScript(FacesContext facesContext, Leaflet leaflet) throws IOException {
	ResponseWriter responseWriter = facesContext.getResponseWriter();
	String clientId = leaflet.getClientId(facesContext);

	responseWriter.startElement("div", leaflet);
	responseWriter.writeAttribute("id", clientId, null);
	final StringBuilder style;
	final boolean written;

	if (leaflet.getStyle() == null || leaflet.getStyle().isEmpty()) {
	  style = new StringBuilder();
	  written = false;
	} else {
	  style = new StringBuilder(leaflet.getStyle());
	  written = true;
	}

	// make responsive full-page map.
	if (Bools.isTrue(leaflet.getResponsive())) {
	  // if given style is not ending with ';'.
	  if (written && !style.toString().endsWith(";")) {
		style.append(";");
	  }

	  style.append("width: 100%;");
	  style.append("height: 100%;");
	  style.append("bottom: 0;");
	  style.append("top: 0;");
	  style.append("position: absolute;");
	}

	responseWriter.writeAttribute("style", style.toString(), null);
	responseWriter.endElement("div");
	writeln(responseWriter);
  }

  /**
   * Reference to {@link leaflet.jsf.js} context variable.
   */
  private static final String JS_CONTEXT_VARIABLE = "ctx";

  /**
   * Js property name that hooked into leaflet.
   */
  private static final String JS_IDENTIFIER = "jsf";

  /**
   * Default Object prefix. Doesn't represent any specialty.
   */
  private static final String JS_OBJECT_PREFIX = "leafletjsf";

  /**
   * @see #JS_OBJECT_PREFIX
   */
  private static final String JS_UNDERSCORED_OBJECT_PREFIX = "leafletjsf_";

  /**
   * Reference to {@link leaflet.jsf.js#Map#setBehaviours}.
   */
  private static final String JS_SETBEHAVIORS_METHOD = "setBehaviors";

  /**
   * @param clientId
   * @return
   */
  private String newJavascriptObject(String clientId) {
	return new StringBuilder()
			.append(JS_UNDERSCORED_OBJECT_PREFIX)
			.append(clientId)
			.append(".")
			.append(JS_IDENTIFIER)
			.toString();
  }

  /**
   * The main namespace for {@link leaflet.jsf.js}.
   */
  private static final String JS_NAMESPACE = "L.jsf";

  /**
   * In {@link leaflet.jsf.js} it holds the name of leaflet jsf component div name.
   */
  private static final String JS_CLIENT_ID = "L.jsf.clientId";

  /**
   * Reference to {@link #L.map#setView}
   */
  private static final String JS_LEAFLET_SET_VIEW_METHOD = "setView";

  private static final String JS_LEAFLET_MAP_CENTER_PROPERTY = JS_CONTEXT_VARIABLE + ".center";
  private static final String JS_LEAFLET_MAP_ZOOM_PROPERTY = JS_CONTEXT_VARIABLE + ".zoom";
  private static final String JS_LEAFLET_MAP_PROPERTY = JS_CONTEXT_VARIABLE + ".map";

  /**
   * Reference to {@link #L.map.jsf.decodeMap}
   */
  private static final String JS_DECODE_MAP_METHOD = "decodeMap";

  /**
   * @param tile
   * @return
   */
  private Token<?> getDeterminedTileLayerToken(TileLayer tile) {
	Token tlToken;

	if (tile instanceof XYZ) {
	  tlToken = new Token<>(XYZ.class);
	} else if (tile instanceof WMS) {
	  tlToken = new Token<>(WMS.class);
	} else {
	  throw new LeafletRendererException("Unresolved TileLayer. {Hint: ErrorCode:190}");
	}

	return tlToken;
  }

  /**
   * @param facesContext FacesContext
   * @param leaflet Leaflet
   * @throws IOException
   */
  protected void encodeLeaflet(FacesContext facesContext, Leaflet leaflet) throws IOException {
	ResponseWriter responseWriter = facesContext.getResponseWriter();
	String clientId = leaflet.getClientId(facesContext);
	String jsObject = JS_UNDERSCORED_OBJECT_PREFIX + clientId;
	MapModel mapmodel = leaflet.getMapmodel();
	if (Objects.isNull(mapmodel)) {
	  return;
	}

	beginScript(responseWriter);
	writeJavascriptAttribute(responseWriter);
	writeln(responseWriter);
	writeln(responseWriter, new Variable(JS_CLIENT_ID, Strings.inQuote(clientId), null));
	writeln(responseWriter, new Variable(jsObject, null, Variable.Type.VAR));

	if (Bools.isTrue(leaflet.getFullScreen())) {
	  mapmodel.getOptions().setFullscreenControl(Boolean.TRUE);
	}

	JsonKit jsonKit = new JsonKit();

	Token<MapOptions> mapOptionsToken = new Token<>(MapOptions.class);
	String mapOptionsJson = jsonKit.getJson().toJson(mapmodel.getOptions(), mapOptionsToken);

	Token tlToken = getDeterminedTileLayerToken(mapmodel.getTileLayer());
	String tileJson = jsonKit.getJson().toJson(mapmodel.getTileLayer(), tlToken);

	writeln(responseWriter, VariableBuilder.newInstance()
			.setType(Variable.Type.VAR)
			.setName(JS_CONTEXT_VARIABLE)
			.setValue(FunctionBuilder.newInstance()
					.setObjectName(JS_NAMESPACE)
					.setName(JS_DECODE_MAP_METHOD)
					.setParameter(0, Parameter.of(jsObject))
					.setParameter(1, Parameter.of(clientId))
					.setParameter(2, newJavascriptParameter(mapOptionsJson))
					.setParameter(3, newJavascriptParameter(tileJson))
					.build())
			.build());

	writeln(responseWriter, new Variable(jsObject, JS_LEAFLET_MAP_PROPERTY, null));

	encodeBehaviors(facesContext, leaflet);
	
	writeln(responseWriter, FunctionBuilder.newInstance()
			.setObjectName(jsObject)
			.setName(JS_LEAFLET_SET_VIEW_METHOD)
			.setParameter(0, Parameter.of(JS_LEAFLET_MAP_CENTER_PROPERTY))
			.setParameter(1, Parameter.of(JS_LEAFLET_MAP_ZOOM_PROPERTY))
			.build());

	encode3thPartyPlugins(facesContext, leaflet, jsonKit);
	encodeLayers(facesContext, leaflet, jsonKit);

	writeln(responseWriter);
	endScript(responseWriter);
  }

  /**
   * Instead of serializing every layer one by one, we set layers of {@link LayerMap} as {@link LayerMap#values()} and
   * let it json to serialize all of them. This strategy gains us a little bit of performance.
   *
   * @see #encodeLayers(javax.faces.context.FacesContext, tr.com.terrayazilim.leaflet.jsf.component.Leaflet,
   * tr.com.terrayazilim.leaflet.jsf.renderkit.JsonKit)
   *
   * @version 1.0.1
   * @since 1.0.1
   */
  static private class LayerHolder {

	public static final String fieldName = "stack";

	private Collection<Layer> stack;

	public LayerHolder() {
	  this.stack = null;
	}

	public LayerHolder(Collection<Layer> stack) {
	  this.stack = stack;
	}

	public Collection<Layer> getStack() {
	  return stack;
	}

	public void setStack(Collection<Layer> stack) {
	  this.stack = stack;
	}
  }

  /**
   * Reference to {@link leaflet.jsf.jsf#Map#add}.
   */
  private static final String JS_ADD_METHOD = "add";

  /**
   * Reference to {@link leaflet.jsf.jsf#Map#addAll}.
   */
  private static final String JS_ADD_ALL_METHOD = "addAll";

  /**
   * @param facesContext FacesContext
   * @param leaflet Leaflet
   * @param jsonKit JsonKit
   * @throws IOException
   */
  protected void encodeLayers(FacesContext facesContext, Leaflet leaflet, JsonKit jsonKit) throws IOException {
	ResponseWriter responseWriter = facesContext.getResponseWriter();
	String clientId = leaflet.getClientId(facesContext);
	MapModel mapmodel = leaflet.getMapmodel();
	if (mapmodel == null) {
	  return;
	}

	Collection<Layer> stack = mapmodel.values();
	if (!stack.isEmpty()) {
	  try {
		// init holder.
		LayerHolder layerHolder = new LayerHolder(stack);

		// get serialized layer as json.
		String json = jsonKit.getJson().toJson(layerHolder, new Token<>(LayerHolder.class));
		// get current js object.
		String jsObject = newJavascriptObject(clientId);
		// create string literal parameter
		Parameter parameter = newJavascriptParameter(json);

		Field field = null;
		try {
		  field = layerHolder.getClass().getDeclaredField(LayerHolder.fieldName);
		} catch (NoSuchFieldException | SecurityException ex) {
		  throw new LeafletRendererException("LayerHolder collection field doesn't match.{Hint: ErrorCode:194}", ex);
		}

		if (field != null) {
		  Parameter keyParameter = newJavascriptParameter(field.getName());
		  String jsMethod = newJavascriptMethod(jsObject, JS_ADD_ALL_METHOD, parameter, keyParameter);
		  writeln(responseWriter, jsMethod);
		}
	  } catch (Exception e) {
		throw new LeafletRendererException("Layers Cannot encode. {Hint:ErrorCode:265}", e);
	  }
	}
  }

  /**
   * @see leaflet.jsf.js#L.Map.jsf.configurePolylineMeasurement
   */
  private static final String JS_CONFIGURE_POLYLINE_MEASUREMENT_METHOD = "configurePolylineMeasurement";

  /**
   * @see leaflet.jsf.js#L.Map.jsf.configurePm
   */
  private static final String JS_CONFIGURE_PM_METHOD = "configurePm";

  /**
   * @see leaflet.jsf.js#L.Map.jsf.configureEasyPrint
   */
  private static final String JS_CONFIGURE_EASY_PRINT_METHOD = "configureEasyPrint";

  /**
   * @see leaflet.jsf.js#L.Map.jsf.configureBoxZoom
   */
  private static final String JS_CONFIGURE_BOXZOOM_METHOD = "configureBoxZoom";
  
  private static final String JS_LEAFLET_TILE_LAYER_PROPERTY = JS_CONTEXT_VARIABLE + ".tilelayer";

  /**
   * @param facesContext FacesContext
   * @param leaflet Leaflet
   * @param jsonKit JsonKit
   * @throws java.io.IOException
   */
  protected void encode3thPartyPlugins(FacesContext facesContext, Leaflet leaflet, JsonKit jsonKit) throws IOException {
	ResponseWriter responseWriter = facesContext.getResponseWriter();
	String clientId = leaflet.getClientId(facesContext);
	MapModel mapmodel = leaflet.getMapmodel();
	if (mapmodel == null) {
	  return;
	}

	String jsObject = newJavascriptObject(clientId);
	if (Bools.isTrue(leaflet.getBoxZoom())) {
	  Parameter parameter = null;
	  String jsMethod = newJavascriptMethod(jsObject, JS_CONFIGURE_BOXZOOM_METHOD, parameter);
	  writeln(responseWriter, jsMethod);
	}

	if (Bools.isTrue(leaflet.getEasyPrint())) {
	  Token<EasyPrint> token = new Token<>(EasyPrint.class);
	  String json = jsonKit.getJson().toJson(mapmodel.getEasyPrint(), token);

	  Parameter optionsParameter = newJavascriptParameter(json);
	  Parameter tileParameter = Parameter.of(JS_LEAFLET_TILE_LAYER_PROPERTY);
	  String jsMethod = newJavascriptMethod(jsObject, JS_CONFIGURE_EASY_PRINT_METHOD, optionsParameter, tileParameter);
	  writeln(responseWriter, jsMethod);
	}

	if (Bools.isTrue(leaflet.getPm())) {
	  Token<Pm> token = new Token<>(Pm.class);
	  String json = jsonKit.getJson().toJson(mapmodel.getPm(), token);

	  Parameter parameter = newJavascriptParameter(json);
	  String jsMethod = newJavascriptMethod(jsObject, JS_CONFIGURE_PM_METHOD, parameter);
	  writeln(responseWriter, jsMethod);
	}

	if (Bools.isTrue(leaflet.getPolylineMeasure())) {
	  Token<PolylineMeasure> token = new Token<>(PolylineMeasure.class);
	  String json = jsonKit.getJson().toJson(mapmodel.getPolylineMeasure(), token);

	  Parameter parameter = newJavascriptParameter(json);
	  String jsMethod = newJavascriptMethod(jsObject, JS_CONFIGURE_POLYLINE_MEASUREMENT_METHOD, parameter);
	  writeln(responseWriter, jsMethod);
	}
  }

  /**
   * {eventName: { mojarra.ab(...);: }, eventName2: { jsf.ajax.request(...); }}
   *
   * @param context FacesContext
   * @param component ClientBehaviorHolder
   * @throws IOException IOException
   */
  protected void encodeBehaviors(FacesContext context, ClientBehaviorHolder component) throws IOException {
	java.util.Map<String, List<ClientBehavior>> clientBehaviors = component.getClientBehaviors();
	ResponseWriter responseWriter = context.getResponseWriter();

	// Magic. Do not touch.
	if (clientBehaviors != null && !clientBehaviors.isEmpty()) {
	  boolean written = false;
	  Collection<String> eventNames = clientBehaviors.keySet();
	  String clientId = ((UIComponent) component).getClientId(context);
	  List<ClientBehaviorContext.Parameter> nullParameters = Collections.emptyList();
	  responseWriter.write(JS_UNDERSCORED_OBJECT_PREFIX + clientId + "." + JS_IDENTIFIER + "." + JS_SETBEHAVIORS_METHOD + "({");
	  for (String eventName : eventNames) {
		List<ClientBehavior> eventBehaviors = clientBehaviors.get(eventName);
		if (eventBehaviors != null && !eventBehaviors.isEmpty()) {
		  if (written) {
			responseWriter.write(",");
		  }
		  int eventBehaviorsSize = eventBehaviors.size();
		  responseWriter.write(eventName + ":");
		  responseWriter.write("function(package, event) {");
		  if (eventBehaviorsSize > 1) {
			boolean chained = false;
			responseWriter.write("L.jsf.util.chain(package, event,[");
			for (int i = 0; i < eventBehaviorsSize; i++) {
			  ClientBehavior behavior = eventBehaviors.get(i);
			  ClientBehaviorContext behaviorContext = ClientBehaviorContext.createClientBehaviorContext(
					  context, (UIComponent) component, eventName, clientId, nullParameters
			  );
			  String jsfAjax = behavior.getScript(behaviorContext);
			  if (jsfAjax != null) {
				if (chained) {
				  responseWriter.write(",");
				}

				responseWriter.write("function(package, event) {");
				jsfAjax = jsfAjax.substring(0, jsfAjax.length() - 1);
				jsfAjax = jsfAjax + ", " + "package" + ")";

				responseWriter.write(jsfAjax);
				responseWriter.write("}");
				chained = true;
			  }
			}
			responseWriter.write("]);");
		  } else {
			ClientBehavior behavior = eventBehaviors.get(0);
			ClientBehaviorContext behaviorContext = ClientBehaviorContext.createClientBehaviorContext(
					context, (UIComponent) component, eventName, clientId, nullParameters
			);

			String jsfAjax = behavior.getScript(behaviorContext);
			if (jsfAjax != null) {
			  jsfAjax = jsfAjax.substring(0, jsfAjax.length() - 1);
			  jsfAjax = jsfAjax + ", " + "package" + ")";

			  responseWriter.write(jsfAjax);
			}
		  }

		  responseWriter.write(";}");
		  written = true;
		}
	  }

	  responseWriter.write("});");
	}

	writeln(responseWriter);
  }

  /**
   * @param context
   * @param component
   * @throws IOException
   */
  @Override
  public void encodeEnd(FacesContext context, UIComponent component) throws IOException {
	encodeScript(context, (Leaflet) component);
	encodeLeaflet(context, (Leaflet) component);
  }

  /**
   * Do not change.
   *
   * @param context
   * @param component
   */
  @Override
  public void decode(FacesContext context, UIComponent component) {
	if (!(component instanceof ClientBehaviorHolder)) {
	  return;
	}

	java.util.Map<String, List<ClientBehavior>> behaviors = ((ClientBehaviorHolder) component).getClientBehaviors();
	if (behaviors == null || behaviors.isEmpty()) {
	  return;
	}

	java.util.Map<String, String> params = context.getExternalContext().getRequestParameterMap();
	String behaviorEvent = params.get("javax.faces.behavior.event");
	if (behaviorEvent == null) {
	  return;
	}

	List<ClientBehavior> behaviorsForEvent = behaviors.get(behaviorEvent);
	if (!(behaviorsForEvent == null || behaviorsForEvent.isEmpty())) {
	  String behaviorSource = params.get("javax.faces.source");
	  String clientId = component.getClientId(context);
	  if (behaviorSource != null && clientId.equals(behaviorSource)) {
		for (ClientBehavior behavior : behaviorsForEvent) {
		  behavior.decode(context, component);
		}
	  }
	}

	// ne günü ne gecesi,
	// gözünü kırpmaz,
	// rüya görmez,
	// nefesi hissedilmez.
  }

  /**
   * @param context
   * @param component
   * @throws IOException
   */
  @Override
  public void encodeChildren(FacesContext context, UIComponent component) throws IOException {
	// İyiler ilk görüşte tanınmaz.
  }

  /**
   * @return
   */
  @Override
  public boolean getRendersChildren() {
	return true;
  }
}
