/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import tr.com.terrayazilim.leaflet.jsf.layer.LayerGroupImpl;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import tr.com.terrayazilim.core.lambda.Predicate;
import tr.com.terrayazilim.leaflet.jsf.layer.Circle;
import tr.com.terrayazilim.leaflet.jsf.layer.Layer;
import tr.com.terrayazilim.leaflet.jsf.layer.Marker;
import tr.com.terrayazilim.leaflet.jsf.layer.Polygon;
import tr.com.terrayazilim.leaflet.jsf.layer.Polyline;
import tr.com.terrayazilim.leaflet.jsf.layer.Rectangle;

/**
 * DevNote: Preferred {@link Map} implementation is {@link LinkedHashMap} 
 * for {@link LayerMap}.
 * 
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public interface LayerMap extends Map<String, Layer> {

  /**
   * @param layer
   * @return 
   */
  public Layer put(Layer layer);
  
  /**
   * @param key
   * @return 
   */
  public Marker getMarker(Object key);
  
  /**
   * @param key
   * @return 
   */
  public Circle getCircle(Object key);
  
  /**
   * @param key
   * @return 
   */
  public Rectangle getRectangle(Object key);
  
  /**
   * @param key
   * @return 
   */
  public Polyline getPolyline(Object key);
  
  /**
   * @param key
   * @return 
   */
  public Polygon getPolygon(Object key);
  
  /**
   * @param key
   * @return 
   */
  public LayerGroupImpl getLayerGroup(Object key);
  
  /**
   * @param predicate
   * @return 
   */
  public Collection<Layer> filter(Predicate<Layer> predicate);
  
  @Override
  public Set<Entry<String, Layer>> entrySet();

  @Override
  public Collection<Layer> values();

  @Override
  public Set<String> keySet();

  @Override
  public void clear();

  @Override
  public void putAll(Map<? extends String, ? extends Layer> m);

  @Override
  public Layer remove(Object key);

  @Override
  public Layer put(String key, Layer value);

  @Override
  public Layer get(Object key);

  @Override
  public boolean containsValue(Object value);

  @Override
  public boolean containsKey(Object key);

  @Override
  public boolean isEmpty();

  @Override
  public int size();
  
}
