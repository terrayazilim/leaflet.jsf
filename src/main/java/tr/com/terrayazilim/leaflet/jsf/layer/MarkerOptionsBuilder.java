/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.layer;

import tr.com.terrayazilim.leaflet.jsf.Icon;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 * @since 1.0.0
 */
public final class MarkerOptionsBuilder {

  public Icon icon = new Icon();
  public Boolean draggable;
  public Boolean keyboard;
  public String title;
  public String alt;
  public Integer zIndexOffset;
  public Double opacity;
  public Boolean riseOnHover;
  public Integer riseOffset;
  public String pane;

  public MarkerOptionsBuilder() {
  }

  public MarkerOptions build() {
    return new MarkerOptions(this);
  }

  /**
   * @param icon
   * @return
   */
  public MarkerOptionsBuilder setIcon(Icon icon) {
    this.icon = icon;
    return this;
  }

  /**
   * @param draggable
   * @return
   */
  public MarkerOptionsBuilder setDraggable(Boolean draggable) {
    this.draggable = draggable;
    return this;
  }

  /**
   * @param keyboard
   * @return
   */
  public MarkerOptionsBuilder setKeyboard(Boolean keyboard) {
    this.keyboard = keyboard;
    return this;
  }

  /**
   * @param title
   * @return
   */
  public MarkerOptionsBuilder setTitle(String title) {
    this.title = title;
    return this;
  }

  /**
   * @param alt
   * @return
   */
  public MarkerOptionsBuilder setAlt(String alt) {
    this.alt = alt;
    return this;
  }

  /**
   * @param zIndexOffset
   * @return
   */
  public MarkerOptionsBuilder setzIndexOffset(Integer zIndexOffset) {
    this.zIndexOffset = zIndexOffset;
    return this;
  }

  /**
   * @param opacity
   * @return
   */
  public MarkerOptionsBuilder setOpacity(Double opacity) {
    this.opacity = opacity;
    return this;
  }

  /**
   * @param riseOnHover
   * @return
   */
  public MarkerOptionsBuilder setRiseOnHover(Boolean riseOnHover) {
    this.riseOnHover = riseOnHover;
    return this;
  }

  /**
   * @param riseOffset
   * @return
   */
  public MarkerOptionsBuilder setRiseOffset(Integer riseOffset) {
    this.riseOffset = riseOffset;
    return this;
  }

  /**
   * @param pane
   * @return
   */
  public MarkerOptionsBuilder setPane(String pane) {
    this.pane = pane;
    return this;
  }
}
