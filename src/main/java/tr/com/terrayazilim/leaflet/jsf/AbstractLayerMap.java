/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import tr.com.terrayazilim.leaflet.jsf.layer.LayerGroupImpl;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import tr.com.terrayazilim.core.Objects;
import tr.com.terrayazilim.core.lambda.Predicate;
import tr.com.terrayazilim.core.reflect.Classes;
import tr.com.terrayazilim.leaflet.jsf.layer.Circle;
import tr.com.terrayazilim.leaflet.jsf.layer.Layer;
import tr.com.terrayazilim.leaflet.jsf.layer.Marker;
import tr.com.terrayazilim.leaflet.jsf.layer.Polygon;
import tr.com.terrayazilim.leaflet.jsf.layer.Polyline;
import tr.com.terrayazilim.leaflet.jsf.layer.Rectangle;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public abstract class AbstractLayerMap implements LayerMap {
  
  /**
   * @return 
   */
  protected abstract Map<String, Layer> getMap();
  
  /**
   * @param map 
   */
  protected abstract void setMap(Map<String, Layer> map);

  @Override
  public Layer put(Layer layer) {
    if (Objects.isNull(layer)) {
      return null;
    }
    
    return put(layer.getLayerId(), layer);
  }

  @Override
  public Marker getMarker(Object key) {
    final Layer marker = getCheckedLayer(key, Marker.class);
    if (Objects.isNull(marker)) {
      return null;
    }
    
    return (Marker) marker;
  }

  @Override
  public Circle getCircle(Object key) {
    final Layer circle = getCheckedLayer(key, Circle.class);
    if (Objects.isNull(circle)) {
      return null;
    }
    
    return (Circle) circle;
  }

  @Override
  public Rectangle getRectangle(Object key) {
    final Layer rectangle = getCheckedLayer(key, Rectangle.class);
    if (Objects.isNull(rectangle)) {
      return null;
    }
    
    return (Rectangle) rectangle;
  }

  @Override
  public Polyline getPolyline(Object key) {
    final Layer polyline = getCheckedLayer(key, Polyline.class);
    if (Objects.isNull(polyline)) {
      return null;
    }
    
    return (Polyline) polyline;
  }

  @Override
  public Polygon getPolygon(Object key) {
    final Layer polygon = getCheckedLayer(key, Polygon.class);
    if (Objects.isNull(polygon)) {
      return null;
    }
    
    return (Polygon) polygon;
  }

  @Override
  public LayerGroupImpl getLayerGroup(Object key) {
    final Layer layerGroup = getCheckedLayer(key, LayerGroupImpl.class);
    if (Objects.isNull(layerGroup)) {
      return null;
    }
    
    return (LayerGroupImpl) layerGroup;
  }
  
  /**
   * @param key
   * @param type
   * @return 
   */
  protected Layer getCheckedLayer(Object key, Class<?> type) {
    final Layer layer = get(key);
    
    if (Objects.isNull(layer)) {
      return null;
    } else if (!Classes.instanceOf(layer, type)) {
      return null;
    }
    
    return layer;
  }

  @Override
  public Collection<Layer> filter(Predicate<Layer> predicate) {
    final Collection<Layer> stack = new ArrayList<>();
    
    for (Entry<String, Layer> entry : this.entrySet()) {
      final Layer value = entry.getValue();
      if (predicate.call(value)) {
        stack.add(value);
      }
    }
    
    return stack;
  }

  @Override
  public Set<Entry<String, Layer>> entrySet() {
    if (Objects.isNull(getMap())) {
      return Collections.emptySet();
    }
    
    return getMap().entrySet();
  }

  @Override
  public Collection<Layer> values() {
    if (Objects.isNull(getMap())) {
      return Collections.emptyList();
    }
    
    return getMap().values();
  }

  @Override
  public Set<String> keySet() {
    if (Objects.isNull(getMap())) {
      return Collections.emptySet();
    }
    
    return getMap().keySet();
  }

  @Override
  public void clear() {
    if (Objects.nonNull(getMap())) {
      getMap().clear();
    }
  }

  @Override
  public void putAll(Map<? extends String, ? extends Layer> m) {
    if (Objects.nonNull(m)) {
      if (Objects.isNull(getMap())) {
        setMap(new LinkedHashMap<String, Layer>());
      }
      
      getMap().putAll(m);
    }
  }

  @Override
  public Layer remove(Object key) {
    if (Objects.isNull(getMap()) || Objects.isNull(key)) {
      return null;
    }
    
    return getMap().remove(key);
  }

  @Override
  public Layer put(String key, Layer value) {
    if (Objects.isNull(key) || Objects.isNull(value)) {
      return null;
    }
    
    if (Objects.isNull(getMap())) {
      setMap(new LinkedHashMap<String, Layer>());
    }
    
    return getMap().put(key, value);
  }

  @Override
  public Layer get(Object key) {
    if (Objects.isNull(key)) {
      return null;
    } else if (!(key instanceof String)) {
      return null;
    } else if (Objects.isNull(getMap())) {
      return null;
    }
    
    return getMap().get(key);
  }

  @Override
  public boolean containsValue(Object value) {
    if (Objects.isNull(value)) {
      return false;
    } else if (!(value instanceof String)) {
      return false;
    } else if (Objects.isNull(getMap())) {
      return false;
    }
    
    return getMap().containsValue(value);
  }

  @Override
  public boolean containsKey(Object key) {
    if (Objects.isNull(key)) {
      return false;
    } else if (!(key instanceof String)) {
      return false;
    } else if (Objects.isNull(getMap())) {
      return false;
    }
    
    return getMap().containsKey(key);
  }

  @Override
  public boolean isEmpty() {
    if (getMap() == null) {
      return true;
    }
    
    return getMap().isEmpty();
  }

  @Override
  public int size() {
    if (getMap() == null) {
      return 0;
    }
    
    return getMap().size();
  }
}
