/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import java.io.Serializable;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public class DivOverlay implements Serializable {

  private Attribution attribution;
  private Point offset;
  private String className;
  private Pane pane;

  public DivOverlay() {
  }

  /**
   * @return
   */
  public Attribution getAttribution() {
    return attribution;
  }

  /**
   * @param attribution
   */
  public void setAttribution(Attribution attribution) {
    this.attribution = attribution;
  }

  /**
   * @return
   */
  public Point getOffset() {
    return offset;
  }

  /**
   * @param offset
   */
  public void setOffset(Point offset) {
    this.offset = offset;
  }

  /**
   * @return
   */
  public String getClassName() {
    return className;
  }

  /**
   * @param className
   */
  public void setClassName(String className) {
    this.className = className;
  }

  /**
   * @return
   */
  public Pane getPane() {
    return pane;
  }

  /**
   * @param pane
   */
  public void setPane(Pane pane) {
    this.pane = pane;
  }
}
