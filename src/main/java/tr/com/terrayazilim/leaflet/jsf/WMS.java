/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import java.io.Serializable;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public class WMS extends TileLayer implements Serializable {

  private String layers;
  private String styles;
  private String format;
  private Boolean transparent;
  private String version;
  private String crs;
  private Boolean uppercase;

  public WMS() {
  }

  /**
   * @param url
   */
  public WMS(String url) {
    super(url);
  }

  /**
   * @return
   */
  public String getLayers() {
    return layers;
  }

  /**
   * @param layers
   */
  public void setLayers(String layers) {
    this.layers = layers;
  }

  /**
   * @return
   */
  public String getStyles() {
    return styles;
  }

  /**
   * @param styles
   */
  public void setStyles(String styles) {
    this.styles = styles;
  }

  /**
   * @return
   */
  public String getFormat() {
    return format;
  }

  /**
   * @param format
   */
  public void setFormat(String format) {
    this.format = format;
  }

  /**
   * @return
   */
  public Boolean getTransparent() {
    return transparent;
  }

  /**
   * @param transparent
   */
  public void setTransparent(Boolean transparent) {
    this.transparent = transparent;
  }

  /**
   * @return
   */
  public String getVersion() {
    return version;
  }

  /**
   * @param version
   */
  public void setVersion(String version) {
    this.version = version;
  }

  /**
   * @return
   */
  public String getCrs() {
    return crs;
  }

  /**
   * @param crs
   */
  public void setCrs(String crs) {
    this.crs = crs;
  }

  /**
   * @return
   */
  public Boolean getUppercase() {
    return uppercase;
  }

  /**
   * @param uppercase
   */
  public void setUppercase(Boolean uppercase) {
    this.uppercase = uppercase;
  }
}
