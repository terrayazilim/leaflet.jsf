/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.layer;

import java.io.Serializable;
import tr.com.terrayazilim.leaflet.jsf.Pane;
import tr.com.terrayazilim.leaflet.jsf.Popup;
import tr.com.terrayazilim.leaflet.jsf.Renderer;
import tr.com.terrayazilim.leaflet.jsf.Style;
import tr.com.terrayazilim.leaflet.jsf.Tooltip;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public interface LayerOptions extends Serializable {

  /**
   * @return 
   */
  Style toStyle();
  
  /**
   * @return
   */
  Boolean getInteractive();

  /**
   * @param interactive
   */
  void setInteractive(Boolean interactive);

  /**
   * @return
   */
  Tooltip getTooltip();

  /**
   * @param tooltip
   */
  void setTooltip(Tooltip tooltip);

  /**
   * @return
   */
  Popup getPopup();

  /**
   * @param popup
   */
  void setPopup(Popup popup);

  /**
   * @return
   */
  Pane getPane();

  /**
   * @param pane
   */
  void setPane(Pane pane);

  /**
   * @return
   */
  Renderer getRenderer();

  /**
   * @param renderer
   */
  void setRenderer(Renderer renderer);
}
