/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import tr.com.terrayazilim.leaflet.jsf.layer.LayerGroupImpl;
import java.util.List;
import java.util.Map;
import tr.com.terrayazilim.leaflet.jsf.layer.Circle;
import tr.com.terrayazilim.leaflet.jsf.layer.CircleOptions;
import tr.com.terrayazilim.leaflet.jsf.layer.CircleImpl;
import tr.com.terrayazilim.leaflet.jsf.layer.MarkerImpl;
import tr.com.terrayazilim.leaflet.jsf.layer.PolygonImpl;
import tr.com.terrayazilim.leaflet.jsf.layer.PolylineImpl;
import tr.com.terrayazilim.leaflet.jsf.layer.RectangleImpl;
import tr.com.terrayazilim.leaflet.jsf.layer.Layer;
import tr.com.terrayazilim.leaflet.jsf.layer.Marker;
import tr.com.terrayazilim.leaflet.jsf.layer.MarkerOptions;
import tr.com.terrayazilim.leaflet.jsf.layer.Polygon;
import tr.com.terrayazilim.leaflet.jsf.layer.PolygonOptions;
import tr.com.terrayazilim.leaflet.jsf.layer.Polyline;
import tr.com.terrayazilim.leaflet.jsf.layer.PolylineOptions;
import tr.com.terrayazilim.leaflet.jsf.layer.Rectangle;
import tr.com.terrayazilim.leaflet.jsf.layer.RectangleOptions;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 * @since 1.0.0
 */
public final class L {

  private L() {
  }
  
  /**
   * @param latLng
   * @return
   */
  public static Marker marker(LatLng latLng) {
    return new MarkerImpl(latLng);
  }

  /**
   * @param latLng
   * @param options
   * @return
   */
  public static Marker marker(LatLng latLng, MarkerOptions options) {
    return new MarkerImpl(latLng, options);
  }

  /**
   * @param latLng
   * @param radius
   * @return
   */
  public static Circle circle(LatLng latLng, Double radius) {
    return new CircleImpl(latLng, radius);
  }

  /**
   * @param latLng
   * @param options
   * @return
   */
  public static Circle circle(LatLng latLng, CircleOptions options) {
    return new CircleImpl(latLng, options);
  }

  /**
   * @param latLng0
   * @param latLng1
   * @return
   */
  public static Rectangle rectangle(LatLng latLng0, LatLng latLng1) {
    return new RectangleImpl(latLng0, latLng1);
  }

  /**
   * @param latLng0
   * @param latLng1
   * @param options
   * @return
   */
  public static Rectangle rectangle(LatLng latLng0, LatLng latLng1, RectangleOptions options) {
    return new RectangleImpl(latLng0, latLng1, options);
  }

  /**
   * @param latLngs
   * @return
   */
  public static Polyline polyline(List<List<LatLng>> latLngs) {
    return new PolylineImpl(latLngs);
  }

  /**
   * @param latLngs
   * @param options
   * @return
   */
  public static Polyline polyline(List<List<LatLng>> latLngs, PolylineOptions options) {
    return new PolylineImpl(latLngs, options);
  }

  /**
   * @param latLngs
   * @return
   */
  public static Polygon polygon(List<List<LatLng>> latLngs) {
    return new PolygonImpl(latLngs);
  }

  /**
   * @param latLngs
   * @param options
   * @return
   */
  public static Polygon polygon(List<List<LatLng>>latLngs, PolygonOptions options) {
    return new PolygonImpl(latLngs, options);
  }

  /**
   * @return
   */
  public static Canvas canvas() {
    return new Canvas();
  }

  /**
   * @return
   */
  public static RoughCanvas roughCanvas() {
    return new RoughCanvas();
  }

  /**
   * @return
   */
  public static DivIcon divIcon() {
    return new DivIcon();
  }

  /**
   * @param iconUrl
   * @return
   */
  public static DivIcon divIcon(String iconUrl) {
    return new DivIcon(iconUrl);
  }

  /**
   * @param iconUrl
   * @param className
   * @return
   */
  public static DivIcon divIcon(String iconUrl, String className) {
    return new DivIcon(iconUrl, className);
  }

  /**
   * @return
   */
  public static DivOverlay divOverlay() {
    return new DivOverlay();
  }

  /**
   * @return
   */
  public static GridLayer gridLayer() {
    return new GridLayer();
  }

  /**
   * @return
   */
  public static Icon icon() {
    return new Icon();
  }

  /**
   * @param iconUrl
   * @return
   */
  public static Icon icon(String iconUrl) {
    return new Icon(iconUrl);
  }

  /**
   * @param lat
   * @param lng
   * @return
   */
  public static LatLng latLng(Double lat, Double lng) {
    return new LatLng(lat, lng);
  }

  /**
   * @param southWest
   * @param northEast
   * @return
   */
  public static LatLngBounds latLngBounds(LatLng southWest, LatLng northEast) {
    return new LatLngBounds(southWest, northEast);
  }

  /**
   * @return
   */
  public static LayerGroupImpl layerGroup() {
    return new LayerGroupImpl();
  }

  /**
   * @param layers
   * @return
   */
  public static LayerGroupImpl layerGroup(java.util.Map<String, Layer> layers) {
    return new LayerGroupImpl(layers);
  }

  /**
   * @param x
   * @param y
   * @return
   */
  public static Point point(Double x, Double y) {
    return new Point(x, y);
  }

  /**
   * @param content
   * @return
   */
  public static Popup popup(String content) {
    return new Popup(content);
  }

  /**
   * @param content
   * @return
   */
  public static Popup responsivePopup(String content) {
    Popup popup = new Popup(content);
    popup.setResponsive(Boolean.TRUE);

    return popup;
  }

  /**
   * @param content
   * @return
   */
  public static Tooltip tooltip(String content) {
    return new Tooltip(content);
  }

  /**
   * @param isWMS
   * @param tileUrl
   * @return
   */
  public static TileLayer tileLayer(String tileUrl, boolean isWMS) {
    return isWMS ? new WMS(tileUrl) : new XYZ(tileUrl);
  }

  /**
   * @param isWMS
   * @return
   */
  public static TileLayer tileLayer(boolean isWMS) {
    return isWMS ? new WMS() : new XYZ();
  }

  /**
   * @return
   */
  public static EasyPrint easyPrint() {
    return new EasyPrint();
  }
  
  /**
   * @return 
   */
  public static MapModel map() {
    return new MapModelImpl();
  }
  
  /**
   * @param map
   * @return 
   */
  public static MapModel map(Map<String, Layer> map) {
    return new MapModelImpl(map);
  }
}
