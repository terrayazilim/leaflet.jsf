/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.convert;

import java.math.BigDecimal;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.BigDecimalConverter;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import tr.com.terrayazilim.core.math.BigLatitude;
import tr.com.terrayazilim.core.math.BigLongitude;
import tr.com.terrayazilim.leaflet.jsf.LatLng;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 * @since 1.0.1
 */
@FacesConverter(value = "LatLngConverter")
public class LatLngConverter implements Converter {

  @Override
  public Object getAsObject(FacesContext context, UIComponent component, String value) {
	String[] latlng = value.split(",");
	String lat = latlng[0];
	String lng = latlng[1];

	BigDecimalConverter converter = new BigDecimalConverter();
	LatLng object = new LatLng();
	BigDecimal latDecimal;
	BigDecimal lngDecimal;

	try {
	  latDecimal = (BigDecimal) converter.getAsObject(context, component, lat);
	  lngDecimal = (BigDecimal) converter.getAsObject(context, component, lng);
	} catch (Exception e) {
	  throw new LeafletConverterException("LatLng cannot be converted. {Hint: ErrorCode:197}", e);
	}

	BigLatitude bigLat = new BigLatitude(latDecimal);
	BigLongitude bigLon = new BigLongitude(lngDecimal);

	object.setBigLatitude(bigLat);
	object.setBigLongitude(bigLon);

	return object;
  }

  @Override
  public String getAsString(FacesContext context, UIComponent component, Object value) {
	BigDecimalConverter converter = new BigDecimalConverter();
	StringBuilder stringBuilder = new StringBuilder();

	String lat;
	String lng;

	try {
	  LatLng latlng = (LatLng) value;
	  lat = converter.getAsString(context, component, latlng.getBigLatitude().getLat());
	  lng = converter.getAsString(context, component, latlng.getBigLongitude().getLon());
	} catch (Exception e) {
	  throw new LeafletConverterException("LatLng cannot be converted. {Hint: ErrorCode:198}", e);
	}

	return stringBuilder.append(lat)
			.append(",")
			.append(lng)
			.toString();
  }

}
