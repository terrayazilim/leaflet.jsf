/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import java.io.Serializable;
import tr.com.terrayazilim.leaflet.jsf.layer.CircleOptions;
import tr.com.terrayazilim.leaflet.jsf.layer.PolylineOptions;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public class PolylineMeasure implements Serializable {

  static public enum Position {

    topLeft("topLeft"),
    topRight("topRight"),
    bottomLeft("bottomLeft"),
    bottomRight("bottomRight");

    private final String pointer;

    /**
     * @param pointer
     */
    private Position(String pointer) {
      this.pointer = pointer;
    }

    @Override
    public String toString() {
      return this.pointer;
    }
  }

  private Position position;

  private String bearingTextIn;
  private String bearingTextOut;
  private String bindTooltipText;
  private String changeUnitsText;
  private String unit;
  private String measureControlTitleOn = "Uzunluk Ölçümünü Aktive Et";
  private String measureControlTitleOff = "Uzunluk Ölçümünü Deaktive Et";
  private String measureControlLabel;
  private String cursor;
  private String backgroundColor;
  private String clearControlTitle = "Ölçümleri Temizle";
  private String clearControlLabel;

  private Boolean showBearings = true;
  private Boolean clearMeasurementsOnStop = false;
  private Boolean showMeasurementsClearControl = true;
  private Boolean showUnitControl;

  private PolylineOptions tempLine;
  private PolylineOptions fixedLine;

  private CircleOptions startCircle;
  private CircleOptions intermedCircle;
  private CircleOptions currentCircle;
  private CircleOptions endCircle;

  public PolylineMeasure() {
  }

  /**
   * @return
   */
  public Position getPosition() {
    return position;
  }

  /**
   * @param position
   */
  public void setPosition(Position position) {
    this.position = position;
  }

  /**
   * @return
   */
  public String getBearingTextIn() {
    return bearingTextIn;
  }

  /**
   * @param bearingTextIn
   */
  public void setBearingTextIn(String bearingTextIn) {
    this.bearingTextIn = bearingTextIn;
  }

  /**
   * @return
   */
  public String getBearingTextOut() {
    return bearingTextOut;
  }

  /**
   * @param bearingTextOut
   */
  public void setBearingTextOut(String bearingTextOut) {
    this.bearingTextOut = bearingTextOut;
  }

  /**
   * @return
   */
  public String getBindTooltipText() {
    return bindTooltipText;
  }

  /**
   * @param bindTooltipText
   */
  public void setBindTooltipText(String bindTooltipText) {
    this.bindTooltipText = bindTooltipText;
  }

  /**
   * @return
   */
  public String getChangeUnitsText() {
    return changeUnitsText;
  }

  /**
   * @param changeUnitsText
   */
  public void setChangeUnitsText(String changeUnitsText) {
    this.changeUnitsText = changeUnitsText;
  }

  /**
   * @return
   */
  public String getUnit() {
    return unit;
  }

  /**
   * @param unit
   */
  public void setUnit(String unit) {
    this.unit = unit;
  }

  /**
   * @return
   */
  public String getMeasureControlTitleOn() {
    return measureControlTitleOn;
  }

  /**
   * @param measureControlTitleOn
   */
  public void setMeasureControlTitleOn(String measureControlTitleOn) {
    this.measureControlTitleOn = measureControlTitleOn;
  }

  /**
   * @return
   */
  public String getMeasureControlTitleOff() {
    return measureControlTitleOff;
  }

  /**
   * @param measureControlTitleOff
   */
  public void setMeasureControlTitleOff(String measureControlTitleOff) {
    this.measureControlTitleOff = measureControlTitleOff;
  }

  /**
   * @return
   */
  public String getMeasureControlLabel() {
    return measureControlLabel;
  }

  /**
   * @param measureControlLabel
   */
  public void setMeasureControlLabel(String measureControlLabel) {
    this.measureControlLabel = measureControlLabel;
  }

  /**
   * @return
   */
  public String getCursor() {
    return cursor;
  }

  /**
   * @param cursor
   */
  public void setCursor(String cursor) {
    this.cursor = cursor;
  }

  /**
   * @return
   */
  public String getBackgroundColor() {
    return backgroundColor;
  }

  /**
   * @param backgroundColor
   */
  public void setBackgroundColor(String backgroundColor) {
    this.backgroundColor = backgroundColor;
  }

  /**
   * @return
   */
  public String getClearControlTitle() {
    return clearControlTitle;
  }

  /**
   * @param clearControlTitle
   */
  public void setClearControlTitle(String clearControlTitle) {
    this.clearControlTitle = clearControlTitle;
  }

  /**
   * @return
   */
  public String getClearControlLabel() {
    return clearControlLabel;
  }

  /**
   * @param clearControlLabel
   */
  public void setClearControlLabel(String clearControlLabel) {
    this.clearControlLabel = clearControlLabel;
  }

  /**
   * @return
   */
  public Boolean getShowBearings() {
    return showBearings;
  }

  /**
   * @param showBearings
   */
  public void setShowBearings(Boolean showBearings) {
    this.showBearings = showBearings;
  }

  /**
   * @return
   */
  public Boolean getClearMeasurementsOnStop() {
    return clearMeasurementsOnStop;
  }

  /**
   * @param clearMeasurementsOnStop
   */
  public void setClearMeasurementsOnStop(Boolean clearMeasurementsOnStop) {
    this.clearMeasurementsOnStop = clearMeasurementsOnStop;
  }

  /**
   * @return
   */
  public Boolean getShowMeasurementsClearControl() {
    return showMeasurementsClearControl;
  }

  /**
   * @param showMeasurementsClearControl
   */
  public void setShowMeasurementsClearControl(Boolean showMeasurementsClearControl) {
    this.showMeasurementsClearControl = showMeasurementsClearControl;
  }

  /**
   * @return
   */
  public Boolean getShowUnitControl() {
    return showUnitControl;
  }

  /**
   * @param showUnitControl
   */
  public void setShowUnitControl(Boolean showUnitControl) {
    this.showUnitControl = showUnitControl;
  }

  /**
   * @return
   */
  public PolylineOptions getTempLine() {
    return tempLine;
  }

  /**
   * @param tempLine
   */
  public void setTempLine(PolylineOptions tempLine) {
    this.tempLine = tempLine;
  }

  /**
   * @return
   */
  public PolylineOptions getFixedLine() {
    return fixedLine;
  }

  /**
   * @param fixedLine
   */
  public void setFixedLine(PolylineOptions fixedLine) {
    this.fixedLine = fixedLine;
  }

  /**
   * @return
   */
  public CircleOptions getStartCircle() {
    return startCircle;
  }

  /**
   * @param startCircle
   */
  public void setStartCircle(CircleOptions startCircle) {
    this.startCircle = startCircle;
  }

  /**
   * @return
   */
  public CircleOptions getIntermedCircle() {
    return intermedCircle;
  }

  /**
   * @param intermedCircle
   */
  public void setIntermedCircle(CircleOptions intermedCircle) {
    this.intermedCircle = intermedCircle;
  }

  /**
   * @return
   */
  public CircleOptions getCurrentCircle() {
    return currentCircle;
  }

  /**
   * @param currentCircle
   */
  public void setCurrentCircle(CircleOptions currentCircle) {
    this.currentCircle = currentCircle;
  }

  /**
   * @return
   */
  public CircleOptions getEndCircle() {
    return endCircle;
  }

  /**
   * @param endCircle
   */
  public void setEndCircle(CircleOptions endCircle) {
    this.endCircle = endCircle;
  }
}
