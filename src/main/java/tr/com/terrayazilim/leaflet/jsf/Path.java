/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import java.io.Serializable;
import tr.com.terrayazilim.core.Objects;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 * @since 1.0.0
 */
public class Path implements Serializable, Cloneable {

  protected Boolean stroke;
  protected String color;
  protected Double weight;
  protected Double opacity;
  protected String lineCap;
  protected String lineJoin;
  protected String dashArray;
  protected String dashOffset;
  protected Boolean fill;
  protected String fillStyle;
  protected String fillColor;
  protected Double fillOpacity;
  protected String fillRule;

  protected Renderer renderer;
  protected String className;

  public Path() {
  }

  /**
   *
   * @param options
   */
  public Path(Path options) {
    if (Objects.isNull(options)) {
      options = new Path();
    }

    this.stroke = options.getStroke();
    this.color = options.getColor();
    this.weight = options.getWeight();
    this.opacity = options.getOpacity();
    this.lineCap = options.getLineCap();
    this.lineJoin = options.getLineJoin();
    this.dashArray = options.getDashArray();
    this.dashOffset = options.getDashOffset();
    this.fill = options.getFill();
	this.fillStyle = options.getFillStyle();
    this.fillColor = options.getFillColor();
    this.fillOpacity = options.getFillOpacity();
    this.fillRule = options.getFillRule();
    this.renderer = options.getRenderer();
    this.className = options.getClassName();
  }

  /**
   * @param builder
   */
  public Path(PathBuilder builder) {
    this.stroke = builder.stroke;
    this.color = builder.color;
    this.weight = builder.weight;
    this.opacity = builder.opacity;
    this.lineCap = builder.lineCap;
    this.lineJoin = builder.lineJoin;
    this.dashArray = builder.dashArray;
    this.dashOffset = builder.dashOffset;
    this.fill = builder.fill;
	this.fillStyle = builder.fillStyle;
    this.fillColor = builder.fillColor;
    this.fillOpacity = builder.fillOpacity;
    this.fillRule = builder.fillRule;
    this.renderer = builder.renderer;
    this.className = builder.className;
  }
  
  /**
   * @return 
   */
  public Style toStyle() {
	return new Style(this);
  }

  /**
   * @return
   */
  public Boolean getStroke() {
    return stroke;
  }

  /**
   * @param stroke
   */
  public void setStroke(Boolean stroke) {
    this.stroke = stroke;
  }

  /**
   * @return
   */
  public String getColor() {
    return color;
  }

  /**
   * @param color
   */
  public void setColor(String color) {
    this.color = color;
  }

  /**
   * @return
   */
  public Double getWeight() {
    return weight;
  }

  /**
   * @param weight
   */
  public void setWeight(Double weight) {
    this.weight = weight;
  }

  /**
   * @return
   */
  public Double getOpacity() {
    return opacity;
  }

  /**
   * @param opacity
   */
  public void setOpacity(Double opacity) {
    this.opacity = opacity;
  }

  /**
   * @return
   */
  public String getLineCap() {
    return lineCap;
  }

  /**
   * @param lineCap
   */
  public void setLineCap(String lineCap) {
    this.lineCap = lineCap;
  }

  /**
   * @return
   */
  public String getLineJoin() {
    return lineJoin;
  }

  /**
   * @param lineJoin
   */
  public void setLineJoin(String lineJoin) {
    this.lineJoin = lineJoin;
  }

  /**
   * @return
   */
  public String getDashArray() {
    return dashArray;
  }

  /**
   * @param dashArray
   */
  public void setDashArray(String dashArray) {
    this.dashArray = dashArray;
  }

  /**
   * @return
   */
  public String getDashOffset() {
    return dashOffset;
  }

  /**
   * @param dashOffset
   */
  public void setDashOffset(String dashOffset) {
    this.dashOffset = dashOffset;
  }

  /**
   * @return
   */
  public Boolean getFill() {
    return fill;
  }

  /**
   * @param fill
   */
  public void setFill(Boolean fill) {
    this.fill = fill;
  }

  /**
   * @since 1.0.1
   * 
   * @return 
   */
  public String getFillStyle() {
	return fillStyle;
  }

  /**
   * @since 1.0.1
   * 
   * @param fillStyle 
   */
  public void setFillStyle(String fillStyle) {
	this.fillStyle = fillStyle;
  }

  /**
   * @return
   */
  public String getFillColor() {
    return fillColor;
  }

  /**
   * @param fillColor
   */
  public void setFillColor(String fillColor) {
    this.fillColor = fillColor;
  }

  /**
   * @return
   */
  public Double getFillOpacity() {
    return fillOpacity;
  }

  /**
   * @param fillOpacity
   */
  public void setFillOpacity(Double fillOpacity) {
    this.fillOpacity = fillOpacity;
  }

  /**
   * @return
   */
  public String getFillRule() {
    return fillRule;
  }

  /**
   * @param fillRule
   */
  public void setFillRule(String fillRule) {
    this.fillRule = fillRule;
  }

  /**
   * @return
   */
  public Renderer getRenderer() {
    return renderer;
  }

  /**
   * @param renderer
   */
  public void setRenderer(Renderer renderer) {
    this.renderer = renderer;
  }

  /**
   * @return
   */
  public String getClassName() {
    return className;
  }

  /**
   * @param className
   */
  public void setClassName(String className) {
    this.className = className;
  }

  /**
   * @return
   */
  @Override
  public Path clone() {
    return new Path(this);
  }
}
