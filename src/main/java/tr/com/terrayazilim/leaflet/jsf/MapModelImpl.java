/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf;

import java.util.LinkedHashMap;
import java.util.Map;
import tr.com.terrayazilim.leaflet.jsf.layer.Layer;

/**
 * @see AbstractLayerMap
 * @see MapModel
 * 
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 */
public class MapModelImpl extends AbstractLayerMap implements MapModel {

  private TileLayer tileLayer;

  private PolylineMeasure polylineMeasure;
  private EasyPrint easyPrint;
  private Pm pm;

  private MapOptions options;
  private MapPanes panes;

  private Map<String, Layer> map;

  public MapModelImpl() {
    this.tileLayer = new XYZ();
    this.polylineMeasure = new PolylineMeasure();
    this.easyPrint = new EasyPrint();
    this.pm = new Pm();
    this.options = new MapOptions();
    this.panes = MapPanes.create();
    this.map = new LinkedHashMap<>();
  }
  
  public MapModelImpl(Map<String, Layer> map) {
	this();
	this.map = map;
  }

  @Override
  protected Map<String, Layer> getMap() {
    return this.map;
  }

  @Override
  protected void setMap(Map<String, Layer> map) {
    this.map = map;
  }

  /**
   * {@inheritDoc }
   */
  @Override
  public PolylineMeasure getPolylineMeasure() {
    return this.polylineMeasure;
  }

  /**
   * {@inheritDoc }
   */
  @Override
  public void setPolylineMeasure(PolylineMeasure polylineMeasure) {
    this.polylineMeasure = polylineMeasure;
  }

  /**
   * {@inheritDoc }
   */
  @Override
  public EasyPrint getEasyPrint() {
    return this.easyPrint;
  }

  /**
   * {@inheritDoc }
   */
  @Override
  public void setEasyPrint(EasyPrint easyPrint) {
    this.easyPrint = easyPrint;
  }

  /**
   * {@inheritDoc }
   */
  @Override
  public Pm getPm() {
    return this.pm;
  }

  /**
   * {@inheritDoc }
   */
  @Override
  public void setPm(Pm pm) {
    this.pm = pm;
  }

  @Override
  public MapOptions getOptions() {
    return this.options;
  }

  @Override
  public void setOptions(MapOptions options) {
    this.options = options;
  }

  @Override
  public MapPanes getPanes() {
    return this.panes;
  }

  @Override
  public void setPanes(MapPanes panes) {
    this.panes = panes;
  }

  @Override
  public TileLayer getTileLayer() {
    return this.tileLayer;
  }

  @Override
  public void setTileLayer(TileLayer tileLayer) {
    this.tileLayer = tileLayer;
  }
}
