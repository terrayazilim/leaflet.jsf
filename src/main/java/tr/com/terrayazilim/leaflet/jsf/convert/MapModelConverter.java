/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.convert;

import java.util.Map;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import tr.com.terrayazilim.gjson.GjsonFeatureCollection;
import tr.com.terrayazilim.json.type.Token;
import tr.com.terrayazilim.leaflet.jsf.L;
import tr.com.terrayazilim.leaflet.jsf.MapModel;
import tr.com.terrayazilim.leaflet.jsf.layer.Layer;
import tr.com.terrayazilim.leaflet.jsf.renderkit.GeojsonKit;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 * @since 1.0.1
 */
@FacesConverter(value = "MapModelConverter")
public class MapModelConverter implements Converter {

  @Override
  public Object getAsObject(FacesContext context, UIComponent component, String value) {	
	GeojsonKit kit = new GeojsonKit();
	MapModel model = L.map();

	try {
	  GjsonFeatureCollection gfc = kit.fromGjson(value, GjsonFeatureCollection.class);
	  Map<String, Layer> map = kit.getMap(gfc);
	  model.putAll(map);
	} catch (Exception e) {
	  throw new LeafletConverterException("MapModel cannot initiated. {Hint: ErrorCode:250}", e);
	}
	
	return model;
  }

  @Override
  public String getAsString(FacesContext context, UIComponent component, Object value) {
	GeojsonKit kit = new GeojsonKit();
	String geojson = null;
	
	try {
	  MapModel model = (MapModel) value;
	  GjsonFeatureCollection gfc = kit.getFeatureCollection(model);
	  geojson = kit.toGjson(gfc, new Token<>(GjsonFeatureCollection.class).getType());
	} catch (Exception e) {
	  throw new LeafletConverterException("MapModel cannot converted. {Hint: ErrorCode:251}", e);
	}
	
	return geojson;
  }

}