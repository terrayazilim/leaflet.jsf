/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tr.com.terrayazilim.leaflet.jsf.layer;

import static java.lang.Math.max;
import static java.lang.Math.min;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import tr.com.terrayazilim.gjson.GjsonFeature;
import tr.com.terrayazilim.gjson.GjsonLineString;
import tr.com.terrayazilim.json.JsonObject;
import tr.com.terrayazilim.json.annotation.Ignored;
import tr.com.terrayazilim.leaflet.jsf.LatLng;
import tr.com.terrayazilim.leaflet.jsf.renderkit.JsonKit;

/**
 * @see <a href="https://leafletjs.com/reference-1.3.0.html#rectangle">L.Rectangle</a>
 * 
 * DevNote: If you use {@link #setSouthWest} or {@link #setNorthEast} method, 
 * use {@link #validate} to stay connected to Leaflet.
 *
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.1
 * @since 1.0.0
 */
public class RectangleImpl extends AbstractLayer implements Rectangle {

  @Ignored
  private static final long serialVersionUID = 3759375058373L;

  private RectangleOptions options = new RectangleOptions();
  private LatLng southWest;
  private LatLng northEast;

  public RectangleImpl() {
    super(LayerType.Rectangle);
  }

  /**
   * @param latLng0
   * @param latLng1 
   */
  public RectangleImpl(LatLng latLng0, LatLng latLng1) {
    this();

    if (latLng0.lat() < latLng1.lat() && latLng0.lon() < latLng1.lon()) {
      this.southWest = latLng0;
      this.northEast = latLng1;
    } else if (latLng0.lat() > latLng1.lat() && latLng0.lon() > latLng1.lon()) {
      this.southWest = latLng1;
      this.northEast = latLng0;
    } else {
      this.southWest = new LatLng(min(latLng0.lat(), latLng1.lat()), min(latLng0.lon(), latLng1.lon()));
      this.northEast = new LatLng(max(latLng0.lat(), latLng1.lat()), max(latLng0.lon(), latLng1.lon()));
    }
  }

  /**
   * @param bounds 
   */
  public RectangleImpl(Collection<LatLng> bounds) {
    this();
    resolveBounds(bounds);
  }

  /**
   * @param bounds 
   */
  public RectangleImpl(LatLng[] bounds) {
    this();

    resolveBounds(Arrays.asList(bounds));
  }

  /**
   * Method calculates the bounds.
   * 
   * @param stack 
   */
  private void resolveBounds(Collection<LatLng> stack) {
    double minLat = Double.MAX_VALUE; 
    double maxLat = Double.MIN_VALUE;
    double minLon = Double.MAX_VALUE;
    double maxLon = Double.MIN_VALUE;
    
    for (LatLng next : stack) {
      final double lat = next.lat();
      final double lon = next.lon();
      
      if (lat < minLat) {
        minLat = lat;
      }
      if (lat > maxLat) {
        maxLat = lat;
      }
      if (lon < minLon) {
        minLon = lon;
      }
      if (lon > maxLon) {
        maxLon = lon;
      }
    }
    
    this.southWest = new LatLng(minLat, minLon);
    this.northEast = new LatLng(maxLat, maxLon);
  }
  
  /**
   * @param latLng0
   * @param latLng1
   * @param options 
   */
  public RectangleImpl(LatLng latLng0, LatLng latLng1, RectangleOptions options) {
    this(latLng0, latLng1);
    this.options = options;
  }

  /**
   * @param bounds
   * @param options 
   */
  public RectangleImpl(List<LatLng> bounds, RectangleOptions options) {
    this(bounds);
    this.options = options;
  }

  /**
   * @param bounds
   * @param options 
   */
  public RectangleImpl(LatLng[] bounds, RectangleOptions options) {
    this(bounds);
    this.options = new RectangleOptions();
  }

  /**
   * @param rectangle 
   */
  public RectangleImpl(Rectangle rectangle) {
    this(rectangle.getSouthWest(), rectangle.getNorthEast(), rectangle.getOptions());

    this.options = rectangle.getOptions();
  }
  
  @Override
  public GjsonFeature toGeoJSON() {
	JsonKit jsonKit = new JsonKit();
	
	GjsonLineString ls = new GjsonLineString(getSouthWest().getLatLng(), getNorthEast().getLatLng());
	
	GjsonFeature feature = new GjsonFeature();
	feature.setGeometry(ls);
	feature.setId(getLayerId());
	
	JsonObject properties = jsonKit.getJson().toJson(getOptions());
	properties.put("layerType", getLayerType().toString());
	feature.setProperties(properties);
	
	return feature;
  }

  @Override
  public LatLng getSouthWest() {
    return this.southWest;
  }

  @Override
  public LatLng getNorthEast() {
    return this.northEast;
  }

  @Override
  public void setNorthEast(LatLng northEast) {
    this.northEast = northEast;
  }

  @Override
  public void setSouthWest(LatLng southWest) {
    this.southWest = southWest;
  }

  @Override
  public boolean isValid() {
    RectangleImpl tmp = new RectangleImpl(getSouthWest(), getNorthEast());

    boolean low0 = getSouthWest().lat() == tmp.getSouthWest().lat();
    boolean low1 = getSouthWest().lon() == tmp.getSouthWest().lon();
    boolean low2 = getNorthEast().lat() == tmp.getNorthEast().lat();
    boolean low3 = getNorthEast().lon() == tmp.getNorthEast().lon();

    return low0 && low1 && low2 && low3;
  }

  @Override
  public void validate() {
    resolveBounds(Arrays.asList(getBounds()));
  }

  @Override
  public LatLng[] getBounds() {
    return new LatLng[]{southWest, northEast};
  }

  @Override
  public void setBounds(List<LatLng> latLngs) {
    RectangleImpl tmp = new RectangleImpl(latLngs);

    this.southWest = tmp.getSouthWest();
    this.northEast = tmp.getNorthEast();
  }

  @Override
  public void setBounds(LatLng... latLngs) {
    RectangleImpl tmp = new RectangleImpl(latLngs);

    this.southWest = tmp.getSouthWest();
    this.northEast = tmp.getNorthEast();
  }

  @Override
  public RectangleOptions getOptions() {
    return this.options;
  }

  @Override
  public void setOptions(LayerOptions options) {
    this.options = (RectangleOptions) options;
  }

  @Override
  public Rectangle clone() {
    Rectangle rectangle = new RectangleImpl(this);
    rectangle.setLayerId(getLayerId());
    rectangle.setOptionalData(getOptionalData());
    
    return rectangle;
  }
}
