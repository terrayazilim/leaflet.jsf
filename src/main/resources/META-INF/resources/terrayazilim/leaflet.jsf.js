/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * This file is part of terrayazilim-leaflet.jsf project.
 *
 * This file incorporates work covered by
 * the following copyright and permission notices:
 *
 * Copyright (C) 2018 Terra Yazılım Bilişim Hiz. Elek. Dan. Oto. ve Loj. Tic. Ltd. Şti. | info [at] terrayazilim [dot] com [dot] tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* global L */

/**
 * @project leaflet.jsf
 * @version 1.0.0
 */
if (typeof (L) !== 'undefined') {

  /**
   * Namespace for L.jsf
   */
  L.jsf = {};

  /**
   * Super class for Layers.
   */
  L.jsf.layer = L.Class.extend({
    clientId: null,
    id: null,
    data: null,

    $layer: null
  });

  /**
   * @type type
   */
  const map = L.Class.extend({

    /**
     * @param {type} map
     * @returns {undefined}
     */
    initialize: function (map) {
      this.$map = map;
      this.clientId = this.$map._container.id;
      this.behaviors = [];
      // crawling back2you.
    },

    /**
     * @param {type} event
     * @returns mapclick event request context
     */
    _onClickEncode: function (event) {
      if (!$$_.isEvent(event, 'click')) {
        throw new Error('Unexpected Event. {Hint: L.Evented, click}');
      }

      return this.encode({
        latlng: event.latlng
      });
    },

    /**
     * @param {type} event
     * @returns mapcontextmenu event request context
     */
    _onContextMenuEncode: function (event) {
      if (!$$_.isEvent(event, 'contextmenu')) {
        throw new Error('Unexpected Event. {Hint: L.Evented, contextmenu}');
      }

      return this.encode({
        latlng: event.latlng
      });
    },

    /**
     * @param {type} event
     * @returns pm:create event request context
     */
    _onPmCreateEncode: function (event) {
      if (!$$_.isEvent(event, 'pm:create')) {
        throw new Error('Unexpected Event. {Hint: L.Evented, pm:create}');
      }

      if ($$_.isMarker(event.shape)) {
        return event.layer.jsf.encode({
          latlng: event.layer.getLatLng()
        });
      } else if ($$_.isCircle(event.shape)) {
        return event.layer.jsf.encode({
          latlng: event.layer.getLatLng(),
          radius: event.layer.getRadius()
        });
      } else if ($$_.isRectangle(event.shape)) {
        var $bounds = event.layer.getBounds();
        return event.layer.jsf.encode({
          sw: $bounds.getSouthWest(),
          ne: $bounds.getNorthEast()
        });
      } else if ($$_.isPolyline(event.shape)) {
        return event.layer.jsf.encode({
          latlngs: event.layer.getLatLngs()
        });
      } else if ($$_.isPolygon(event.shape)) {
        return event.layer.jsf.encode({
          latlngs: event.layer.getLatLngs()
        });
      }
    },

    /**
     * @param {type} event
     * @returns pm:remove event request context
     */
    _onPmRemoveEncode: function (event) {
      if (!$$_.isEvent(event, 'pm:remove')) {
        throw new Error('Unexpected Event. {Hint: L.Evented, pm:remove}');
      }

      return event.layer.jsf.encode({});
    },

    /**
     * @param {type} event
     * @returns pm:edit event request context
     */
    _onPmEditEncode: function (event) {
      if (!$$_.isEvent(event, 'pm:edit')) {
        throw new Error('Unexpected Event. {Hint: L.Evented, pm:edit}');
      } else {
        if ($$_.isMarker(event.target)) {
          return event.target.jsf.encode({
            latlng: event.target.getLatLng()
          });
        } else if ($$_.isCircle(event.target)) {
          return event.target.jsf.encode({
            latlng: event.target.getLatLng(),
            radius: event.target.getRadius()
          });
        } else if ($$_.isRectangle(event.target)) {
          var $bounds = event.target.getBounds();
          return event.target.jsf.encode({
            sw: $bounds.getSouthWest(),
            ne: $bounds.getNorthEast()
          });
        } else if ($$_.isPolyline(event.target)) {
          return event.target.jsf.encode({
            latlngs: event.target.getLatLngs()
          });
        } else if ($$_.isPolygon(event.target)) {
          return event.target.jsf.encode({
            latlngs: event.target.getLatLngs()
          });
        }
      }
    },

    /**
     * @param {type} context
     * @returns {leaflet.jsfL#29}
     */
    encode: function (context) {
      var $p = {};

      if (context) {
        $p[this.clientId + $jsf.requestPrefix] = this.clientId;

        if (context.type) {
          $p[this.clientId + $jsf.eventPrefix] = context.type;
        }

        if (context.latlng) {
          $p[this.clientId + $jsf.latPrefix] = context.latlng.lat.toString();
          $p[this.clientId + $jsf.lngPrefix] = context.latlng.lng.toString();
        }
      }

      return JSON.stringify($$_.setAttr(this.clientId, $p));
    },

    /**
     * @param {L.PM} pm
     * @returns {void}
     */
    configurePm: function (pm) {
      var $json;
      if (pm) {
        $json = $$_.toObject(pm);
      } else {
        $json = $jsf.pmOptions;
      }
      this.$map.pm.addControls($json);
    },

    /**
     * @param {type} boxzoom
     * @returns {undefined}
     */
    configureBoxZoom: function (boxzoom) {
      var $json;
      if (boxzoom) {
        $json = $$_.toObject(boxzoom);
      } else {
        $json = $jsf.boxZoomOptions;
      }
      L.Control.boxzoom($json).addTo(this.$map);
    },

    /**
     * @param {type} options
     */
    configurePolylineMeasurement: function (options) {
      var $json;
      if (options) {
        $json = $$_.toObject(options);
      } else {
        $json = $jsf.polylineMeasureOptions;
      }
      L.control.polylineMeasure($json).addTo(this.$map);
    },

    /**
     * @param {type} options
     * @param {type} tiles
     */
    configureEasyPrint: function (options, tiles) {
      if (!options && !tiles) {
        return -1;
      } else if (!options && tiles) {
        L.easyPrint($jsf.easyPrint.options(tiles)).addTo(this.$map);
        return 1;
      }
      
      if (options && tiles) {
        options.tileLayer = tiles;
        var tmp = $$_.toObject(options);
        if (!tmp.sizeModes) {
          tmp.sizeModes = $jsf.easyPrint.sizeModes;
        }
        
        L.easyPrint(tmp).addTo(this.$map);

        return 1;
      }

      return -1;
    },

    /**
     * @param {type} context
     * @param {type} event
     * @returns {undefined}
     */
    request: function (context, event) {
      if ($$_.has(this.behaviors, event)) {
        let ajax = this.behaviors[event];

        // Süre gelen erozyonun en pahalı kaybıyken arkadaşlık
        // Ben bu sınavdan kalmam.
        var response = {};
        response[this.clientId + "_json"] = context;

        ajax(response);
      }
    },

    /**
     * handles response to layer click event.
     *
     * @param {L.Layer} layer L.Layer
     */
    _handleLayerOnClick: function (layer) {
      var that = this;
      const $e = $jsf.events.click;

      if ($$_.has(this.behaviors, $e)) {
        layer.on($e, function (event) {
          if (!that.$map.pm.globalRemovalEnabled() && !layer.pm.enabled()) {
            L.DomEvent.stopPropagation(event);
            that.request(layer.jsf.encode({}), $e);
          }
        });
      }
    },

    /**
     * handles response to layer contextmenu event.
     *
     * @param {L.Layer} layer
     */
    _handleLayerOnContextMenu: function (layer) {
      var that = this;
      const $e = $jsf.events.contextmenu;
      if ($$_.has(this.behaviors, $e)) {
        layer.on($e, function (event) {
          L.DomEvent.stopPropagation(event);
          that.request(this.jsf.encode({}), $e);
        });
      }
    },

    /**
     * @param {type} $popup
     */
    _handlePopup: function ($popup) {
      var $p;
      if ($popup) {
        const $isR = $popup.responsive;
        if ($isR && $isR === true) {
          $p = L.responsivePopup($popup).setContent($popup.content);
        } else {
          $p = L.popup($popup).setContent($popup.content);
        }
      }

      return $p;
    },

    _handleTooltip: function ($layer, $tooltip) {
      if ($tooltip) {
        $layer.bindTooltip($tooltip.content, $tooltip);
      }

      return $layer;
    },

    /**
     * @param  {L.Layer} layer
     */
    handle: function (layer) {
      var that = this;

      this._handleLayerOnClick(layer);
      this._handleLayerOnContextMenu(layer);

      if ($$_.has(this.behaviors, 'pmedit')) {
        const $e = 'pm:edit';
        layer.on($e, function (event) {
          L.DomEvent.stopPropagation(event);
          if ($$_.isMarker(layer)) {
            if (!layer.listens('dragend')) {
              that.request(that._onPmEditEncode(event), 'pmedit');
            }
          } else {
            that.request(that._onPmEditEncode(event), 'pmedit');
          }
        });
      }

      if (($$_.isMarker(layer)) && ($$_.has(this.behaviors, $jsf.events.dragend))) {
        const $e = $jsf.events.dragend;
        layer.on($e, function (event) {
          that.request(this.jsf.encode({
            latlng: event.target.getLatLng(),
            distance: event.distance
          }), $e);
        });
      }
    },

    decodeCanvasRenderer: function ($options) {
      // suçu üstümüze yüklediler.
      var renderer = $options.renderer;
      if (renderer && renderer.type && renderer.type === 'roughCanvas') {
        $options.renderer = L.Canvas.roughCanvas();
        var random = renderer.randomPaint;
        if (random && random === true) {
          $options.fillColor = $$_.color();
          $options.fillStyle = $$_.style();
          $options.hachureAngle = $$_.angle();
        }
      }

      return $options;
    },

    decodeMarker: function (json) {
      const $rx = json;
      let latlng = $rx.latLng;
      let lat = $rx.latLng['lat'];
      let lng = $rx.latLng['lng'];
      if (!lat || !lng) {
        throw new Error("Unacceptable latitude, longitude.");
      }

      latlng = [lat, lng];

      let id = $rx.layerId;
      var marker;
      if ($rx.options) {
        if ($rx.options.icon) {
          $rx.options.icon = L.icon($rx.options.icon);
        }
        marker = L.marker(latlng, $rx.options);
      } else {
        marker = L.marker(latlng);
      }
      marker = this._handleTooltip(marker, $rx.options.tooltip);
      // yolları kapattılar, açacağız.
      // ufku kararttılar, ağartacağız.
      // yurdumuz virandır, şenleteceğiz.
      var $pp = this._handlePopup($rx.options.popup);
      marker.jsf.id = id;
      if ($rx.data) {
        marker.jsf.data = $rx.data;
      }
      this.handle(marker);
      return $pp ?
              marker.addTo(this.$map).bindPopup($pp) :
              marker.addTo(this.$map);
    },

    decodeCircle: function (json) {
      const $rx = json;
      let latlng = $rx.latLng;
      let lat = $rx.latLng['lat'];
      let lng = $rx.latLng['lng'];
      if (!lat || !lng) {
        throw new Error("Unacceptable latitude, longitude.");
      }

      latlng = [lat, lng];
      let id = $rx.layerId;
      var circle;
      if ($rx.options) {
        $rx.options = this.decodeCanvasRenderer($rx.options);
        circle = L.circle(latlng, $rx.options);
      } else {
        circle = L.circle(latlng);
      }

      circle = this._handleTooltip(circle, $rx.options.tooltip);
      var $pp = this._handlePopup($rx.options.popup);
      circle.jsf.id = id;
      if ($rx.data) {
        circle.jsf.data = $rx.data;
      }
      // aklımın odaları senle doldu taştı.
      this.handle(circle);
      return $pp ?
              circle.addTo(this.$map).bindPopup($pp) :
              circle.addTo(this.$map);
    },

    decodeRectangle: function (json) {
      const $rx = json;
      let id = $rx.layerId;
      let sw = $rx.southWest;
      let swlat = $rx.southWest['lat'];
      let swlng = $rx.southWest['lng'];
      if (!swlat || !swlng) {
        throw new Error("Unacceptable latitude, longitude.");
      }
      sw = [swlat, swlng];

      let nelat = $rx.northEast['lat'];
      let nelng = $rx.northEast['lng'];
      if (!nelat || !nelng) {
        throw new Error("Unacceptable latitude, longitude.");
      }
      ne = [nelat, nelng];

      var rectangle;
      if ($rx.options) {
        $rx.options = this.decodeCanvasRenderer($rx.options);
        rectangle = L.rectangle([sw, ne], $rx.options);
      } else {
        rectangle = L.rectangle([sw, ne]);
      }

      rectangle = this._handleTooltip(rectangle, $rx.options.tooltip);
      var $pp = this._handlePopup($rx.options.popup);
      rectangle.jsf.id = id;
      if ($rx.data) {
        rectangle.jsf.data = $rx.data;
      }
      this.handle(rectangle);
      return $pp ?
              rectangle.addTo(this.$map).bindPopup($pp) :
              rectangle.addTo(this.$map);
    },

    decodePolyline: function (json) {
      const $rx = json;
      let id = $rx.layerId;
      let ll = $rx.latLngs;
      let latLngs = [];
      var polyline;

      for (var i = 0; i < ll.length; i++) {
        var exterior = ll[i];
        for (var j = 0; j < exterior.length; j++) {
          var pair = exterior[j];
          let lat = Number(pair.lat);
          let lng = Number(pair.lng);
          latLngs.push([lat, lng]);
        }
      }

      if ($rx.options) {
        $rx.options = this.decodeCanvasRenderer($rx.options);
        polyline = L.polyline(latLngs, $rx.options);
      } else {
        polyline = L.polyline(latLngs);
      }

      polyline = this._handleTooltip(polyline, $rx.options.tooltip);
      var $pp = this._handlePopup($rx.options.popup);
      polyline.jsf.id = id;
      if ($rx.data) {
        polyline.jsf.data = $rx.data;
      }
      this.handle(polyline);
      return $pp ?
              polyline.addTo(this.$map).bindPopup($pp) :
              polyline.addTo(this.$map);
    },

    decodePolygon: function (json) {
      const $rx = json;
      let id = $rx.layerId;
      let ll = $rx.latLngs;
      let latLngs = [];
      var polygon;

      for (var i = 0; i < ll.length; i++) {
        var exterior = ll[i];
        for (var j = 0; j < exterior.length; j++) {
          var pair = exterior[j];
          let lat = Number(pair.lat);
          let lng = Number(pair.lng);
          latLngs.push([lat, lng]);
        }
      }
      
      if ($rx.options) {
        $rx.options = this.decodeCanvasRenderer($rx.options);
        polygon = L.polygon(latLngs, $rx.options);
      } else {
        polygon = L.polygon(latLngs);
      }
      
      polygon = this._handleTooltip(polygon, $rx.options.tooltip);
      var $pp = this._handlePopup($rx.options.popup);
      polygon.jsf.id = id;
      if ($rx.data) {
        polygon.jsf.data = $rx.data;
      }
      this.handle(polygon);
      return $pp ?
              polygon.addTo(this.$map).bindPopup($pp) :
              polygon.addTo(this.$map);
    },

    /**
     * @param {type} json
     * @returns {L.Layer} 
     */
    decode: function (json) {
      const $rx = (json !== null && typeof json === 'object') ? json : JSON.parse(json);
      var $var;

      if ($$_.isMarker($rx.layerId)) {
        $var = this.decodeMarker($rx);
      } else if ($$_.isCircle($rx.layerId)) {
        $var = this.decodeCircle($rx);
      } else if ($$_.isRectangle($rx.layerId)) {
        $var = this.decodeRectangle($rx);
      } else if ($$_.isPolyline($rx.layerId)) {
        $var = this.decodePolyline($rx);
      } else if ($$_.isPolygon($rx.layerId)) {
        $var = this.decodePolygon($rx);
      } else {
        throw new Error('Given layerId is not acceptable. {Hint: l[L.Layer]_[uuid]} See: lcircle_f872a9d6-e289-46d5-9279-b8a573841652');
      }

      return $var;
    },

    _fire: function (eventType) {
      var that = this;
      if ($$_.has(this.behaviors, eventType)) {
        var $t;
        if (eventType === $jsf.events.mapclick) {
          $t = 'click';
        } else if (eventType === $jsf.events.mapcontextmenu) {
          $t = 'contextmenu';
        } else if (eventType === $jsf.events.pmcreate) {
          $t = 'pm:create';
        } else if (eventType === $jsf.events.pmremove) {
          $t = 'pm:remove';
        }

        this.$map.on($t, function (event) {
          //L.DomEvent.stopPropagation(event);
          var $context;
          if ($t === 'click') {
            $context = that._onClickEncode(event);
          } else if ($t === 'contextmenu') {
            $context = that._onContextMenuEncode(event);
          } else if ($t === 'pm:create') {
            that.handle(event.layer);
            $context = that._onPmCreateEncode(event);
          } else if ($t === 'pm:remove') {
            $context = that._onPmRemoveEncode(event);
          } else if ($t === 'pm:edit') {
            $context = that._onPmEditEncode(event);
          } else {
            throw new Error('Unexpected event. {Hint: click, mapclick, contextmenu, contextmenu, dragend, pm:create, pm:edit, pm:remove}');
          }

          if ($t === 'click') {
            if (!$$_.drawingEnabled(that.$map.pm) && !that.$map.pm.globalEditEnabled()) {
              that.request($context, eventType);
            }
          } else {
            that.request($context, eventType);
          }
        });
      }
    },

    /**
     * @param {type} behaviors
     * @returns {undefined}
     */
    setBehaviors: function (behaviors) {
      this.behaviors = behaviors;
      this._fire($jsf.events.mapclick);
      this._fire($jsf.events.mapcontextmenu);
      this._fire($jsf.events.pmcreate);
      this._fire($jsf.events.pmremove);
      // Runaway train never going back,
      // Wrong way on a one-way track.

      var that = this;
      this.$map.on('popupopen', function (e) {
        e.popup.highlight = L.circleMarker(e.popup.getLatLng(), {
          radius: 15,
          opacity: 0,
          fillColor: "#000000",
          fillOpacity: .3
        }).addTo(that.$map);
      });

      this.$map.on('popupclose', function (e) {
        that.$map.removeLayer(e.popup.highlight);
      });
    },

    /**
     * @param  {} json
     */
    add: function (json) {
      var $tmp = this.decode(json);
      $tmp.clientId = this.clientId;

      // Ayrıntıya takık azınlığa hayran insanların
      // Gün gelince böyle ağzı bıçak açmaz

      if ($tmp) {
        return $tmp.addTo(this.$map);
      }
    },

    addAll: function (json, key) {
      var $json = JSON.parse(json);
      var $values = $json[key];

      var that = this;
      $values.forEach(function (each) {
        var $tmp = that.decode(each);
        $tmp.clientId = that.clientId;

        if ($tmp) {
          $tmp.addTo(that.$map);
        }
      });
    }
  });

  /**
   * @type type
   */
  const marker = L.jsf.layer.extend({

    /**
     * @param {type} layer
     * @returns {undefined}
     */
    initialize: function (layer) {
      this.$layer = layer;
      this.clientId = L.jsf.clientId;

      if (!this.id) {
        this.id = $$_.uuid(this.$layer);
      }

      // Black bird, black moon, black sky, black light,
      // Black, everything black.
    },

    /**
     * @param {type} context
     * @returns {leaflet.jsfL#29}
     */
    encode: function (context) {
      var pkg = {};

      if (context) {
        pkg[this.clientId + $jsf.layerIdPrefix] = this.id;

        if (context.latlng) {
          pkg[this.clientId + $jsf.latPrefix] = context.latlng.lat.toString();
          pkg[this.clientId + $jsf.lngPrefix] = context.latlng.lng.toString();
        }

        if (context.distance) {
          pkg[this.clientId + $jsf.distancePrefix] = context.distance;
        }
      }

      return JSON.stringify($$_.setAttr(this.clientId, pkg));
    }
  });

  /**
   * @type type
   */
  const circle = L.jsf.layer.extend({

    /**
     * @param {type} layer
     * @returns {undefined}
     */
    initialize: function (layer) {
      this.$layer = layer;
      this.clientId = L.jsf.clientId;

      if (!this.id) {
        this.id = $$_.uuid(this.$layer);
      }
    },

    encode: function (context) {
      var pkg = {};

      if (context) {
        pkg[this.clientId + $jsf.layerIdPrefix] = this.id;
        if (context.latlng) {
          pkg[this.clientId + $jsf.latPrefix] = context.latlng.lat.toString();
          pkg[this.clientId + $jsf.lngPrefix] = context.latlng.lng.toString();
        }

        if (context.radius) {
          pkg[this.clientId + $jsf.radiusPrefix] = context.radius;
        }
      }

      return JSON.stringify($$_.setAttr(this.clientId, pkg));
    }
  });

  /**
   * @type type
   */
  const rectangle = L.jsf.layer.extend({

    /**
     * @param {type} layer
     * @returns {undefined}
     */
    initialize: function (layer) {
      this.$layer = layer;
      this.clientId = L.jsf.clientId;

      if (!this.id) {
        this.id = $$_.uuid(this.$layer);
      }

      // Black heart, black keys, black diamonds,
      // Blackout, black, everything black.
    },

    encode: function (context) {
      var pkg = {};

      if (context) {
        pkg[this.clientId + $jsf.layerIdPrefix] = this.id;
        if (context.sw && context.ne) {
          pkg[this.clientId + '_sw' + $jsf.latPrefix] = context.sw.lat.toString();
          pkg[this.clientId + '_sw' + $jsf.lngPrefix] = context.sw.lng.toString();
          pkg[this.clientId + '_ne' + $jsf.latPrefix] = context.ne.lat.toString();
          pkg[this.clientId + '_ne' + $jsf.lngPrefix] = context.ne.lng.toString();
        }
      }

      return JSON.stringify($$_.setAttr(this.clientId, pkg));
    }
  });

  /**
   * @type type
   */
  const polyline = L.jsf.layer.extend({

    /**
     * @param {type} layer
     * @returns {undefined}
     */
    initialize: function (layer) {
      this.$layer = layer;
      this.clientId = L.jsf.clientId;

      if (!this.id) {
        this.id = $$_.uuid(this.$layer);
      }
    },

    /**
     * @param  {} context
     */
    encode: function (context) {
      var pkg = {};
      
      if (context) {
        pkg[this.clientId + $jsf.layerIdPrefix] = this.id;
        if (context.latlngs) {
          var ring = context.latlngs;
          var exterior = new Array();
          
          for (var i = 0; i < ring.length; i++) {
            var next = ring[i];
            if (Array.isArray(next)) {
              let interior = new Array();
              for (var j = 0; j < next.length; j++) {
                let pair = next[j];
                interior.push(pair.lat + "," + pair.lng);
              }
              exterior.push(interior);
            } else {
              let shell = new Array();
              shell.push(next.lat + "," + next.lng);
              exterior.push(shell);
            }
          }
        }
        
        pkg[this.clientId + $jsf.latlngsPrefix] = exterior;
      }

      return JSON.stringify($$_.setAttr(this.clientId, pkg));
    }
  });

  /**
   * @type type
   */
  const polygon = L.jsf.layer.extend({

    /**
     * @param {type} layer
     * @returns {undefined}
     */
    initialize: function (layer) {
      this.$layer = layer;
      this.clientId = L.jsf.clientId;

      if (!this.id) {
        this.id = $$_.uuid(this.$layer);
      }
    },

    /**
     * @param  {} context
     */
    encode: function (context) {
      var pkg = {};
      
      if (context) {
        pkg[this.clientId + $jsf.layerIdPrefix] = this.id;
        if (context.latlngs) {
          var ring = context.latlngs;
          var exterior = new Array();
          
          for (var i = 0; i < ring.length; i++) {
            var next = ring[i];
            if (Array.isArray(next)) {
              let interior = new Array();
              for (var j = 0; j < next.length; j++) {
                let pair = next[j];
                interior.push(pair.lat + "," + pair.lng);
              }
              exterior.push(interior);
            } else {
              let shell = new Array();
              shell.push(next.lat + "," + next.lng);
              exterior.push(shell);
            }
          }
        }
        
        pkg[this.clientId + $jsf.latlngsPrefix] = exterior;
      }
      
      return JSON.stringify($$_.setAttr(this.clientId, pkg));
    }
  });

  /**
   * namespace for L.jsf
   */
  L.jsf = {
    map,
    marker,
    circle,
    rectangle,
    polyline,
    polygon,

    initialize: function () {
      this.clientId;
      this.setHooks();

      // It's time for me to leave this place ,
    },

    setHooks: function () {
      L.Map.addInitHook(function () {
        this.jsf = new L.jsf.map(this);
      });

      L.Marker.addInitHook(function () {
        this.jsf = new L.jsf.marker(this);
      });

      L.Circle.addInitHook(function () {
        this.jsf = new L.jsf.circle(this);
      });

      L.Rectangle.addInitHook(function () {
        this.jsf = new L.jsf.rectangle(this);
      });

      L.Polyline.addInitHook(function () {
        this.jsf = new L.jsf.polyline(this);
      });

      L.Polygon.addInitHook(function () {
        this.jsf = new L.jsf.polygon(this);
      });
    },

    decodeMap: function (map, clientId, options, tileLayer) {
      const $o = JSON.parse(options);
      const $t = JSON.parse(tileLayer);
      var $c, $z;
      var $p = {};
      for (var key in $o) {
        let attrKey = key;
        let attrVal = $o[key];

        if (attrKey === "center") {
          break;
        } else if (attrKey === "zoom") {
          break;
        } else {
          $p[attrKey] = attrVal;
        }
      }
      map = L.map(clientId, $p);
      if (!$t.url) {
        throw new Error("TileLayer doesn't contain URL.");
      }

      var $tile;
      if ($t.options) {
        $tile = L.tileLayer($t.url, $t.options).addTo(map);
      } else {
        $tile = L.tileLayer($t.url).addTo(map);
      }

      if ($o.center) {
        let lng = $o.center['lng'];
        let lat = $o.center['lat'];
        if (!lat || !lng) {
          throw new Error("Unacceptable latitude, longitude");
        }

        $c = [lat, lng];
      }
      if ($o.zoom) {
        $z = $o.zoom;
      }
      return {
        map: map,
        center: $c,
        zoom: $z,
        tilelayer: $tile
      };
    }
  };

  /**
   * namespace for L.jsf.util
   */
  L.jsf.util = {};

  /**
   * @param {JSON} json
   */
  L.jsf.util.toObject = function (json) {
    const $json = JSON.parse(json);
    var literal = {};

    for (var key in $json) {
      let attrKey = key;
      let attrVal = $json[key];

      literal[attrKey] = attrVal;
    }

    return literal;
  };

  /**
   * @returns random L.Layer color
   */
  L.jsf.util.color = function () {
    return `rgb(${Math.round(Math.random() * 255)}, ${Math.round(Math.random() * 255)}, ${Math.round(Math.random() * 255)})`;
  };

  /**
   * @returns random L.Layer angle
   */
  L.jsf.util.angle = function () {
    return (Math.random() > 0.5 ? -1 : 1) * (1 + Math.random() * 88);
  };

  /**
   * @returns random L.Layer style
   */
  L.jsf.util.style = function () {
    return (Math.random() > 0.8 ? 'solid' : '');
  };

  /**
   * @param {L.PM.Map} pm
   * @returns true if any drawing activated. Otherwise false.
   */
  L.jsf.util.drawingEnabled = function (pm) {
    if (pm.Draw['Marker'].enabled()) {
      return true;
    }

    if (pm.Draw['Circle'].enabled()) {
      return true;
    }

    if (pm.Draw['Rectangle'].enabled()) {
      return true;
    }

    if (pm.Draw['Line'].enabled()) {
      return true;
    }

    if (pm.Draw['Poly'].enabled()) {
      return true;
    }

    if (pm.Draw['Cut'].enabled()) {
      return true;
    }

    return false;
  };

  L.jsf.util.setAttr = function (id, pkg) {
    var $c = pkg ? pkg : {};

    var $d = new Date().toLocaleDateString();
    var $t = new Date().toLocaleTimeString('en-US', {
      hour12: false,
      hour: "numeric",
      minute: "numeric"
    });

    $c[id + L.jsf.datePrefix] = $d;
    $c[id + L.jsf.timePrefix] = $t;

    return $c;
  };

  /**
   * @param  {} req
   */
  L.jsf.util.print = function (req) {
    L.jsf.util.foreach(req, function (key, value) {
      console.log(key + ":" + value);
    });
    //console.log(req);
  };

  /**
   * @param {type} property
   * @param {type} agreed
   * @returns {unresolved}
   */
  L.jsf.util.orElse = function (property, agreed) {
    return (property) ? property : agreed;
  };

  /**
   * @param {type} obj
   * @returns {Boolean}
   */
  L.jsf.util.isEmpty = function (obj) {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop))
        return false;
    }

    return JSON.stringify(obj) === JSON.stringify({});
  };

  /**
   * @param {type} pkg
   * @param {type} ev
   * @param {type} functors
   * @returns {undefined}
   */
  L.jsf.util.chain = function (pkg, ev, functors) {
    if (functors) {
      for (var i = 0; i < functors.length; i++) {
        var retVal = functors[i].call(this, pkg, ev);
        if (retVal === false) {
          break;
        }
      }
    }
  };

  /**
   * @param {type} map
   * @param {type} value
   * @returns {L.jsf.util.getKey.map|leaflet.jsfL#26.util.getKey.prop}
   */
  L.jsf.util.getKey = function (map, value) {
    for (var prop in map) {
      if (map.hasOwnProperty(prop)) {
        if (map[prop] === value)
          return prop;
      }
    }
  };

  /**
   * @param {type} layer
   * @returns {String}
   */
  L.jsf.util.uuid = function (layer) {
    var tmp = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (
            c) {
      var r = Math.random() * 16 | 0,
              v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });

    var prefix;

    if (layer instanceof L.Marker) {
      prefix = L.jsf.layerPrefix.markerPrefix;
    } else if (layer instanceof L.Circle) {
      prefix = L.jsf.layerPrefix.circlePrefix;
    } else if (layer instanceof L.Rectangle) {
      prefix = L.jsf.layerPrefix.rectanglePrefix;
    } else if ((layer instanceof L.Polyline) && !(layer instanceof L.Polygon)) {
      prefix = L.jsf.layerPrefix.polylinePrefix;
    } else if ((layer instanceof L.Polygon) && !(layer instanceof L.Rectangle)) {
      prefix = L.jsf.layerPrefix.polygonPrefix;
    } else {
      prefix = "";
    }

    return prefix + tmp;
  };

  /**
   * @param {type} object
   * @param {type} callback
   * @returns {undefined}
   */
  L.jsf.util.foreach = function (object, callback) {
    for (var property in object) {
      if (object.hasOwnProperty(property)) {
        callback(property, object[property]);
      }
    }
  };

  /**
   * @param {type} map
   * @param {type} element
   * @returns {Boolean}
   */
  L.jsf.util.has = function (map, element) {
    return (map && map[element]);
  };

  /**
   * @param {type} map
   * @param {type} key
   * @param {type} value
   * @returns {unresolved}
   */
  L.jsf.util.set = function (map, key, value) {
    return map[key] = value;
  };

  L.jsf.util.isString = function (property) {
    return (typeof (property) === 'string') || (property instanceof String);
  };

  /**
   * @param {type} property
   * @returns true if L.Marker
   */
  L.jsf.util.isMarker = function (property) {
    if (L.jsf.util.isString(property)) {
      var $k = property.toString() === 'Marker';
      var $l = property.startsWith(L.jsf.layerPrefix.markerPrefix);
      var $m = property.id && property.id.startsWith(L.jsf.layerPrefix.markerPrefix);

      return $k || $l || $m;
    } else if (property instanceof L.Layer) {
      return property instanceof L.Marker;
    }

    return false;
  };

  /**
   * @param {type} property
   * @returns true if L.Circle
   */
  L.jsf.util.isCircle = function (property) {
    if (L.jsf.util.isString(property)) {
      var $k = property.toString() === 'Circle';
      var $l = property.startsWith(L.jsf.layerPrefix.circlePrefix);
      var $m = property.id && property.id.startsWith(L.jsf.layerPrefix.circlePrefix);

      return $k || $l || $m;
    } else if (property instanceof L.Layer) {
      return property instanceof L.Circle;
    }

    return false;
  };

  /**
   * @param {type} property
   * @returns true if L.Rectangle
   */
  L.jsf.util.isRectangle = function (property) {
    if (L.jsf.util.isString(property)) {
      var $k = property.toString() === 'Rectangle';
      var $l = property.startsWith(L.jsf.layerPrefix.rectanglePrefix);
      var $m = property.id && property.id.startsWith(L.jsf.layerPrefix.rectanglePrefix);

      return $k || $l || $m;
    } else if (property instanceof L.Layer) {
      return property instanceof L.Rectangle;
    }

    return false;
  };

  /**
   * @param {type} property
   * @returns true if L.Polyline
   */
  L.jsf.util.isPolyline = function (property) {
    if (L.jsf.util.isString(property)) {
      var $k = property.toString() === 'Line';
      var $l = property.startsWith(L.jsf.layerPrefix.polylinePrefix);
      var $m = property.id && property.id.startsWith(L.jsf.layerPrefix.polylinePrefix);

      return $k || $l || $m;
    } else if (property instanceof L.Layer) {
      return (property instanceof L.Polyline) && !(property instanceof L.Polygon);
    }

    return false;
  };

  /**
   * @param {type} property
   * @returns true if L.Polygon
   */
  L.jsf.util.isPolygon = function (property) {
    if (L.jsf.util.isString(property)) {
      var $k = property.toString() === 'Poly';
      var $l = property.startsWith(L.jsf.layerPrefix.polygonPrefix);
      var $m = property.id && property.id.startsWith(L.jsf.layerPrefix.polygonPrefix);

      return $k || $l || $m;
    } else if (property instanceof L.Layer) {
      return (property instanceof L.Polygon) && !(property instanceof L.Rectangle);
    }

    return false;
  };

  /**
   * @param {L.Evented} event
   * @param {String} name
   * @returns true if name is eq L.Evented.type. Otherwise false.
   */
  L.jsf.util.isEvent = function (event, name) {
    if (!event || !event.type) {
      return false;
    }

    return name && (event.type === name);
  };

  /**
   * JSF Communication configurations. Server side communicates via
   * these prefixes.
   */
  L.jsf.requestPrefix = "::Request";
  L.jsf.layerIdPrefix = "::LayerId";
  L.jsf.latPrefix = "::Latitude";
  L.jsf.lngPrefix = "::Longitude";
  L.jsf.latlngsPrefix = "::LatLngs";
  L.jsf.seperatorPrefix = ";;";
  L.jsf.datePrefix = "::Date";
  L.jsf.timePrefix = "::Time";
  L.jsf.uuidPrefix = "::Uuid";
  L.jsf.dimensionPrefix = "::Dimension";
  L.jsf.distancePrefix = "::Distance";
  L.jsf.radiusPrefix = "::Radius";
  L.jsf.eventPrefix = "::Event";
  L.jsf.synchPrefix = "::Synch";

  /**
   * Necessary for L.Layer id's.
   */
  L.jsf.layerPrefix = {
    markerPrefix: 'lmarker_',
    circlePrefix: 'lcircle_',
    rectanglePrefix: 'lrectangle_',
    polylinePrefix: 'lpolyline_',
    polygonPrefix: 'lpolygon_'
  };

  /**
   * Supported Events
   */
  L.jsf.events = {
    pmcreate: 'pmcreate',
    pmremove: 'pmremove',
    pmedit: 'pm:edit',
    dragend: 'dragend',
    click: 'click',
    contextmenu: 'contextmenu',
    mapclick: 'mapclick',
    mapcontextmenu: 'mapcontextmenu',
    load: 'load'
  };

  /**
   * Box-Zoom Plugin default configurations.
   */
  L.jsf.boxZoomOptions = {
    position: 'topleft'
  };

  /**
   * Leaflet.PM Plugin default configurations.
   */
  L.jsf.pmOptions = {
    position: 'topleft',
    drawMarker: true,
    drawPolyline: true,
    drawRectangle: true,
    drawPolygon: true,
    drawCircle: true,
    cutPolygon: false,
    editMode: true,
    removalMode: true
  };

  L.jsf.polylineMeasureOptions = {
    showBearings: true,
    measureControlTitleOn: 'Uzunluk Ölçümünü Aktive Et.',
    measureControlTitleOff: 'Uzunluk Ölçümünü Deaktive Et.',
    clearMeasurementsOnStop: false,
    showMeasurementsClearControl: true,
    clearControlTitle: 'Ölçümleri Temizle.'
  };

  L.jsf.easyPrint = {
    options: function (tilelayer) {
      return {
        tileLayer: tilelayer,
        sizeModes: ['Current', 'A4Landscape', 'A4Portrait'],
        filename: 'myMap',
        exportOnly: true,
        hideControlContainer: true
      };
    },

    sizeModes: ['Current', 'A4Landscape', 'A4Portrait']
  };

  const $$_ = L.jsf.util;
  const $jsf = L.jsf;

  /**
   * L.jsf Initialization.
   */
  L.jsf.initialize();
}